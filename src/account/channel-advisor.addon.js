"use strict";
const qs = require("qs");
const mem = require("mem");
const map = require("p-map");
const axios = require("axios");
const retry = require("p-retry");
const logger = require("../utils/logger");
const base64 = require("../utils/base64");

function getToken(renewToken, appId, sharedSecret) {
	return retry(
		() => axios
			.post(
				"https://api.channeladvisor.com/oauth2/token",
				qs.stringify({
					grant_type: "refresh_token",
					refresh_token: renewToken
				}),
				{
					headers: {
						Authorization: "Basic " + base64.encode([appId, sharedSecret].join(":")),
						"Content-Type": "application/x-www-form-urlencoded"
					}
				}
			)
			.then(response => response.data.access_token)
		,
		{ maxRetryTime: 2 }
	);
}

getToken = mem(getToken, { maxAge: 1000 * 60 * 1 });

module.exports = {

	getAgent() {
		return axios.create({ baseURL: "https://api.channeladvisor.com/v1" });
	},

	getAccessToken: async function () {
		return await getToken(
			this.credential.renewToken,
			this.credential.appId,
			this.credential.sharedSecret
		);
	},

	getInventory: async function (id) {
		return this
			.getAgent()
			.get(
				`/Products(${id})/DCQuantities`,
				{
					params: { access_token: await this.getAccessToken() }
				}
			)
			.then(response => response.data?.value)
			.then(distributionCenters => distributionCenters.find(item => item.DistributionCenterID == 4)?.AvailableQuantity);
	},

	updateInventory: async function (id, inventory) {
		return this
			.getAgent()
			.post(
				`/Products(${id})/UpdateQuantity`,
				{
					Value: {
						UpdateType: "Absolute",
						Updates: [{
							DistributionCenterID: 4,
							Quantity: inventory
						}]
					}
				},
				{
					params: { access_token: await this.getAccessToken() }
				}
			);
	},

	/**
	 * 
	 * Images
	 * 
	 */
	updateImages: async function (place, id, images = []) {

		let min, max;
		const PREFIX = "ITEMIMAGEURL";

		switch (place) {
			case "walmart":
				min = 1;
				max = 10;
				break;

			case "target":
				min = 20;
				max = 27;
				break;

			default:
				throw new Error("Unknown market place #" + place);
		}

		await map(
			images,
			async (image, index) => {

				if (index + min > max)
					return {};

				logger.debug(`[Channel advisor] Upload product #${id} image #${index + 1} ('${PREFIX + (min + index)}')`);

				await this
					.getAgent()
					.patch(
						`/Products(${id})/Images('${PREFIX + (min + index)}')`,
						{
							Url: image
						},
						{
							params: { access_token: await this.getAccessToken() }
						}
					);
				return {};
			},
			{
				concurrency: 1
			}
		);

		return true;
	},

	async addLabel(id, label) {
		return this
			.getAgent()
			.put(
				`https://api.channeladvisor.com/v1/Products(${id})/Labels('${label}')`,
				{},
				{
					params: { access_token: await this.getAccessToken() }
				}
			)
			.catch(error => console.error(error.response.data))
	},

	async getAttributes(id) {
		return this
			.getAgent()
			.get(
				`https://api.channeladvisor.com/v1/Products(${id})/Attributes`,
				{
					params: { access_token: await this.getAccessToken() }
				}
			)
			.then(response => response.data.value)
			.catch(error => console.error(error.response.data))
	}

};
