"use strict";
const qs = require("qs");
const ms = require("ms");
const mem = require("mem");
const UUID = require("uuid");
const axios = require("axios");

const addon = {

	getAgent() {
		return axios
			.create({
				baseURL: "https://marketplace.walmartapis.com/v3",
				headers: {
					["WM_SVC.NAME"]: "Walmart Marketplace",
					["WM_QOS.CORRELATION_ID"]: UUID.v4(),
					Accept: "application/json",
					Authorization: "Basic " + this.credential.token
				}
			});
	},

	getAccessToken() {
		return this
			.getAgent()
			.post(
				"/token",
				qs.stringify({ grant_type: "client_credentials" })
			)
			.then(response => response.data.access_token);
	}

};

addon.getAccessToken = mem(addon.getAccessToken, { maxAge: ms("3 minutes") });

module.exports = addon;
