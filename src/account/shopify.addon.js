"use strict";
const url = require("url");
const _ = require("lodash");
const delay = require("delay");
const axios = require("axios");
const retry = require("p-retry");
const queryString = require("query-string");

module.exports = {

	getAgent() {
		return axios
			.create({
				baseURL: `https://${this.credential.api_key}:${this.credential.password}@${this.credential.domain}/admin/api/2021-04`
			});
	},

	async listProducts() {

		const agent = this.getAgent();
		let products = [], nextCursor;

		do {

			const params = { limit: 250 };

			if (nextCursor)
				params.page_info = nextCursor;

			const response = await retry(
				() => agent
					.get("/products.json", { params })
					.then(response => {
						let page_info;
						if (response.headers.link) {
							let link = response
								.headers
								.link
								.split(",")
								.find(line => line.includes("rel=\"next\""));
							if (link) {
								link = /\<(.+)\>/mg.exec(link, true)[1];
								page_info = queryString.parse(url.parse(link).search).page_info;
							}
						}
						return {
							page_info: page_info,
							data: response.data.products
						};
					}),
				{
					onFailedAttempt: async error => {
						logger.debug(`[${this.name}] Attempt ${error.attemptNumber} failed. There are ${error.retriesLeft} retries left.`);
						await delay(2000);
					},
					retries: 5
				}
			);

			products = [].concat(products, response.data);
			nextCursor = response.page_info;
		} while (nextCursor);

		return products;
	},

	updateTitle(id, title) {

		if (!title)
			throw new Error("Missing product title");

		const agent = this.getAgent();
		return agent
			.put(`/products/${id}.json`, { product: { id, title } })
			.then(response => response.data);
	},

	updateDescription(id, description) {
		const agent = this.getAgent();
		return agent
			.put(
				`/products/${id}.json`,
				{ product: { id, body_html: description } }
			)
			.then(response => response.data)
			.then(() => description);
	},

	async getVariant(id) {
		const agent = this.getAgent();
		return agent
			.get(`/variants/${id}.json`)
			.then(response => response.data.variant);
	},

	updateImages(id, images) {
		const agent = this.getAgent();
		return agent
			.put("/inventory_levels/set.json", {
				product: {
					id,
					images: images.map(image => ({ src: image }))
				}
			})
			.then(response => response.data);
	},

	updatePrice(id, variantId, price) {
		const agent = this.getAgent();
		return agent
			.put(`/products/${id}.json`, {
				product: {
					id: id,
					variants: [
						{
							id: variantId,
							price: price
						}
					]
				}
			})
			.then(response => response.data);
	},

	updateInventory(variantId, inventory) {
		const agent = this.getAgent();
		return this
			.getVariant(variantId)
			.then(variant => agent.post("/inventory_levels/set.json", {
				"location_id": this.meta.efc_location_id,
				"inventory_item_id": variant.inventory_item_id,
				"available": inventory
			}));
	}
};
