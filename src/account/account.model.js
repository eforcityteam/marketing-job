"use strict";
const MySql = require("mysql-plugin");
const encoder = require("../utils/encoder");
const amazonAddon = require("./amazon.addon");
const walmartAddon = require("./walmart.addon");
const shopifyAddon = require("./shopify.addon");
const channelAdvisorAddon = require("./channel-advisor.addon");

function Account(id) {
	const proxy = { id };
	return ["get"].reduce((proxy, key) => {
		proxy[key] = Account[key].bind(Account, id);
		return proxy;
	}, proxy);
}

Account.marketPlace = async function (id) {
	const accounts = await MySql.query(
		"SELECT AccountId id FROM OrderIMDb.Account WHERE MarketPlace = ? AND RecStatus = \"A\";",
		id.toUpperCase()
	);
	return accounts.map(item => item.id);
};

Account.get = async function (id) {

	const [account] = await MySql.query(`
		SELECT 
			account.AccountId as id,
			account.FullName as name,
			account.MarketPlatformId as marketPlatform,
			marketPlatform.marketPlaceId,
			marketPlatform.marketPlatformName,
			marketPlatform.Country as country,
			marketPlatform.PlatformCurrency as currency,
			account.Collection as mongo,
			account.recStatus
		FROM OrderIMDb.Account account 
		LEFT JOIN ErpDb.MarketPlatform marketPlatform
		ON account.MarketPlatformId = marketPlatform.MarketPlatformId
		WHERE account.AccountId = ? OR account.MarketPlatformId = ?;
	`, [id, id]);

	if (!account)
		return null;

	const [
		dummy,
		meta,
		credential
	] = await MySql.query(`
        SELECT @id := ? as id;
        -- Meta
        SELECT 
            MetaKey as \`key\`,
            MetaValue as \`value\`,
            encrypted
        FROM OrderIMDb.AccountMeta meta
        WHERE meta.AccountId = @id;
        -- Credential
        SELECT 
			MetaKey as \`key\`,
			MetaValue as \`value\`,
            encrypted
        FROM AuthDb.ProgramCredentialMeta meta WHERE CredentialId = (
            SELECT CredentialId FROM OrderIMDb.Account account WHERE account.AccountId = @id
        );
	`, account.id);

	account.recStatus = account.recStatus == "A";

	account.meta = meta.reduce((proxy, curr) => {
		(proxy = (proxy || {}))[curr.key] = curr.encrypted == "T" ? encoder.decode(curr.value) : curr.value;
		return proxy;
	}, {});

	account.credential = credential.reduce((proxy, curr) => {
		(proxy = (proxy || {}))[curr.key] = curr.encrypted == "T" ? encoder.decode(curr.value) : curr.value;
		return proxy;
	}, {});

	if (id == "channel_advisor") {
		Object.assign(account, channelAdvisorAddon);
	} else if (account.marketPlaceId == "amaz") {
		Object.assign(account, amazonAddon);
	} else if (account.marketPlaceId == "SHOPI") {
		Object.assign(account, shopifyAddon);
	} else if (account.marketPlatform == "WALMART-FS") {
		Object.assign(account, walmartAddon);
	}
	
	return account;
};

module.exports = Account;
