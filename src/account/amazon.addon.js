"use strict";
const axios = require("axios");
const moment = require("moment");
const Immutable = require("immutable");
const parseXml = require("../utils/parseXml");
const signMWS = require("../utils/signMWS");

module.exports = {

	sign(options) {

		let params = Immutable
			.Map({
				Version: "2009-01-01",
				SellerId: this.credential.seller_id,
				SignatureMethod: "HmacSHA256",
				SignatureVersion: "2",
				AWSAccessKeyId: this.credential.access_key,
				["MarketplaceId.Id.1"]: this.credential.market_place_id,
				Timestamp: moment().toISOString()
			});

		params = params.merge(options);

		params = params
			.set(
				"Signature",
				signMWS(
					"Reports",
					"POST",
					params.toJSON(),
					this.credential.url,
					this.credential.secret_access_key
				)
			);

		return params.toJSON();
	},

	getReport(id) {

		const params = {
			Action: "GetReport",
			ReportId: id,
			Acknowledged: false
		};

		return axios
			.post(
				"https://" + this.credential.url + "/Reports/2009-01-01",
				null,
				{
					params: this.sign(params)
				}
			)
			.then(response => response.data);
	},

	requestReport(report) {

		const params = {
			Version: "2009-01-01",
			Action: "RequestReport",
			ReportType: report,
		};

		return axios
			.post(
				"https://" + this.credential.url + "/Reports/2009-01-01",
				null,
				{
					params: this.sign(params)
				}
			)
			.then(response => response.data)
			.then(parseXml);
	},

	requestReportId(id) {

		const params = {
			Version: "2009-01-01",
			Acknowledged: false,
			Action: "GetReportRequestList",
			"ReportRequestIdList.Id.1": id
		};

		return axios
			.post(
				"https://" + this.credential.url + "/Reports/2009-01-01",
				null,
				{
					params: this.sign(params)
				}
			)
			.then(response => response.data)
			.then(parseXml);
	},

	getReportList(reportType = []) {

		if (typeof reportType === "string")
			reportType = [reportType];

		const params = reportType
			.reduce((prev, curr, index) => {
				prev["ReportTypeList.Type." + (index + 1)] = curr;
				return prev;
			}, {
				Action: "GetReportList",
				MaxCount: 100,
				Acknowledged: false
			});

		return axios
			.post(
				"https://" + this.credential.url + "/Reports/2009-01-01",
				null,
				{
					params: this.sign(params)
				}
			)
			.then(response => response.data)
			.then(parseXml)
			.then(response => {
				response = response.GetReportListResponse.GetReportListResult;
				response.HasNext = response.HasNext == "true";
				response.ReportInfo = (response.ReportInfo || []).map(item => {
					item.AvailableDate = moment(item.AvailableDate).toDate();
					return item;
				});
				return response;
			});
	},

	getReportListByNextToken(reportType, nextToken) {

		const params = {
			Action: "GetReportListByNextToken",
			["ReportTypeList.Type.1"]: reportType,
			NextToken: nextToken,
			Acknowledged: false
		};

		return axios
			.post(
				"https://" + this.credential.url + "/Reports/2009-01-01",
				null,
				{
					params: this.sign(params)
				}
			)
			.then(response => response.data)
			.then(parseXml)
			.then(response => {
				response = response.GetReportListByNextTokenResponse.GetReportListByNextTokenResult;
				response.HasNext = response.HasNext == "true";
				response.ReportInfo = (response.ReportInfo || []).map(item => {
					item.AvailableDate = moment(item.AvailableDate).toDate();
					return item;
				});
				return response;
			});
	}


};
