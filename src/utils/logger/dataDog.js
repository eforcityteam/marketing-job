"use strict";
const util = require("util");
const AWS = require("aws-sdk");
const Transport = require("winston-transport");
const packageJson = require("../../package.json");
const isDocker = require("../isDocker");
const os = require("os");

const lambda = new AWS.Lambda({ region: "us-west-2" });

function DataDog(options = {}) {
	options.objectMode = true;
	Transport.call(this, options);
}

DataDog.prototype.log = async function (info, callback) {
	await lambda
		.invoke({
			FunctionName: "logging-production-log",
			InvocationType: "Event",
			Payload: JSON.stringify({
				payload: info,
				hostname: process.env.HOST || "localhost",
				service: process.env.APPLICATION,
				env:process.env.NODE_ENV,
				source: process.env.SOURCE || "nodejs",
				tags: packageJson.keywords,
				...(isDocker() && { ["container_id"]: os.hostname()}),
			})
		})
		.promise();
	return callback();
};

util.inherits(DataDog, Transport);
DataDog.prototype.name = "DataDogLogger";

module.exports = DataDog;
