"use strict";
const path = require("path");
const colors = require("colors");
const winston = require("winston");

const { format } = winston;
const dataDog = require("./dataDog");

const logger = winston.createLogger({
	level: "debug",
	levels: winston.config.npm.levels,
	format: winston.format.combine(
		format.splat(),
		format.errors({ stack: true }),
		format.timestamp(),
		format(info => {
			if (info.label && typeof info.message === "string") {
				info.message = `${colors.grey(`[${info.label}]`)} ${info.message}`;
			}
			return info;
		})()
	),
	transports: [
		new dataDog({ level: "info" }),
		new winston.transports.Console({
			level: "silly",
			format: format.combine(
				format.colorize(),
				format.printf(info => {
					let result = `${info.level} ${info.message}${colors.gray(" - " + info.timestamp)} `;

					if (info.payload) {
						result += `\n${
							typeof info.payload === "object" ?
								JSON.stringify({ payload: info.payload }, null, 4) :
								info.payload.toString()
							}`
					}
					
					if (info.stack) {
						const pwd = path.resolve(process.env.PWD || process.cwd());
						result += (
							"\n" +
							info
								.stack
								.split("\n")
								.map((line, index) => {
									if (index == 0 || (!line.includes("node_modules") && line.includes(pwd)))
										return line;
									return colors.grey(line);
								})
								.join("\n")
						);
					}

					return result;
				})
			),
		}),
	]
});

module.exports = logger;
