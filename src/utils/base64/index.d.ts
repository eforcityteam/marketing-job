/**
 * 
 * Base64 Encode and Decode
 * 
 */

/**
 * Encode string to base64 string
 * @param string string to encode
 * @return {string} encoded base64 string
 */
export function encode(string:string): string;

/**
 * Decode the base64 string to normal string
 * @param base64 
 * @return {string} original string
 */
export function decode(base64:string): string;
