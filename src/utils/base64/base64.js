"use strict";

function encode(string){
	return Buffer.from(string).toString("base64");
}

function decode(base64){
	return Buffer.from(base64 , "base64").toString("ascii");
}

module.exports = encode;
module.exports.encode = encode;
module.exports.decode = decode;
