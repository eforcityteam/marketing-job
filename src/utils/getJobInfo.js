"use strict";
const uuid = require("uuid").v4;
const Bull = require("bull");

function getJobInfo(args = []) {
    const argsArray = Array.prototype.slice.call(args);
    const job = argsArray.find(arg => arg instanceof Bull.Job);
    return {
        jobId: job ? job.id : uuid(),
        action: job ? job.name : ""
    }
};

module.exports = getJobInfo;