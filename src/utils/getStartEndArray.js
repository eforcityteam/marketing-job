"use strict";
const moment = require("moment");

function get(start, end, overlap = 1, interval = 1) {

	start = moment(start);
	end = moment(end);

	let array = [];

	while (start.isBefore(end)) {
		const _start = start.toDate();
		start = start.add(interval, "days");
		array.push({
			start: _start,
			end: start.subtract(overlap, "minutes").toDate()
		});
	}

	return array;
}

module.exports = get;
