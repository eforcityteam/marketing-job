"use strict";
const knex = require("knex");
const getConfig = require("../config");

const getDatabase = (
	() => {
		let db = {};
		return async function (key = "efcDb") {
			if (db[key]) {
				return db[key];
			}
			const config = await getConfig();
			db[key] = knex(config.database[key]);
			return db[key];
		}
	}
)();

module.exports = getDatabase;
