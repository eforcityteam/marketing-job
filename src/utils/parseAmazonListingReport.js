"use strict";
const moment = require("moment");
const csvtojson = require("csvtojson");

function parse(csv) {

	return csvtojson({ delimiter: "\t" })
		.fromString(csv)
		.then(rows => rows.map(row => {
			return {
				ASIN: row.asin1,
				Title: row["item-name"],
				Description: row["item-description"],
				ListingId: row["listing-id"],
				SKU: row["seller-sku"],
				Price: row["price"],
				Quantity: row["quantity"],
				OpenDate: moment(row["open-date"], "YYYY-MM-DD hh:mm:ss").toDate(),
				ItemIsMarketplace: row["item-is-marketplace"],
				Note: row["item-note"],
				FulfillmentChannel: row["fulfillment-channel"],
				Status: row.status
			};
		}));
}

module.exports = parse;
