"use strict";
const MySql = require("mysql-plugin");

async function get(ids) {

	const accounts = await MySql
		.query(`
			SELECT 
				CredentialId as id, 
				FullName as name,
				Remark as platform
			FROM AuthDb.ProgramCredential 
			WHERE CredentialId IN (?) AND RecStatus = "A";
			SELECT 
				CredentialId as id, 
				MetaKey as \`key\`,
				MetaValue as \`value\`
			FROM AuthDb.ProgramCredentialMeta 
			WHERE CredentialId IN (
				SELECT CredentialId as id
				FROM AuthDb.ProgramCredential 
				WHERE CredentialId IN (?) AND RecStatus = "A"
			);
		`, [ids, ids])
		.then(response => {
			const [accounts, meta] = response;
			return accounts
				.map(account => {
					account.meta = meta
						.filter(item => item.id == account.id)
						.reduce((prev, curr) => {
							prev[curr.key] = curr.value;
							return prev;
						}, {});
					return account;
				});
		});

	return accounts;
}

module.exports = get;
