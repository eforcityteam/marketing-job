"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");

const ssm = new AWS.SSM({
	region: "us-west-2",
	useAccelerateEndpoint: process.env.NODE_ENV === "production"
});

function getStores(keys) {

	return map(
		keys,
		async key => {
			let response;
			let items = [];
			do {
				response = await ssm
					.getParametersByPath({
						Path: key,
						WithDecryption: true,
						Recursive: true,
						NextToken: response?.NextToken
					})
					.promise();
				items = [].concat(items, response.Parameters);
			} while (response?.NextToken);
			return items;
		})
		.then(rows => rows.flat().reduce((prev, curr) => {
			const keys = curr.Name.split("/");
			if (keys[1] == "common")
				prev[_.last(keys)] = curr.Value;
			else
				prev[keys.filter(item => item != "").join("-")] = curr.Value;
			return prev;
		}, {}));
}

module.exports = getStores;
