"use strict";
const MySql = require("mysql-plugin");

function getASINBySKU(SKUs) {
	return MySql
		.query(`
			SELECT 
				map.ProductCode as SKU,
				amaz.ProductCode as ASIN
			FROM ProductDb.ProductCodeMap map
			LEFT JOIN ProductDb.ProductCodeMap amaz
			ON map.ProductId = amaz.ProductId
			WHERE map.ProductCodeClassId = "EFC" 
				AND map.ProductCodeType = "SKU" 
				AND amaz.ProductCodeClassId = "AMAZ" 
				AND map.ProductCode IN (?);
		`, [
			SKUs
		])
		.reduce((prev, curr) => {
			prev[curr.SKU] = curr.ASIN;
			return prev;
		}, {});
}

module.exports = getASINBySKU;
