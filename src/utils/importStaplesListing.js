"use strict";

const _ = require("lodash");
const map = require("p-map");
const MySql = require("mysql-plugin");
const exceltojson = require("convert-excel-to-json");
const logger = require("../utils/logger");
const updateListingTitleFromSync = require("../utils/updateListingTitleFromSync");

const PLATFORM_MAP = {
	"StaplesA": "SPLSA-62",
	"Quill": "QUILL-61",
	"Staples": "SPLS-60"
};

const COLUMNS = [
	"MarketPlatformId", "ProductListingId", "MerchantSKU",
	"ProductTitle", "Availability",
	"ExpectedInStockDate", "InsertDate",
	"UnitCost"
];

const DETAIL_COLUMNS = [
	"ListingId",
	"LaunchDate",
	"MarketplaceListingId",
	"ProductId",
	"Title",
	"ListingStatus"
];

async function insert(buffer) {

	logger.debug("[importStaplesListing] Reading Staples file ... ");

	const excels = exceltojson({ source: buffer });

	await MySql.query(`TRUNCATE TABLE TempDb.LMSRawListingStaples;`);

	for (const tab in excels) {

		logger.debug("[importStaplesListing] Running on #" + PLATFORM_MAP[tab]);

		const rows = _
			.drop(excels[tab], 5)
			.map(row => [PLATFORM_MAP[tab], row["A"], row["D"], row["F"], row["L"], row["N"], row["V"], row["K"]]);

		await map(
			_.chunk(rows, 5000),
			async (rows, index) => {
				logger.debug("[importStaplesListing] Running on chunk #" + (index + 1));
				await MySql.query(`
					INSERT INTO TempDb.LMSRawListingStaples (??) 
					VALUES ?
					ON DUPLICATE KEY UPDATE ${COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")}
				`, [
					COLUMNS,
					rows
				]);
			},
			{
				concurrency: 1
			}
		);
	}

	await MySql.query(`
		REPLACE INTO LMSDb.RawListingStaples(
			??
		)
		SELECT 
			??
		FROM TempDb.LMSRawListingStaples;

		UPDATE LMSDb.RawListingStaples staples
		LEFT JOIN TempDb.LMSRawListingStaples temp ON staples.MarketPlatformId = temp.MarketPlatformId and staples.ProductListingId = temp.ProductListingId
		SET staples.RecStatus = IF(temp.ProductListingId IS NULL, "D", temp.RecStatus);
	`, [
		COLUMNS,
		COLUMNS
	]);

	/**
	 * 
	 * Update listing table's product ID
	 * 
	 */
	await MySql.query(`
		UPDATE 
			RawListingStaples listing,
			ProductDb.ProductCodeMap map
		SET listing.ProductId = map.ProductId
		WHERE 
			map.ProductCode = listing.ProductListingId 
				AND 
			map.ProductCodeType = "AC" 
				AND 
			map.ProductCodeClassId = "EFC" 
				AND 
			listing.ProductId IS NULL;
	`);


	logger.debug("[importStaplesListing] Creating listing ... ");

	await MySql.query(`
		INSERT INTO LMSDb.Listing (
			MarketPlatformId, ProductListingId, ListingStatusId
		)
		SELECT 
			MarketPlatformId, 
			ProductListingId, 
			IF(Availability = "Available", "ACTIVE", IF(Availability = "Discontinued", "CLOSE", "UNPUB")) as ListingStatusId 
		FROM RawListingStaples
		ON DUPLICATE KEY UPDATE ListingStatusId = VALUES(ListingStatusId);
	`);

	logger.debug("[importStaplesListing] Updating listings' publish status ... ");

	await MySql.query(`
			UPDATE 			
				Listing listing 			
			LEFT JOIN 
				RawListingStaples staples 				
					ON 			
				listing.MarketPlatformId = staples.MarketPlatformId
					AND 			
				listing.ProductListingId = staples.ProductListingId 		
			SET  			
				listing.PublishStatus = IF(
						staples.ProductListingId IS NULL OR staples.RecStatus = 'D', 
							"DELETED",
							IF(staples.DiscontinuedDate IS NOT NULL, "PUBLISHED", "UNPUBLISHED")
						) 		
			WHERE  			
				listing.MarketPlatformId IN ("SPLSA-62","QUILL-61","SPLS-60");
	`)

	logger.debug("[importStaplesListing] Updating listing detail ... ");

	await MySql.query(`
		INSERT INTO ListingDetail (??)
		SELECT 
			listing.ListingId,
			staples.InsertDate,
			MerchantSKU as MarketplaceListingId,
			staples.ProductId,
			ProductTitle,
			detail.RecStatus as ListingStatus
		FROM RawListingStaples staples
		INNER JOIN Listing listing
		ON staples.ProductListingId = listing.ProductListingId
		LEFT JOIN ErpDb.ProductListingDetail detail
		ON detail.ProductListingId = listing.ProductListingId
		WHERE listing.MarketPlatformId IN ("SPLSA-62","QUILL-61","SPLS-60")
		ON DUPLICATE KEY UPDATE ${DETAIL_COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")}
	`, [DETAIL_COLUMNS]);

	logger.debug("[importStaplesListing] Updating listing title ... ");

	await updateListingTitleFromSync(["SPLSA-62","QUILL-61","SPLS-60"]);

	await MySql.query(`
		INSERT INTO ListingPrice(ListingId, Price)
		SELECT ListingId, UnitCost
		FROM RawListingStaples staples
		INNER JOIN Listing listing
		ON listing.MarketPlatformId = staples.MarketPlatformId AND staples.ProductListingId = listing.ProductListingId
		ON DUPLICATE KEY UPDATE Price = VALUES(Price);
	`);
	
	logger.debug("[importStaplesListing] Program completed");
	return true;
}

module.exports = insert;
