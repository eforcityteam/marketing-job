"use strict";
const MySql = require("mysql-plugin");
const mem = require("mem");

function getRates() {
	return MySql
		.query(`
			SELECT currency, rate
			FROM ReportDb.ExchangeRate
			RIGHT JOIN (
				SELECT MAX(CONVERT(CONCAT_WS("-", Year, Month, Day), Date)) as date 
				FROM ReportDb.ExchangeRate
			) latest
			ON 
				year = YEAR(latest.date) 
				AND month = MONTH(latest.date) 
				AND day = day(latest.date) 
		`)
		.then(rows => Object.fromEntries(rows.map(item => [item.currency, item.rate])));
}

getRates = mem(getRates, { maxAge: "3 hours" });

async function get(price, from = "USD", to = "USD") {

	if (from == to)
		return price;

	const RATES = await getRates();

	if (!RATES[from] || !RATES[to])
		throw new Error("Invalid currency. ");

	return price * (1 / RATES[from]) * RATES[to];
}

module.exports = get;
module.exports.getRates = getRates;
