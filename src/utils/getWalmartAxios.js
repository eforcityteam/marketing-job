"use strict";
const uuid = require("uuid");
const AWS = require("aws-sdk");
const axios = require("axios");
const qs = require("querystring");
const logger = require("../utils/logger");
const ssm = new AWS.SSM({ region: "us-west-2", useAccelerateEndpoint: true });

async function getAgent() {

	const token = await ssm
		.getParameter({ Name: "/platform/walmart/token", WithDecryption: true })
		.promise()
		.then(item => item?.Parameter?.Value);

	logger.debug("[Walmart] Getting walmart credential token");

	const agent = axios.create({
		baseURL: "https://marketplace.walmartapis.com/v3",
		headers: {
			["WM_SVC.NAME"]: "Walmart Marketplace",
			["WM_QOS.CORRELATION_ID"]: uuid.v4(),
			Accept: "application/json",
			Authorization: "Basic " + token
		}
	});

	logger.debug("[Walmart] Getting walmart access token");

	const response = await agent.post("/token", qs.stringify({ grant_type: "client_credentials" }));
	agent.defaults.headers["WM_SEC.ACCESS_TOKEN"] = response.data.access_token;

	return agent;
}

module.exports = getAgent;
