"use strict";
const qs = require("qs");
const _ = require("lodash");
const crypto = require("crypto");

function sign(type, method, params, endpoint, secret) {
	
	const sorted = _.reduce(_.keys(params).sort(), function (m, k) {
		m[k] = params[k];
		return m;
	}, {});
	
	const path = "/" + type + "/" + params.Version;
	
	const stringToSign = [
		method,
		endpoint,
		path,
		qs.stringify(sorted)
	]
		.join("\n")
		.replace(/"/g, "%27")
		.replace(/\*/g, "%2A")
		.replace(/\(/g, "%28");

	return crypto
		.createHmac("sha256", secret)
		.update(stringToSign, "utf8")
		.digest("base64");
}

module.exports = sign;
