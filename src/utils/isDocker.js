let isDockerCached = process.env.DOCKER_ENV;

function hasDockerEnv() {
	try {
		fs.statSync('/.dockerenv');
		return true;
	} catch {
		return false;
	}
}

function hasDockerCGroup() {
	try {
		return fs.readFileSync('/proc/self/cgroup', 'utf8').includes('docker');
	} catch {
		return false;
	}
}

function isDocker() {
	if (!isDockerCached || isDockerCached === undefined) {
		isDockerCached = hasDockerEnv() || hasDockerCGroup();
	}
	return isDockerCached;
}

module.exports = isDocker