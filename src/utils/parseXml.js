"use strict";
const xml2js = require("xml2js");
const bluebird = require("bluebird");

const parser = new xml2js.Parser({
	explicitArray: false,
	ignoreAttrs: true,
	tagNameProcessors: [xml2js.processors.stripPrefix]
});

const parseString = bluebird.promisify(parser.parseString);

module.exports = parseString;
