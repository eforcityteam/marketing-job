"use strict";
const getDatabase = require("../utils/getDatabase");

async function update(platforms) {

	const knex = await getDatabase();

	return knex
		.from(knex.raw("?? (??, ??)", ["LMSDb.ListingTitle", "ListingId", "ListingTitle"]))
		.insert(
			knex
				.select("ListingId", "Title")
				.from(("LMSDb.ListingDetail"))
				.whereIn(
					"ListingId",
					knex
						.select("ListingId")
						.from(("LMSDb.Listing"))
						.whereIn("MarketPlatformId", platforms)
				)
				.andWhere("Title", "<>", "")
		)
		.onConflict()
		.merge(["ListingTitle"]);

}

module.exports = update;
