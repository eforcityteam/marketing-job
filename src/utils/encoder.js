"use strict";
const crypto = require("crypto");

if (process.env.SECRET === undefined)
	throw new Error("Missing Secret");

function encode(data) {
	const cipher = crypto.createCipher("aes-256-ctr", process.env.SECRET);
	let crypted = cipher.update(data, "utf8", "hex");
	crypted += cipher.final("hex");
	return crypted;
}

function decode(data) {
	const decipher = crypto.createDecipher("aes-256-ctr", process.env.SECRET);
	let dec = decipher.update(data, "hex", "utf8");
	dec += decipher.final("utf8");
	return dec;
}

module.exports = { encode, decode };
