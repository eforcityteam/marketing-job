"use strict";

const _ = require("lodash");
const map = require("p-map");
const MySql = require("mysql-plugin");
const exceltojson = require("convert-excel-to-json");
const logger = require("../utils/logger");
const updateListingTitleFromSync = require("../utils/updateListingTitleFromSync");

const COLUMNS = [
	"SKU",
	"ProductListingId",
	"ProductTitle",
	"ProductStatus",
	"ItemCreateDate",
	"FirstOnsiteDate",
	"Price",
	"Category",
	"FullCategory"
];

const DETAIL_COLUMNS = [
	"ListingId",
	"LaunchDate",
	"MarketplaceListingId",
	"Category",
	"ProductId",
	"Title",
	"ListingStatus"
];

async function insert(buffer) {

	logger.debug("[importOverstockListing] Reading overstock file ... ");

	const rows = await Promise
		.resolve()
		.then(() => exceltojson({ source: buffer })["Product Information Details"])
		.then(rows => rows.map(item => [
			item["A"], item["B"], item["D"], item["G"],
			item["M"], item["N"],
			item["P"],
			item["AO"],
			[item["AL"], item["AM"], item["AN"], item["AO"]].filter(item => item && item != "").join(" > ")
		]));

	rows.shift();

	logger.debug("[importOverstockListing] Inserting into overstock table ... ");

	await map(
		_.chunk(rows, 10000),
		async (rows, index) => {

			logger.debug("[importOverstockListing] Running on chunk #" + (index + 1));

			const sql = MySql.format(`
				INSERT INTO RawListingOverStock (??) 
				VALUES ? 
				ON DUPLICATE KEY UPDATE ${COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")}
			`, [
				COLUMNS,
				rows
			]);

			await MySql.query(sql);
		},
		{
			concurrency: 2
		}
	);

	await MySql.query(`
		UPDATE 
			RawListingOverStock listing,
			ProductDb.ProductCodeMap map
		SET listing.ProductId = map.ProductId
		WHERE 
			map.ProductCode = listing.ProductListingId 
				AND 
			map.ProductCodeType = "AC" 
				AND 
			map.ProductCodeClassId = "EFC" 
				AND 
			listing.ProductId IS NULL;
	`);

	logger.debug("[importOverstockListing] Creating listing ... ");

	await MySql.query(`
		INSERT IGNORE INTO LMSDb.Listing (
			MarketPlatformId, ProductListingId, ListingStatusId
		)
		SELECT 
			"OVER-O" as MarketPlatformId, 
			ProductListingId, 
			"ACTIVE" as ListingStatusId 
		FROM RawListingOverStock;
	`);

	logger.debug("[importOverstockListing] Updating listing detail ... ");

	await MySql.query(`
		INSERT INTO ListingDetail (??)
		SELECT 
			listing.ListingId,
			FirstOnsiteDate,
			SKU as MarketplaceListingId,
			Category,
			overStock.ProductId,
			ProductTitle,
			detail.RecStatus as ListingStatus
		FROM RawListingOverStock overStock
		INNER JOIN Listing listing
		ON overStock.ProductListingId = listing.ProductListingId 
		LEFT JOIN ProductDb.ProductCodeMap map
		ON map.ProductId = overStock.ProductId AND map.ProductCodeType = "SKU" AND map.ProductCodeClassId = "EFC"
		LEFT JOIN ErpDb.ProductListingDetail detail
		ON detail.ProductListingId = listing.ProductListingId
		WHERE listing.MarketPlatformId = "OVER-O"
		ON DUPLICATE KEY UPDATE ${DETAIL_COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")}
	`, [DETAIL_COLUMNS]);

	logger.debug("[importOverstockListing] Updating listing title ... ");

	await updateListingTitleFromSync(["OVER-O"]);

	await MySql.query(`
		UPDATE 
			Listing listing,
			RawListingOverStock overStock
		SET listing.ListingStatusId = IF(overStock.RecStatus = "A", "ACTIVE", "CLOSE")
		WHERE MarketPlatformId = "OVER-O" AND listing.ProductListingId = overStock.ProductListingId;
	`);
	
	logger.debug("[importOverstockListing] Program completed");
	return true;
}

module.exports = insert;
