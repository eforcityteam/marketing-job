"use strict";
const axios = require("axios");
const UUIDv4 = require("uuid").v4;
const logger = require("../utils/logger");
const _ = require("lodash");

const FILTER_KEYS = ["headers.Authorization", "params.access_token", "data", "job"];

function createAgent({ account, config, tokenPath = [] }) {

    const agent = axios.create(config);

    const marketPlatform = account.marketPlatform || account.platform || account.name || account.id;
    const job = config && config.job ? config.job : { job_id: "", action: "" };

    agent.interceptors.request.use((config) => {

        const customUuid = UUIDv4();
        config.traceId = customUuid;
        const payload = {
            job_id: job.job_id,
            trace_id: customUuid,
            action: job.action,
            market_platform: marketPlatform,
            config: _.cloneDeep(_.omit(config, FILTER_KEYS.concat(tokenPath)))
        };

        logger.info({
            message: `${config.method.toUpperCase()} ${config.baseURL + config.url}`,
            payload,
        });

        return config;
    });

    agent.interceptors.response.use((response) => {
        const customUuid = response.config && response.config.traceId ? response.config.traceId : "";
        const job = response.config && response.config.job ? response.config.job : { job_id: "", action: "" };
        const payload = {
            job_id: job.job_id,
            trace_id: customUuid,
            action: job.action,
            market_platform: marketPlatform,
            status: response.status,
            headers: response.headers,
        };

        logger.info({
            message: `Response received from trace_id: ${customUuid}`,
            payload,
        });

        return response;
    }, (error) => {
        const customUuid = error.response && error.response.config && error.response.config.traceId ? error.response.config.traceId : "";
        const job = error.response && error.response.config && error.response.config.job ? error.response.config.job : { job_id: "", action: "" };

        const errorResponse = error.response ? error.response : {
            status: null,
            data: null,
            headers: null
        };

        const payload = {
            job_id: job.job_id,
            trace_id: customUuid,
            action: job.action,
            market_platform: marketPlatform,
            status: errorResponse.status,
            data: errorResponse.data,
            headers: errorResponse.headers,
        };

        logger.error({
            message: `Error occurred from trace_id: ${customUuid}`,
            payload
        });

        return Promise.reject(error);
    })

    return agent;
}

module.exports = createAgent;