/**
 * 
 * Validate and sort the images set according to the target image requirement:
 * 
 * 1st image restriction:
 * 1. NO Background.
 * 2. NO Info-graphic.
 * 
 * Choice of sequence for the 1st image:
 * 1. Image without tag.
 * 2. Lifestyle without background & info graphic.
 * 3. Packaging without background & info graphic.
 * 4. Product Components without background & info graphic.
 * 5. Zoom in without background & info graphic.
 * 
 * Additional image restriction:
 * 1. NO Info graphic.
 * 
 * Choice of sequence for the rest of the images:
 * 1. It’s based on the order of the images in CMS.
 * 
 */
"use strict";
const Immutable = require("immutable");

function validate(images) {

	if (!images || images.length < 1)
		return [];

	let $images = Immutable.List(images);

	/**
	 * 
	 * Additional image restriction:
	 * 1. NO Info graphic.
	 */
	$images = $images.filter(item => !item.attributes.includes("IG"));

	/**
	 * 
	 * 1st image restriction:
	 * 1. NO Background
	 * 2. NO Info graphic (Which is filtered in the previous step) 
	 * 
	 */
	let $main = $images.filter(item => !item.attributes.includes("BG"));

	if ($main.$size < 1)
		throw new Error("Can't found a clear image for the first image");

	if ($main.size > 1)
		$main = $main.sort(
			(prev, curr) => getPriority(prev) - getPriority(curr)
		);

	/**
	 * 
	 * Move the main images into first of the image array
	 * 
	 */
	const main = $main.first();

	if (!main)
		return [];

	return $images
		.filter(item => item && item.hash != main.hash)
		.unshift(main)
		.toJSON();
}

function getPriority(image) {

	// 1. Image without tag.
	if (image.attributes.length < 1)
		return 1;

	// 2. Lifestyle without background & info graphic.
	if (image.attributes.includes("LS") && !image.attributes.includes("BG"))
		return 2;

	// 3. Packaging without background & info graphic.
	if (image.attributes.includes("PK") && !image.attributes.includes("BG"))
		return 3;

	// 4. Product Components without background & info graphic.
	if (image.attributes.includes("PC") && !image.attributes.includes("BG"))
		return 4;

	// 5. Zoom in without background & info graphic.	
	if (image.attributes.includes("ZI") && !image.attributes.includes("BG"))
		return 5;

	return 100;
}

module.exports = validate;
