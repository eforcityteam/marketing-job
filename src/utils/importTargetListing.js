"use strict";
const _ = require("lodash");
const map = require("p-map");
const retry = require("p-retry");
const MySql = require("mysql-plugin");
const exceltojson = require("convert-excel-to-json");
const logger = require("../utils/logger");
const updateListingTitleFromSync = require("../utils/updateListingTitleFromSync");

const COLUMNS = [
	"TCIN", "SKU", "ProductListingId", "ProductTitle",
	"ItemType", "ItemTypeID", "RelationShip", "PublishStatus", "DataUpdatesStatus",
	"Price", "OfficePrice", "MapPrice",
	"PriceLastUpdated", "Inventory", "InventoryLastUpdated"
];

const DETAIL_COLUMNS = [
	"ListingId",
	"Category",
	"MarketplaceListingId",
	"ProductId",
	"Title",
	"ListingStatus"
];

async function insert(buffer) {

	let rows = exceltojson({ source: buffer })["Inventory"];

	rows.shift();

	rows = rows.map(item => [
		item.A, item.B, item.B.split("_")[0], item.C,
		item.D, item.E, item.F, item.G, item.H,
		item.I, item.J, item.K,
		item.M, item.P, item.Q
	]);

	await map(
		_.chunk(rows, 5000),
		async (rows, index) => {
			logger.debug("[importTargetListing] Running on chunk #" + (index + 1));
			await retry(
				() => MySql.query(`
					INSERT INTO TempDb.LMSRawListingTarget (??) 
					VALUES ?
					ON DUPLICATE KEY UPDATE ${COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")}
				`, [
					COLUMNS,
					rows
				]),
				{
					retries: 5
				}
			);
		},
		{
			concurrency: 1
		}
	);

	await MySql.query(`
		REPLACE INTO LMSDb.RawListingTarget(
			??
		)
		SELECT 
			??
		FROM TempDb.LMSRawListingTarget;
		
		UPDATE LMSDb.RawListingTarget target
		LEFT JOIN TempDb.LMSRawListingTarget temp ON target.TCIN = temp.TCIN
		SET target.RecStatus = IF(temp.TCIN IS NULL, "D", temp.RecStatus);
	`, [
		COLUMNS,
		COLUMNS
	]);

	/**
	 * 
	 * Update listing table's product ID
	 * 
	 */
	await retry(
		() => MySql.query(`
			UPDATE 
				RawListingTarget listing,
				ProductDb.ProductCodeMap map
			SET listing.ProductId = map.ProductId
			WHERE 
				map.ProductCode = listing.ProductListingId 
					AND 
				map.ProductCodeType = "AC" 
					AND 
				map.ProductCodeClassId = "EFC" 
					AND 
				listing.ProductId IS NULL;
		`),
		{
			retries: 5
		}
	);

	logger.debug("[importTargetListing] Creating listing ... ");

	await retry(
		() => MySql.query(`
			INSERT INTO LMSDb.Listing (
				MarketPlatformId, ProductListingId, ReferenceNum, ListingStatusId
			) 
			SELECT "TARGET-96", ProductListingId, SKU, "ACTIVE" FROM RawListingTarget
			ON DUPLICATE KEY UPDATE ProductListingId = VALUES(ProductListingId)
		`),
		{
			retries: 5
		}
	);

	logger.debug("[importTargetListing] Updating listing publish status ... ");

	await retry(
		() =>
			MySql.query(`
			UPDATE 			
				Listing listing 			
			LEFT JOIN 
				RawListingTarget target  				
					ON 			
				listing.MarketPlatformId = "TARGET-96" 				
					AND 			
				listing.ReferenceNum = target.SKU 		
			SET  			
				listing.PublishStatus = IF(
						target.SKU IS NULL OR target.RecStatus = 'D', 
							"DELETED",
							IF(target.PublishStatus = "YES", "PUBLISHED", "UNPUBLISHED")
						) 		
			WHERE  			
				listing.MarketPlatformId = "TARGET-96";
		`),
		{
			retries: 5,
		}
	);

	logger.debug("[importTargetListing] Updating listing detail ... ");

	await retry(
		() => MySql.query(`
			INSERT INTO ListingDetail (??)
			SELECT 
				listing.ListingId,
				target.ItemType,
				target.TCIN as MarketplaceListingId,
				map.ProductId,
				target.ProductTitle,
				detail.RecStatus as ListingStatus
			FROM RawListingTarget target
			INNER JOIN Listing listing
			ON target.ProductListingId = listing.ProductListingId
			LEFT JOIN ErpDb.ProductListingDetail detail
			ON detail.ProductListingId = listing.ProductListingId
			LEFT JOIN ProductDb.ProductPrimaryCode map
			ON 
				map.ProductCode = detail.ProductId 
					AND 
				map.ProductCodeClassId = "EFC" 
					AND 
				map.ProductCodeTypeId = "SKU"
			WHERE listing.MarketPlatformId = "TARGET-96"
			ON DUPLICATE KEY UPDATE ${DETAIL_COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")}
		`, [
			DETAIL_COLUMNS
		]),
		{
			retries: 5
		}
	);

	await MySql.query(`
		INSERT INTO LMSDb.ListingPrice (ListingId, Price, UserId)
		SELECT listing.ListingId, target.OfficePrice, "CronSyncListing"
		FROM LMSDb.RawListingTarget target
		LEFT JOIN LMSDb.Listing listing
		ON listing.ReferenceNum = target.SKU 
		WHERE listing.MarketPlatformId = "TARGET-96" AND target.OfficePrice IS NOT NULL
		ON DUPLICATE KEY UPDATE Price=VALUES(Price), UserId = VALUES(UserId);
	`);

	await MySql.query(
		"UPDATE ListingDetail SET MarketplaceListingId = NULL WHERE MarketplaceListingId = \"\""
	);

	await updateListingTitleFromSync(["TARGET-96"]);

	logger.debug("[importTargetListing] Completed");
	return true;
}

module.exports = insert;
