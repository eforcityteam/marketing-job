"use strict";
const _ = require("lodash");

function quickMerge(object) {
	return _.assign(
		{},
		Object
			.keys(object)
			.filter(item => item[0].toLowerCase() == item[0] || !item.includes("_"))
			.reduce((prev, curr) => {
				prev[curr] = object[curr];
				return prev;
			}, {}),
		Object
			.keys(object)
			.filter(item => item[0].toUpperCase() == item[0] && item.includes("_"))
			.map(key => ({
				key,
				attributes: key.split("_")[1],
				field: _.camelCase(key.split("_")[0])
			}))
			.reduce((prev, curr) => {
				if (curr.attributes == "id" && !object[curr.key])
					return prev;
				(prev[curr.field] = prev[curr.field] || {})[curr.attributes] = object[curr.key];
				return prev;
			}, {})
	);
}

module.exports = quickMerge;
