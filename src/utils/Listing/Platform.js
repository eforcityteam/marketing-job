"use strict";
const logger = require("../logger");

function Platform() {
	if (this.constructor.name === "Platform")
		throw new Error("You shouldn't create instance from Platform interface");

	this.name = this.constructor.name;
	this.taskId = "CronSyncListing";
}

Platform.prototype.process = async function () {

	/**
	 * 
	 * Create the unique listing id or update the listing status id.
	 * Remark field includes the publish failed reason.
	 * 
	 */
	logger.debug(`[${this.name}] Creating listing`);
	await this.createListing();


	/**
	 * 
	 * Update the listing detail, including 
	 * 
	 */
	logger.debug(`[${this.name}] Updating listing detail`);
	await this.updateDetail();

	/**
	 * 
	 * 
	 * 
	 */
	logger.debug(`[${this.name}] Updating listing price`);
	await this.updatePrice();

	/**
	 * 
	 * 
	 * 
	 */
	logger.debug(`[${this.name}] Updating listing inventory`);
	await this.updateInventory();

	await this.updateCreateQueue();

	logger.debug(`[${this.name}] Program finished`);
	return true;
};


Platform.prototype.createListing = function () {
	logger.warn(`[${this.name}] Create Listing hasn't been implemented`);
};

Platform.prototype.updateDetail = function () {
	logger.warn(`[${this.name}] Update Detail hasn't been implemented`);
};

Platform.prototype.updatePrice = function () {
	logger.warn(`[${this.name}] Update Price hasn't been implemented`);
};

Platform.prototype.updateInventory = function () {
	logger.warn(`[${this.name}] Update Inventory hasn't been implemented`);
};

Platform.prototype.updateCreateQueue = function () {
	// return MySql.query(`
	// 	UPDATE ProcQueueListing 
	// 	SET RecStatus = "C" 
	// 	WHERE QueueId IN (
	// 		SELECT QueueId FROM (
	// 			SELECT QueueId
	// 			FROM ProcQueueListing listing
	// 			WHERE 
	// 				RecStatus <> "C" 
	// 				AND 
	// 				NOT EXISTS (
	// 					SELECT * 
	// 					FROM ProcQueueListingLine line 
	// 					LEFT JOIN ProcQueueListing header
	// 					ON line.QueueId = header.QueueId
	// 					LEFT JOIN Listing listing
	// 					ON listing.MarketPlatformId = header.MarketPlatformId AND listing.ProductListingId = line.SKU
	// 					WHERE line.QueueId = header.QueueId AND ListingStatusId = "PENDCR"
	// 				)
	// 			) ids
	// 		)
	// `);
};

module.exports = Platform;
