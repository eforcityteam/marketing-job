"use strict";
const util = require("util");
const retry = require("p-retry");
const MySql = require("mysql-plugin");
const Platform = require("./Platform");

function EBay() {
	Platform.call(this);
}

EBay.prototype.createListing = function () {
	return retry(
		() => MySql.query(`
			INSERT INTO LMSDb.Listing (
				MarketPlatformId, ProductListingId, ReferenceNum, 
				ListingStatusId, Remark, UserId
			)
			SELECT 
				MarketPlatformId, 
				ProductListingId,
				SKU as ReferenceNum,
				"CLOSE" as ListingStatusId,
				EndingReason as Remark,
				"CronSyncListing"
			FROM RawListingEBay inactives 
			WHERE 
				ListingStatus <> "Active"
					AND 
				NOT EXISTS (
					SELECT actives.SKU 
					FROM RawListingEBay actives 
					WHERE 
						actives.MarketPlatformId = inactives.MarketPlatformId 
							AND
						actives.SKU = inactives.SKU 
							AND 
						ListingStatus = "Active"
					)
			UNION ALL
			SELECT 			
				MarketPlatformId, 
				ProductListingId,
				SKU as ReferenceNum,
				"ACTIVE" as ListingStatusId,
				EndingReason as Remark,
				"CronSyncListing" 
			FROM RawListingEBay actives 
			WHERE actives.SKU AND ListingStatus = "Active"
			ON DUPLICATE KEY UPDATE ListingStatusId=VALUES(ListingStatusId), UserId=VALUES(UserId);
		`),
		{
			retries: 3
		}
	);
};

EBay.prototype.updateDetail = function () {
	return MySql.query(`
		INSERT INTO LMSDb.ListingDetail (
			ListingId, 
			LaunchDate,
			MarketplaceListingId,
			Category,
			Title, 
			ProductId, 
			ProductType,
			SyncDate, 
			ListingStatus, 
			UserId
		)
		SELECT 
			listing.ListingId,
			eBay.StartTime,
			eBay.ItemID as MarketplaceListingId,
			eBay.CategoryName as Category,
			eBay.Title,
			map.ProductId,
			product.ComponentType,
			eBay.LastUpdate,
			IFNULL(product.recStatus, "D") as ListingStatus,
			"CronSyncListing"
		FROM LMSDb.Listing listing
		LEFT JOIN LMSDb.ListingDetail detail
		ON listing.ListingId = detail.ListingId
		LEFT JOIN LMSDb.RawListingEBay eBay
		ON listing.MarketPlatformId = eBay.MarketPlatformId AND listing.ProductListingId = eBay.ProductListingId
		LEFT JOIN ProductDb.ProductCodeMap map
		ON map.ProductCode = listing.ProductListingId AND map.ProductCodeType = "AC" and map.ProductCodeClassId = "EFC"
		LEFT JOIN ErpDb.ProductListingDetail product
		ON product.ProductListingId = listing.ProductListingId
		WHERE 
			listing.MarketPlatformId LIKE "EBAY%" 
			AND (detail.ListingId IS NULL OR eBay.LastUpdate > detail.SyncDate) 
			AND eBay.ListingStatus = "Active"
		ON DUPLICATE KEY UPDATE 
			MarketplaceListingId=VALUES(MarketplaceListingId), 
			Title=VALUES(Title), 
			LaunchDate=VALUES(LaunchDate),
			SyncDate=VALUES(SyncDate), 
			ListingStatus=VALUES(ListingStatus),
			UserId=VALUES(UserId);

		INSERT INTO ListingTitle (ListingId, ListingTitle)
		SELECT ListingId, Title 
		FROM ListingDetail 
		WHERE ListingId IN (
			SELECT ListingId FROM Listing WHERE MarketPlatformId LIKE "EBAY%"
		)
		ON DUPLICATE KEY UPDATE ListingTitle = VALUES(ListingTitle);
	`, [this.taskId]);
};

EBay.prototype.updatePrice = function () {
	return MySql.query(`
		INSERT INTO LMSDb.ListingPrice (ListingId, Price, UserId)
		SELECT listing.ListingId, eBay.CurrentPrice as Price, ?
		FROM LMSDb.Listing listing
		LEFT JOIN LMSDb.ListingPrice price 
		ON listing.ListingId = price.ListingId 
		LEFT JOIN LMSDb.RawListingEBay eBay
		ON listing.MarketPlatformId = eBay.MarketPlatformId AND listing.ProductListingId = eBay.ProductListingId
		WHERE listing.MarketPlatformId LIKE "EBAY%" AND (eBay.LastUpdate > price.LastUpdate OR price.ListingId IS NULL)
		ON DUPLICATE KEY UPDATE Price=VALUES(Price);
	`, [this.taskId]);
};

EBay.prototype.updateInventory = function () {

};

util.inherits(EBay, Platform);
module.exports = EBay;
