"use strict";
/**
 * 
 * Standard Flow
 * Create Listing => Update Detail => Update Pricing => Update Inventory => Update Create Queue
 * 
 * Available status:
 * Active, CLOSE, UNPUB
 * 
 */
const util = require("util");
const MySql = require("mysql-plugin");
const Platform = require("./Platform");
const updateListingTitleFromSync = require("../updateListingTitleFromSync");

function Walmart() {
	Platform.call(this);
	this.id = "WALMART-26";
}

Walmart.prototype.createListing = async function () {

	/**
	 * 
	 * Update Listing Status and Remark,
	 * Create the listing if not exists
	 * 
	 */
	await MySql.query(`
		INSERT INTO LMSDb.Listing (
			MarketPlatformId, ProductListingId, ReferenceNum, 
			ListingStatusId, Remark, UserId
		)
		SELECT 
			walmart.MarketPlatformId,
			walmart.ProductListingId,
			walmart.SKU as ReferenceNum,
			CASE 
				WHEN LifeCycleStatus <> "ACTIVE"
					THEN "CLOSE"
				WHEN LifeCycleStatus = "ACTIVE" AND walmart.PublishStatus IN ("SYSTEM_PROBLEM", "UNPUBLISHED")
					THEN "UNPUB"
				WHEN LifeCycleStatus = "ACTIVE"
					THEN "ACTIVE"
				ELSE "ACTIVE"
			END as ListingStatusId,
			walmart.StatusChangeReason as Remark,
			"CronSyncListing"
		FROM RawListingWalmart walmart
		LEFT JOIN LMSDb.Listing listing
		ON listing.MarketPlatformId IN ("WALMART-26", "WALMART-FS") 
			AND listing.ProductListingId = walmart.ProductListingId 
			AND listing.ReferenceNum = walmart.SKU
			AND listing.MarketPlatformId = walmart.MarketPlatformId
			WHERE 
				listing.ListingStatusId IS NULL 
					OR 
				(
					listing.ListingStatusId IS NOT NULL AND (
						listing.ListingStatusId <> (
							CASE 
								WHEN LifeCycleStatus <> "ACTIVE"
									THEN "CLOSE"
								WHEN LifeCycleStatus = "ACTIVE" AND walmart.PublishStatus IN ("SYSTEM_PROBLEM", "UNPUBLISHED")
									THEN "UNPUB"
								WHEN LifeCycleStatus = "ACTIVE"
									THEN "ACTIVE"
								ELSE "ACTIVE"
							END
						)
					)
				)
		ON DUPLICATE KEY UPDATE 
			ListingStatusId=VALUES(ListingStatusId), 
			Remark=VALUES(Remark),
			RecStatus="A",
			UserId=VALUES(UserId);
	`);

	/**
	 * 
	 * Close the transferred listing
	 * 
	 */
	await MySql.query(`
		UPDATE Listing listing
		SET ListingStatusId = "CLOSE"
		WHERE 
			listing.MarketPlatformId IN ("WALMART-26", "WALMART-FS") 
				AND 
			ListingStatusId = "ACTIVE" 
				AND 
			NOT EXISTS (
				SELECT * FROM RawListingWalmart walmart
				WHERE walmart.MarketPlatformId = listing.MarketPlatformId AND walmart.SKU = listing.ReferenceNum
				LIMIT 1
			);
	`);

	return {};
};

Walmart.prototype.updateDetail = async function () {

	await MySql.query(`
		INSERT INTO LMSDb.ListingDetail (
			ListingId, LaunchDate, Category, MarketplaceListingId,
			Title, ProductId, UPC,
			SyncDate, ListingStatus, UserId
		)
		SELECT 
			listing.ListingId, walmart.OfferStartDate, walmart.ProductCategory, walmart.ItemID,
			ProductName as Title, walmart.ProductId, walmart.UPC,
			walmart.LastUpdate as SyncDate, IFNULL(product.recStatus, "D") as ListingStatus, "CronSyncListing"
		FROM LMSDb.Listing listing
		LEFT JOIN LMSDb.ListingDetail detail
		ON listing.ListingId = detail.ListingId
		LEFT JOIN LMSDb.RawListingWalmart walmart
		ON listing.MarketPlatformId IN ("WALMART-26", "WALMART-FS") 
			AND listing.ProductListingId = walmart.ProductListingId 
			AND listing.ReferenceNum = walmart.SKU
		LEFT JOIN ErpDb.ProductListingDetail product
		ON product.ProductListingId = walmart.ProductListingId
		WHERE listing.MarketPlatformId IN ("WALMART-26", "WALMART-FS") 
		ON DUPLICATE KEY UPDATE 
				Category=VALUES(Category), 
				MarketplaceListingId=VALUES(MarketplaceListingId), 
				Title=VALUES(Title), 
				ProductId=VALUES(ProductId),
				SyncDate = VALUES(SyncDate), 
				ListingStatus=VALUES(ListingStatus),
				UPC=VALUES(UPC),
				LaunchDate=VALUES(LaunchDate),
				UserId=VALUES(UserId);
	`);

	await updateListingTitleFromSync(["WALMART-26", "WALMART-FS"]);

	return {};
};

Walmart.prototype.updatePrice = function () {
	return MySql.query(`
		INSERT INTO LMSDb.ListingPrice (ListingId, Price, UserId)
		SELECT 
			listing.ListingId, 
			walmart.price, 
			"CronSyncListing"
		FROM LMSDb.Listing listing
		LEFT JOIN LMSDb.RawListingWalmart walmart
		ON listing.MarketPlatformId IN ("WALMART-26", "WALMART-FS")
			AND listing.ProductListingId = walmart.ProductListingId 
			AND listing.ReferenceNum = walmart.SKU
		LEFT JOIN LMSDb.ListingPrice price 
		ON listing.ListingId = price.ListingId 
		WHERE 
			listing.MarketPlatformId IN ("WALMART-26", "WALMART-FS")
				AND 
			(walmart.BuyBoxItemPrice <> price.Price OR price.ListingId IS NULL)
		ON DUPLICATE KEY UPDATE Price=VALUES(Price);
	`);
};

util.inherits(Walmart, Platform);
module.exports = Walmart;
