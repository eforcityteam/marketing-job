"use strict";
const fs = require("fs");
const path = require("path");
const YAML = require("yaml");
const initialize = require("./config");
const Bull = require("./Bull");

void async function () {

	const config = await initialize();

	const setting = YAML.parse(
		fs.readFileSync(path.join(__dirname, "./bull.yml"), "utf-8")
	);

	setting.config.redis = config.redis;

	const bull = new Bull(setting);

	await bull.updateConfigs();
	await bull.process();

}();
