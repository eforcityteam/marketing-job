"use strict";

module.exports = {
	"env": {
		"node": true,
		"commonjs": true,
		"es6": true,
		"mocha": true,
		"browser": true,
	},
	"parser": "babel-eslint",

	"extends": [
		"eslint:recommended"
	],
	"parserOptions": {
		"ecmaVersion": 2020,
		"sourceType": "module",
		"ecmaFeatures": {
			"jsx": true,
			"modules": true,
			"experimentalObjectRestSpread": true,
			experimentalDecorators: true
		},
		"ecmaFeatures": {
			"jsx": true
		}
	},
	"rules": {
		"no-console": "off",
		"react/prop-types": "off",
		"no-prototype-builtins": "off",
		"no-mixed-spaces-and-tabs": "off",
		"indent": [
			"error",
			"tab",
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		]
	},
	"settings": {
		"react": {
			"pragma": "React",
			"version": "16.13.1"
		}
	}
}
