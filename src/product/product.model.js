"use strict";
const _ = require("lodash");
const AWS = require("aws-sdk");
const axios = require("axios");
const MySql = require("mysql-plugin");
const Immutable = require("immutable");
const merge = require("../utils/mysqlQuickMerge");

const lambda = new AWS.Lambda({
	region: "us-west-2",
	useAccelerateEndpoint: true
});

function Product(body) {
	_.assign(this, body);
}

/**
 * 
 * 
 * 
 */
Product.getPackages = function (SKUs) {
	return MySql
		.pools["EFCReadDb"]
		.exec(`
			SELECT 
				product.SKU, 
				product.ProductVendorId as vendorId,
				mapPrice.price as mapPrice,
				mapPriceCa.price as mapPriceCa,
				pdt.TargetSalesPriceUSD as targetSalesPrice,
				cost.SalesCost as cost,
				GREATEST(GrossLength, GrossWidth, GrossHeight) as length,
				(GrossLength + GrossWidth + GrossHeight) - (GREATEST(GrossLength,GrossWidth,GrossHeight) + LEAST(GrossLength,GrossWidth,GrossHeight)) as width,
				LEAST(GrossLength,GrossWidth,GrossHeight) as height, 
				GrossWeight as weight, 
				IF(packageFormat.ShipPackageFormatId = "", NULL, packageFormat.ShipPackageFormatId) as Package_format,
				packageFormat.PackageCost as Package_cost,
				UPPER(IF(ShipPackageType IN (SELECT DISTINCT PackageType FROM ShipCalDb.ShipMethodPackageType), ShipPackageType, NULL)) as Package_type, 
				IFNULL(PackagePieces, 0) * 1 as Package_pieces,
				restriction.shipMethodRestrictionId as Battery_id
			FROM ErpDb.Product product
			LEFT JOIN ErpDb.ProductExt ext
			ON product.SKU = ext.SKU
			LEFT JOIN ErpDb.ProductShipMethodRestriction restriction
			ON ext.SKU = restriction.SKU
			LEFT JOIN ErpDb.ProductExtCost cost
			ON ext.SKU = cost.SKU
			LEFT JOIN ErpDb.DefShipPackageFormat packageFormat
			ON packageFormat.ShipPackageFormatId = ext.ShipPackageFormatId
			LEFT JOIN ProductDb.ProductCodeMap map
			ON map.ProductCode = product.SKU AND map.ProductCodeType = "SKU" AND map.ProductCodeClassId = "EFC" 
			LEFT JOIN ProductDb.ProductMapPrice mapPrice
			ON product.SKU = mapPrice.SKU AND mapPrice.Country = "US"
			LEFT JOIN ProductDb.ProductMapPrice mapPriceCa
			ON product.SKU = mapPriceCa.SKU AND mapPriceCa.Country = "CA"
			LEFT JOIN ProductDb.RawJuvoPDT pdt
			ON pdt.ProductId = map.ProductId
			WHERE product.SKU IN (?);
		`, [SKUs])
		.then(products => products.map(merge));
};

Product.getImages = async function (id) {
	const images = await MySql
		.query(`
			SELECT 
				FileCRC as hash,
				CONCAT("https://d1cyb0v9awdagl.cloudfront.net/hash/1200/", FileCRC, ".jpg") as url
			FROM ImageDb.ImageDetail 
			WHERE ProductId = ? AND RecStatus = "A"
			ORDER BY ObjectNum;
			SELECT 
				FileCRC as hash, 
				UPPER(AttributeId) as id
			FROM ImageDb.ImageAttributes
			WHERE FileCRC IN (
				SELECT FileCRC FROM ImageDb.ImageDetail WHERE ProductId = ?
			);
		`, [
			id, id
		])
		.then(response => {
			let [images, attributes] = response;
			return images.map(item => {
				item.attributes = attributes
					.filter(attribute => item.hash == attribute.hash)
					.map(item => item.id);
				return item;
			});
		});
	return images;
};

Product.prototype.getDescription = function (type) {
	return lambda
		.invoke({
			FunctionName: "listing-production-getDescription",
			Payload: JSON.stringify({
				id: this.id,
				platform: type
			})
		})
		.promise()
		.then(response => JSON.parse(response.Payload))
		.then(json => json.payload.html)
		.catch(error => console.error(error));
};

Product.get = async function (id) {
	return this
		.list([id])
		.then(item => item[0]);
};

Product.listImages = async function (ids) {

	let [
		images,
		imageAttributes
	] = await MySql.query(`
		-- Images
		SELECT
			ProductId,
			ObjectNum as seq,
			FileCRC as hash,
			CONCAT("https://d1cyb0v9awdagl.cloudfront.net/hash/1600/", FileCRC, ".jpg") as url
		FROM ImageDb.ImageDetail
		WHERE ProductId IN (?) AND RecStatus = "A";

		-- Attributes
		SELECT
			FileCRC as hash,
			UPPER(AttributeId) as id
		FROM ImageDb.ImageAttributes
		WHERE FileCRC IN (
			SELECT FileCRC
			FROM ImageDb.ImageDetail
			WHERE ProductId IN (?) AND RecStatus = "A"
		);
	`, [ids, ids]);

	images = _.reduce(
		_.groupBy(
			images.map(item => {
				item.attributes = imageAttributes
					.filter(attribute => item.hash == attribute.hash)
					.map(item => item.id);
				return item;
			}),
			"ProductId"
		),
		(prev, values, id) => {
			prev[id] = _.sortBy(values, "seq").map(item => _.omit(item, ["ProductId", "seq"]));
			return prev;
		},
		{}
	);

	return images;
};

Product.list = async function (ids = []) {

	const sql = MySql
		.format(`
			-- Product Main
			SELECT
				main.ProductId as id,
				main.categoryId as Category_id,
				root.FullName as Category_root,
				parent.FullName as Category_parent,
				category.FullName as Category_category,
				ext.ProductVendorId as vendorId,
				CF_Brand as brand,
				IF(CF_Chemical = "no", NULL, CF_Chemical) as chemical,
				CF_Material as material,
				SKU.ProductCode as SKU,
				(
					SELECT ProductCode
					FROM ProductDb.ProductCodeMap UPC
					WHERE ProductCodeType = "AC" AND main.ProductId = UPC.ProductId AND ProductCodeClassId = "EFC" AND ProductCode < "A"
					LIMIT 1
				) as auctionCode,
				(
					SELECT ProductCode
					FROM ProductDb.ProductPrimaryCode UPC
					WHERE ProductCodeTypeId = "UPC" AND main.ProductId = UPC.ProductId
					LIMIT 1
				) as UPC,
				IFNULL(erpProductExt.GrossWeight, ext.WeightPackaging) as Packing_weight,
				IFNULL(erpProductExt.GrossHeight, ext.HeightPackaging) as Packing_height,
				IFNULL(erpProductExt.GrossWidth, ext.WidthPackaging) as Packing_width,
				IFNULL(erpProductExt.GrossLength, ext.LengthPackaging) as Packing_length,
				erpProductExtCost.SalesCost as itemCost,
				UPPER(IF(erpProductExt.ShipPackageType = "", "PARCEL", ShipPackageType)) as shipPackageType,
				restriction.shipMethodRestrictionId,
				keyword.keyword as keywords,
				smallPartId.Id as smallPartId,
				smallPart.FullName as smallPart,
				main.UserId as triggeredBy,
				ext.CF_Category as juvoCategory
			FROM ProductDb.ProductMain main
			LEFT JOIN ProductDb.ProductPrimaryCode SKU
			ON main.ProductId = SKU.ProductId AND SKU.ProductCodeTypeId = "SKU" AND SKU.ProductCodeClassId = "EFC"
			LEFT JOIN ProductDb.DefCategory category
			ON category.categoryId = main.categoryId
			LEFT JOIN ProductDb.DefCategory parent
			ON category.ParentCategoryId = parent.categoryId
			LEFT JOIN ProductDb.DefCategory root
			ON category.rootCategoryId = root.categoryId
			LEFT JOIN ProductDb.ProductExt ext
			ON main.ProductId = ext.ProductId
			LEFT JOIN ErpDb.Product erpProduct
			ON SKU.ProductCode = erpProduct.SKU
			LEFT JOIN ErpDb.ProductExtCost erpProductExtCost
			ON erpProduct.SKU = erpProductExtCost.SKU
			LEFT JOIN ErpDb.ProductShipMethodRestriction restriction
			ON erpProduct.SKU = restriction.SKU
			LEFT JOIN ErpDb.ProductExt erpProductExt
			ON erpProduct.SKU = erpProductExt.SKU
			LEFT JOIN ProductDb.ProductKeyword keyword
			ON main.ProductId = keyword.ProductId AND keyword.EditionId = 12
			LEFT JOIN ProductDb.ProductMeta smallPartId
			ON main.ProductId = smallPartId.ProductId AND ProductMetaId = 13
			LEFT JOIN ProductDb.DefProductMetaValue smallPart
			ON smallPart.ProductMetaValueId = smallPartId.Id
			WHERE main.ProductId IN (?);

			-- Compatibilities
			SELECT
				ProductId,
				compatibilities.ModelId AS id,
				brand.FullName as brand,
				IF(series.SeriesId = 0, NULL, series.FullName) as series,
				model.FullName as model,
				CONCAT_WS(" ", IF(series.SeriesId = 0, NULL, series.FullName), model.FullName) as seriesModel,
				CONCAT_WS(" ", brand.FullName, IF(series.SeriesId = 0, NULL, series.FullName), model.FullName) as name
			FROM ProductDb.ProductCompatibilities compatibilities
			INNER JOIN ProductDb.DefModel model
			ON compatibilities.ModelId = model.ModelId
			INNER JOIN ProductDb.DefSeries series
			ON model.SeriesId = series.SeriesId
			INNER JOIN ProductDb.DefBrand brand
			ON model.BrandId = brand.BrandId
			WHERE Type = "C" AND ProductId IN (?)
			ORDER BY model.ModelId ASC;

			-- Title
			SELECT ProductId, EditionId as id, ProductTitle as title
			FROM ProductDb.ProductTitle
			WHERE ProductId IN (?);

			-- Description
			SELECT ProductId, EditionId as id, Sequence as seq, ProductDescription as description
			FROM ProductDb.ProductDescription
			WHERE ProductId IN (?) AND RecStatus = "A" AND EditionId IN (5, 6);

			-- Images
			SELECT
				ProductId,
				ObjectNum as seq,
				FileCRC as hash,
				CONCAT("https://d1cyb0v9awdagl.cloudfront.net/hash/1600/", FileCRC, ".jpg") as url
			FROM ImageDb.ImageDetail
			WHERE ProductId IN (?) AND RecStatus = "A";

			-- Attributes
			SELECT
				FileCRC as hash,
				UPPER(AttributeId) as id
			FROM ImageDb.ImageAttributes
			WHERE FileCRC IN (
				SELECT FileCRC
				FROM ImageDb.ImageDetail
				WHERE ProductId IN (?) AND RecStatus = "A"
			);

			-- DefMeta
			SELECT ProductMetaId as id, dataType as type
			FROM ProductDb.DefProductMeta
			WHERE RecStatus = "A";

			-- Meta
			SELECT
				ProductId,
				meta.ProductMetaId as id,
				dataType as type,
				IFNULL(defValue.FullName, meta.string) as string,
				meta.boolean,
				meta.number
			FROM ProductDb.ProductMeta meta
			LEFT JOIN ProductDb.DefProductMeta defMeta
			ON meta.ProductMetaId = defMeta.ProductMetaId
			LEFT JOIN ProductDb.DefProductMetaValue defValue
			ON defValue.ProductMetaValueId = meta.Id
			WHERE ProductId IN (?) AND defMeta.RecStatus = "A";
		`,
			Array
				.from({ length: 12 })
				.fill(ids)
		);

	return MySql
		.query(sql)
		.then(response => {

			let [
				mains,
				compatibilities,
				titles,
				descriptions,
				images,
				imageAttributes,
				defMeta,
				meta
			] = response;

			defMeta = Immutable.Map(
				defMeta.reduce((prev, curr) => {
					prev[curr.id] = curr.type.includes("array") ? [] : null;
					if (curr.type == "boolean")
						prev[curr.id] = false;
					return prev;
				}, {})
			);

			compatibilities = Object.fromEntries(
				_.map(
					_.groupBy(compatibilities, "ProductId"),
					(models, id) => [
						id,
						_.sortBy(
							_.map(
								_.groupBy(models, "brand"), (value, brand) => {
									const series = _.map(_.groupBy(value, "series"), (value, series) => {
										const result = {
											series,
											models: _.sortBy(value, "model").map(item => item.model)
										};
										result.text = series == "null" ? result.models.join(", ") : (series + " " + result.models.join(", "));
										delete result.models;
										return result;
									});
									const result = {
										brand,
										series: _.sortBy(series, "series")
									};
									return result;
								}),
							"brand"
						)
					]
				)
			);

			titles = _.reduce(
				_.groupBy(titles, "ProductId"),
				(prev, values, id) => {
					prev[id] = Object.fromEntries(values.map(value => [value.id, value.title]));
					return prev;
				},
				{}
			);

			descriptions = _.reduce(
				_.groupBy(descriptions, "ProductId"),
				(prev, values, id) => {
					prev[id] = {
						description: _.find(values, item => item.id == 5)?.description,
						features: _.sortBy(
							values
								.filter(item => item.id == 6)
								.map(item => item.description),
							"seq"
						)
					};
					return prev;
				},
				{}
			);

			images = _.reduce(
				_.groupBy(
					images.map(item => {
						item.attributes = imageAttributes
							.filter(attribute => item.hash == attribute.hash)
							.map(item => item.id);
						return item;
					}),
					"ProductId"
				),
				(prev, values, id) => {
					prev[id] = _.sortBy(values, "seq").map(item => _.omit(item, ["ProductId", "seq"]));
					return prev;
				},
				{}
			);

			meta = _.reduce(
				_.groupBy(meta, "ProductId"),
				(prev, values, id) => {
					prev[id] = _.reduce(
						values,
						(prev, curr) => {
							let value;
							if (curr.type == "boolean")
								value = curr.boolean == "Y";
							else if (curr.type == "number")
								value = parseFloat(curr.number);
							else
								value = curr.string;
							if (curr.type.includes("array"))
								prev[curr.id].push(value);
							else
								prev[curr.id] = value;
							return prev;
						},
						defMeta.toJS()
					);
					return prev;
				},
				{}
			);

			mains = mains
				.map(product => {
					product = merge(product);
					[
						product.packing.length,
						product.packing.width,
						product.packing.height
					] = _.sortBy([
						product.packing.length,
						product.packing.width,
						product.packing.height
					]).reverse();
					product.compatibilities = compatibilities[product.id] ?? [];
					product.titles = titles[product.id] ?? {};
					product.description = descriptions[product.id]?.description;
					product.features = descriptions[product.id]?.features || [];
					product.images = images[product.id] || [];
					product.meta = meta[product.id] || [];
					return new Product(product);
				});

			return mains;
		});
};

module.exports = Product;
