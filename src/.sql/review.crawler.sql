-- How much new listings each day
SELECT
    temp.place,
    ROUND(AVG(temp.count)) as average
FROM (
    SELECT
        platform.MarketplaceId as place,
        DATE(listing.EnterDate) as date,
        COUNT(*) as count
    FROM LMSDb.Listing listing
    LEFT JOIN ErpDb.MarketPlatform platform
    ON platform.MarketPlatformId = listing.MarketPlatformId
    WHERE 
        platform.MarketplaceId IN ("AMAZ", "WALMA", "EBAY", "TARG")
            AND
        listing.EnterDate > NOW() - INTERVAL 7 DAY
    GROUP BY platform.MarketplaceId, DATE(listing.EnterDate)
) temp
GROUP BY temp.place;

-- How much listings you have scraped each day
SELECT 
	DATE(log.LastUpdate) as date,
	platform.MarketplaceId,
	COUNT(*) as count
FROM LMSDb.ListingReviewHeaderLog log
LEFT JOIN LMSDb.Listing listing
ON listing.ListingId = log.ListingId
LEFT JOIN ErpDb.MarketPlatform platform
ON platform.MarketPlatformId = listing.MarketPlatformId
WHERE log.LastUpdate > NOW() - INTERVAL 14 DAY AND listing.MarketPlatformId IS NOT NULL
GROUP BY DATE(log.LastUpdate), platform.MarketplaceId
ORDER BY date DESC;

-- Average Delay between review and scraper
SELECT
    total.place,
    ROUND(total.count / average.average) as days
FROM (
	SELECT 
		platform.MarketplaceId as place,
		COUNT(*) as count
	FROM LMSDb.Listing listing
	LEFT JOIN LMSDb.ListingDetail detail 
	ON detail.ListingId = listing.ListingId
	LEFT JOIN LMSDb.ListingReviewHeader review
	ON review.ListingId = listing.ListingId
	LEFT JOIN ErpDb.MarketPlatform platform
	ON listing.MarketPlatformId = platform.MarketPlatformId 
	WHERE 
		listing.ListingStatusId = "ACTIVE"
		AND listing.RecStatus = "A"
		AND detail.ListingStatus = "A"
		AND detail.RecStatus = "A"
		AND detail.MarketplaceListingId IS NOT NULL
		AND review.RecStatus = "A"
		AND platform.MarketplaceId = "AMAZ"
	UNION
	SELECT 
		platform.MarketplaceId as place,
		COUNT(*) as count
	FROM LMSDb.Listing listing
	LEFT JOIN ErpDb.MarketPlatform platform
	ON listing.MarketPlatformId = platform.MarketPlatformId
	LEFT JOIN LMSDb.ListingDetail detail 
	ON detail.ListingId = listing.ListingId
	LEFT JOIN LMSDb.ListingCrawler crawler
	ON crawler.ListingId = listing.ListingId
	LEFT JOIN LMSDb.ListingReviewHeader review
	ON review.ListingId = listing.ListingId
	WHERE
		platform.MarketplaceId IN ("WALMA", "EBAY", "TARG")
		AND listing.ListingStatusId = "ACTIVE"
		AND listing.RecStatus = "A"
		AND detail.MarketplaceListingId IS NOT NULL
		AND detail.RecStatus = "A"
		AND review.RecStatus = "A"
		AND crawler.HasReview = "Y"
	GROUP BY platform.MarketplaceId
) total -- total job queue for each place
LEFT JOIN (
    SELECT
        temp.place,
        ROUND(AVG(temp.count)) as average
    FROM (
		SELECT 
			platform.MarketplaceId as place,
			DATE(log.LastUpdate) as data,
			COUNT(*) as count
		FROM LMSDb.ListingReviewHeaderLog log
		LEFT JOIN LMSDb.Listing listing
		ON listing.ListingId = log.ListingId
		LEFT JOIN ErpDb.MarketPlatform platform
		ON platform.MarketPlatformId = listing.MarketPlatformId
		WHERE log.LastUpdate > NOW() - INTERVAL 14 DAY AND listing.MarketPlatformId IS NOT NULL
		GROUP BY DATE(log.LastUpdate), platform.MarketplaceId
    ) temp
    GROUP BY temp.place
) average -- average crawled listings count everyday for each place
ON average.place = total.place;
