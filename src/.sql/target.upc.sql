DELIMITER $$
CREATE PROCEDURE usp_Listing_Update_Target_UPC()
BEGIN
UPDATE 
	ListingDetail detail,
	(
		SELECT 
			listing.ListingId, 
			line.UPC
		FROM Listing listing
		LEFT JOIN ListingDetail detail
		ON listing.ListingId = detail.ListingId
		INNER JOIN ProcQueueChannelAdvisorLine line
		ON line.SKU = listing.ReferenceNum
		WHERE 
			listing.MarketPlatformId IN ("TARGET-96", "WALMART-26", "WALMART-FS")
				AND 
			line.LineStatusId = "CREATE" 
				AND 
			LaunchChannel = "Walmart" 
				AND 
			detail.UPC IS NULL
	) updates
SET detail.UPC = updates.UPC
WHERE detail.ListingId = updates.ListingId;
UPDATE 
	ListingDetail,
	(
	SELECT 
	listing.ListingId,
	line.UPC
FROM Listing listing
LEFT JOIN ListingDetail detail
ON listing.ListingId = detail.ListingId
INNER JOIN ProcQueueChannelAdvisorLine line
ON LaunchChannel = "Walmart" AND line.SKU = listing.ReferenceNum
WHERE MarketPlatformId = "TARGET-96" AND detail.UPC IS NULL
	) updates
SET ListingDetail.UPC = updates.UPC
WHERE ListingDetail.ListingId = updates.ListingId;
END $$
DELIMITER ; 
