"use strict";
const moment = require("moment");
const MySql = require("mysql-plugin");
const retry = require("p-retry");
const Account = require("../../account/account.model.js");
const logger = require("../../utils/logger");

async function scrape() {

	const isExists = await MySql
		.query(`
			SELECT LoadDate 
			FROM RawListingQualityWalmart 
			WHERE LoadDate = ?
		`,
			moment().format("YYYY-MM-DD")
		)
		.then(response => {
			console.log(response);
			return !!(response[0]?.LoadDate);
		});

	if (isExists) {
		console.log("Exists");
		return;
	}

	logger.info("[Walmart - Insight] Start get walmart insight");

	await updateAccountScore();

	await updateListingQuality();

	await retry(
		() => MySql.query(`
			UPDATE 
				RawListingQualityWalmart listing,
				ProductDb.ProductCodeMap map
			SET listing.ProductId = map.ProductId
			WHERE 
				map.ProductCode = SUBSTRING_INDEX(listing.SKU, "_", 1) 
					AND 
				listing.ProductId IS NULL;
			UPDATE 
				RawListingQualityIssuesWalmart listing,
				ProductDb.ProductCodeMap map
			SET listing.ProductId = map.ProductId
			WHERE
				map.ProductCode = SUBSTRING_INDEX(listing.SKU, "_", 1) 
					AND 
				listing.ProductId IS NULL;
		`),
		{
			retries: 5
		}
	);

	logger.debug("[Walmart - Insight] Finished");
}

async function updateListingQuality() {

	const account = await Account.get("walmart_wfs");
	const agent = account.getAgent();

	let nextCursor, items = 0;

	do {

		const response = await agent
			.post(
				"/insights/items/listingQuality/items",
				{},
				{
					headers: {
						"WM_SEC.ACCESS_TOKEN": await account.getAccessToken()
					},
					params: {
						nextCursor
					}
				}
			)
			.then(response => response.data);

		logger.debug(`[Walmart - Insight] Running on ${items} items out of ${response.totalItems} items`);

		if (response.totalItems < 1)
			break;

		nextCursor = response.nextCursor;
		items += response.payload.length;

		let updates = response
			.payload
			.map(listing => [
				listing.itemId,
				listing.sku,
				listing.updatedTimestamp,
				listing.priority,
				listing.qualityScoreData?.score,
				moment().toDate()
			]);

		await retry(
			async () => {
				await MySql.query(`
					INSERT INTO RawListingQualityWalmart (
						ItemID, SKU, UpdatedTimestamp, Priority, Score, LoadDate
					) VALUES ?
					ON DUPLICATE KEY UPDATE ItemID = VALUES(ItemID);
				`,
					[updates]
				);
			},
			{
				retries: 5
			}
		);

		updates = response
			.payload
			.map(
				listing => listing.scoreDetails.contentAndDiscoverability.issues.map(issue => [
					listing.itemId,
					listing.sku,
					issue.score,
					issue.attributeName,
					issue.attributeValue,
					listing.scoreDetails.contentAndDiscoverability,
					(issue.issues ?? []).map(item => item.value).join("\n"),
					moment().toDate()
				])
			)
			.flat();

		await retry(
			async () => {
				await MySql.query(`
					INSERT INTO RawListingQualityIssuesWalmart (
						ItemID, SKU, Score, AttributeName, AttributeValue, IssueCount, Issues, LoadDate
					) VALUES ?
					ON DUPLICATE KEY UPDATE ItemID = VALUES(ItemID);
				`,
					[updates]
				);
			},
			{
				retries: 5
			}
		);

	} while (nextCursor);
}

async function updateAccountScore() {

	logger.debug("[Walmart - Insight] Updating account scores");

	const account = await Account.get("walmart_wfs");
	const agent = account.getAgent();

	const scores = await agent
		.get(
			"/insights/items/listingQuality/score",
			{
				headers: {
					"WM_SEC.ACCESS_TOKEN": await account.getAccessToken()
				}
			}
		)
		.then(response => response.data.payload);


	await retry(
		() => {

		}
	);

	await MySql.query(`
		INSERT INTO RawListingQualityScoreWalmart 
		SET ?
		ON DUPLICATE KEY UPDATE 
			OfferScore = VALUES(OfferScore),
			RatingReviewScore = VALUES(RatingReviewScore),
			ContentScore = VALUES(ContentScore),
			ItemDefectCount = VALUES(ItemDefectCount),
			DefectRatio = VALUES(DefectRatio),
			ListingQuality = VALUES(ListingQuality);
	`,
		{
			Date: moment().toDate(),
			OfferScore: scores.score.offerScore,
			RatingReviewScore: scores.score.ratingReviewScore,
			ContentScore: scores.score.contentScore,
			ItemDefectCount: scores.postPurchaseQuality.itemDefectCnt,
			DefectRatio: scores.postPurchaseQuality.defectRatio,
			ListingQuality: scores.listingQuality
		}
	);
}

module.exports = scrape;
