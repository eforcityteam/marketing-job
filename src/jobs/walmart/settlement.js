"use strict";
// const fs = require("fs");
const ms = require("ms");
const qs = require("qs");
const md5 = require("md5");
const mem = require("mem");
const _ = require("lodash");
const map = require("p-map");
const UUID = require("uuid");
const JsZip = require("jszip");
const AWS = require("aws-sdk");
const axios = require("axios");
const moment = require("moment");
const MySql = require("mysql-plugin");
const csvtojson = require("csvtojson");

const initialize = require("../../config");
const logger = require("../../utils/logger");
const Account = require("../../account/account.model.js");

const COLUMNS = require("./settlementColumn.json");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

async function handle() {

	await initialize();

	/**
	 * 
	 * Get the walmart API agent
	 * 
	 */
	const account = await Account.get("walmart_wfs");

	const agent = axios.create({
		baseURL: "https://marketplace.walmartapis.com/v3",
		headers: {
			["WM_SVC.NAME"]: "Walmart Marketplace",
			["WM_QOS.CORRELATION_ID"]: UUID.v4(),
			Accept: "application/json",
			Authorization: "Basic " + account.credential.token
		}
	});

	account.getAccessToken = mem(
		function () {
			return agent
				.post(
					"/token",
					qs.stringify({ grant_type: "client_credentials" })
				)
				.then(response => response.data.access_token);
		},
		{ maxAge: ms("3 minutes") }
	);

	/**
	 * 
	 * 
	 * 
	 */
	const ids = await MySql
		.query(
			"SELECT ReportId as ID FROM LMSDb.ProcQueueWalmartReconReport WHERE RecStatus = ?",
			[
				"U"
			]
		)
		.then(rows => rows.map(item => item.ID));

	await map(
		ids,
		async id => {

			logger.debug("[walmart-settlement] Running on report #" + id);

			try {
				let csv = await agent
					.get(
						"/report/reconreport/reconFile",
						{
							params: {
								reportDate: id
							},
							headers: {
								"WM_SEC.ACCESS_TOKEN": await account.getAccessToken()
							},
							responseType: "arraybuffer"
						}
					)
					.then(response => JsZip.loadAsync(response.data))
					.then(zip => {
						const filename = Object.keys(zip.files).find(file => file.endsWith(".csv"));
						return zip.files[filename].async("string");
					});

				const filename = `eForCity_MP_${id}_reconciliation_report.csv`;

				csv = csv.split("\n");

				if (csv[0] == csv[1])
					csv.shift();

				csv[0] = csv[0]
					.replace(/\./g, "-")
					.replace(/\s/g, "_")
					.toLowerCase();

				csv = csv.join("\n");

				await insert(csv, id);

				await s3
					.upload({
						Bucket: "eforcity-attachments-intelligent",
						Key: "projects/marketing/settlement/walmart/" + filename,
						Body: csv
					})
					.promise();

				await MySql.query(`
					UPDATE LMSDb.ProcQueueWalmartReconReport 
					SET ? 
					WHERE ReportId = ?
				`, [
					{
						RecStatus: "C",
						Bucket: "eforcity-attachments-intelligent",
						BucketKey: "projects/marketing/settlement/walmart/" + filename,
						FileName: filename
					},
					id
				]);
			} catch (error) {

				logger.error(error);

				await MySql.query(`
					UPDATE LMSDb.ProcQueueWalmartReconReport 
					SET ? 
					WHERE ReportId = ?
				`, [
					{
						RecStatus: "E",
						Remark: error.message
					},
					id
				]);
			}
		},
		{
			concurrency: 1,
			stopOnError: false
		}
	);

	logger.debug("[walmart-settlement] Program completed");
	return {};
}

async function insert(csv, id) {

	let json = await csvtojson({}).fromString(csv);

	json = json.map(item => {

		const object = {
			market_platform_id: item["fulfillment_type_"] != "Seller Fulfilled" ? "WALMART-FS" : "WALMART-26",
			report_id: id,
			hash: md5(JSON.stringify(item)),
			...item
		};

		object["transaction_date_time"] = moment(object["transaction_date_time"], "MM/DD/YYYY").toDate();

		Object
			.keys(object)
			.forEach(key => {
				if (object[key] == "")
					object[key] = null;
			});

		return object;
	});

	await map(
		_.chunk(json, 5000),
		rows => MySql.query(`
			INSERT INTO 
				LMSDb.RawSettlementWalmart (??) 
			VALUES ? 
			ON DUPLICATE KEY UPDATE 
				TransactionHash = VALUES(TransactionHash)
		`,
			[
				COLUMNS,
				rows.map(row => Object.values(row))
			]
		),
		{
			concurrency: 2,
			stopOnError: true
		}
	);

	return {};
}

module.exports = handle;

