"use strict";
const qs = require("qs");
const ms = require("ms");
const mem = require("mem");
const UUID = require("uuid");
const axios = require("axios");
const moment = require("moment");
const MySql = require("mysql-plugin");
const Account = require("../../account/account.model.js");
const initialize = require("../../config");

async function getSettlementReports() {

	await initialize();

	/**
	 * 
	 * Get the walmart API agent
	 * 
	 */
	const account = await Account.get("walmart_wfs");

	const agent = axios.create({
		baseURL: "https://marketplace.walmartapis.com/v3",
		headers: {
			["WM_SVC.NAME"]: "Walmart Marketplace",
			["WM_QOS.CORRELATION_ID"]: UUID.v4(),
			Accept: "application/json",
			Authorization: "Basic " + account.credential.token
		}
	});

	account.getAccessToken = mem(
		function () {
			return agent
				.post(
					"/token",
					qs.stringify({ grant_type: "client_credentials" })
				)
				.then(response => response.data.access_token);
		},
		{ maxAge: ms("3 minutes") }
	);

	/**
	 * 
	 * 
	 * 
	 */
	const dates = await agent
		.get(
			"/report/reconreport/availableReconFiles",
			{
				headers: {
					"WM_SEC.ACCESS_TOKEN": await account.getAccessToken()
				}
			}
		)
		.then(response => response.data.availableApReportDates);

	if (dates.length > 0)
		await MySql.query(`
			INSERT INTO LMSDb.ProcQueueWalmartReconReport (ReportId) 
			VALUES ?
			ON DUPLICATE KEY UPDATE ReportId = VALUES(ReportId)
		`, [
			dates
				.filter(item => moment(item, "MMDDYYYY").isAfter(moment("06182020", "MMDDYYYY")))
				.map(date => [date])
		]);

	return {};
}

module.exports = getSettlementReports;
