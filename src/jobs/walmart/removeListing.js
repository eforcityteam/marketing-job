"use strict";
const MySql = require("mysql-plugin");
const map = require("p-map");
const Account = require("../../account/account.model.js");
const logger = require("../../utils/logger");

async function remove() {

	const account = await Account.get("walmart_wfs");
	const agent = account.getAgent();

	const ids = await MySql
		.query(`
			SELECT SKU 
			FROM ProcWalmartPendingDelete 
			WHERE RecStatus = "Q" 
		`)
		.then(rows => rows.map(item => item.SKU));

	await map(
		ids,
		async id => {
			try {
				await agent
					.delete("/items/" + id, {
						headers: {
							"WM_SEC.ACCESS_TOKEN": await account.getAccessToken()
						}
					});
				await MySql.query(
					"UPDATE ProcWalmartPendingDelete SET RecStatus = ? WHERE SKU = ?",
					["C", id]
				);
			} catch (error) {
				console.error(error);
				await MySql.query(
					"UPDATE ProcWalmartPendingDelete SET RecStatus = ? WHERE SKU = ?",
					["E", id]
				);
			}
		},
		{
			concurrency: 1
		}
	);

	logger.debug("[removeListing] Program completed");
	return {};
}

module.exports = remove;
