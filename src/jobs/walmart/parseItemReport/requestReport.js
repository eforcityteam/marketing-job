"use strict";
const axios = require("axios");
const MySql = require("mysql-plugin");

const Account = require("../../../account/account.model.js");
const logger = require("../../../utils/logger");

async function requestReport() {

	const account = await Account.get("walmart_wfs");

	const agent = account.getAgent();
	const accessToken = await account.getAccessToken();

	const data = await agent
		.post(
			"/reports/reportRequests",
			null,
			{
				params: {
					reportType: "ITEM",
					reportVersion: "v1"
				},
				headers: {
					"WM_SEC.ACCESS_TOKEN": accessToken
				}
			}
		)
		.then(response => response.data);

	await MySql.query(
		"INSERT INTO LMSDb.ProcQueueWalmartReport SET ?",
		[
			{
				ReportId: data.requestId,
				ReportType: data.reportType
			}
		]
	);

	logger.debug("[walmart/requestReport] Program finished");
	return {};
}

module.exports = requestReport;
