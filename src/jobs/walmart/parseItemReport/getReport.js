"use strict";
// const fs = require("fs");
const _ = require("lodash");
const map = require("p-map");
const axios = require("axios");
const JsZip = require("jszip");
const AWS = require("aws-sdk");
const MySql = require("mysql-plugin");
const csvtojson = require("csvtojson");

const parse = require("./parseRow");
const columnMap = require("./columnMap.json");
const Account = require("../../../account/account.model.js");
const logger = require("../../../utils/logger");

const s3 = new AWS.S3({ region: "us-west-2" });

async function getReport() {

	const ids = await MySql
		.query(`
			SELECT ReportId as id 
			FROM ProcQueueWalmartReport 
			WHERE RecStatus = ? AND ReportType = ?
			ORDER BY EnterDate DESC
		`,
			["Q", "ITEM"]
		)
		.then(rows => rows.map(item => item.id));

	if (ids.length < 1) {
		logger.debug("[Walmart] No walmart item report");
		return {};
	}

	const account = await Account.get("walmart_wfs");
	const agent = account.getAgent();

	await map(
		ids,
		async id => {
			const accessToken = await account.getAccessToken();
			try {

				const data = await agent
					.get(
						"/reports/reportRequests",
						{
							params: { requestId: id },
							headers: { "WM_SEC.ACCESS_TOKEN": accessToken }
						}
					)
					.then(response => _.first(response.data.requests));

				console.log(
					JSON.stringify(
						data,
						null,
						4
					)
				);

				if (!data || !["READY", "ERROR"].includes(data.requestStatus))
					return false;

				if (data.requestStatus == "ERROR") {
					await MySql.query(
						"UPDATE ProcQueueWalmartReport SET RecStatus = ?, Remark = ? WHERE ReportId = ?",
						["E", data.requestStatus, id]
					);
					return false;
				}

				if (data.requestStatus == "READY") {

					let csv = await agent
						.get(
							"/reports/downloadReport",
							{
								params: { requestId: id },
								headers: {
									"WM_SEC.ACCESS_TOKEN": accessToken
								}
							}
						)
						.then(response => response?.data.downloadURL)
						.then(url => {
							return axios
								.get(
									url,
									{
										headers: {
											"WM_SEC.ACCESS_TOKEN": accessToken
										},
										responseType: "arraybuffer"
									}
								);
						})
						.then(response => JsZip.loadAsync(response.data))
						.then(zip => {
							const filename = Object.keys(zip.files).find(file => file.endsWith(".csv"));
							return zip.files[filename].async("string");
						});

					logger.debug("[walmart/getItemReport] Got zip file, uploading to S3 ... ");

					await s3
						.upload({
							Bucket: "eforcity-attachments-intelligent",
							Key: "projects/marketing/item-reports/walmart/" + id + ".csv",
							Body: csv
						})
						.promise();

					logger.debug("[walmart/getItemReport] Unzip folder, parsing and chunking ");

					csv = await csvtojson({})
						.fromString(csv)
						.then(rows => rows.map(parse));

					const updateFields = _.map(
						columnMap,
						item => MySql.format(`?? = VALUES(??)`, [item.target, item.target])
					).join(", ");

					logger.debug("[walmart/getItemReport] Inserting rows ... ");

					await map(
						_.chunk(csv, 5000),
						async (rows, index) => {
							logger.debug("[walmart/getItemReport] Inserting chunk #" + (index + 1));
							const sql = MySql.format(`
								INSERT INTO RawListingWalmart(??) 
								VALUES ?
								ON DUPLICATE KEY UPDATE ${updateFields}
							`,
								[
									_.map(columnMap, item => item.target),
									rows.map(row => Object.values(row))
								]
							);
							await MySql.query(sql);
							return true;
						},
						{ concurrency: 1 }
					);

					await MySql.query(
						"UPDATE ProcQueueWalmartReport SET RecStatus = ?, BucketFolder = ?, BucketKey = ? WHERE ReportId = ?",
						["C", "projects/marketing/item-reports/walmart", id + ".csv", id]
					);
					return true;
				}

			} catch (error) {
				logger.error(error);
				await MySql.query(
					"UPDATE ProcQueueWalmartReport SET RecStatus = ?, Remark = ? WHERE ReportId = ?",
					["E", error.message, id]
				);
				return false;
			}
		},
		{
			concurrency: 1
		}
	);

	await MySql.query(`
		UPDATE RawListingWalmart 
		SET MarketPlatformId = IF(FulfillmentType = "Walmart Fulfilled", "WALMART-FS", "WALMART-26");
	`);

	await MySql.query(`
		UPDATE RawListingWalmart
		SET ProductListingId = SUBSTRING_INDEX(SKU, '_', 1)
		WHERE SUBSTRING_INDEX(SKU, '_', 1) <> ProductListingId OR ProductListingId IS NULL;
	`);

	await MySql.query(`
		UPDATE
			RawListingWalmart walmart,
			ProductDb.ProductCodeMap map
		SET walmart.ProductId = map.ProductId
		WHERE 
			map.ProductCode = walmart.ProductListingId 
				AND 
			map.ProductCodeType = "AC" 
				AND 
			map.ProductCodeClassId = "EFC" 
				AND 
			map.ProductId <> walmart.ProductId;
	`);

	logger.debug("[walmart/getItemReport] Program finished");
	return {};
}

module.exports = getReport;
