"use strict";
const _ = require("lodash");
const moment = require("moment");
const COLUMN_MAP = require("./columnMap.json");

function parse(row) {
	return _
		.reduce(
			COLUMN_MAP,
			(prev, criteria, key) => {
				let value = row[key].trim();
				if (value != "") {
					if (criteria.type == "date") {
						value = moment(value, "MM/DD/YYYY").toDate();
					} else if (criteria.type == "number")
						value = parseFloat(value);
				} else
					value = null;
				prev[criteria.target] = value;
				return prev;
			},
			{}
		);
}

module.exports = parse;
