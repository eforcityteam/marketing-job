"use strict";
const map = require("p-map");
const AWS = require("aws-sdk");
const MySql = require("mysql-plugin");

const logger = require("../utils/logger");
const importTargetListing = require("../utils/importTargetListing");
const importStaplesListing = require("../utils/importStaplesListing");

const s3 = new AWS.S3({ region: "us-west-2" });

async function processUploadListingFile() {

	const jobs = await MySql.query(`
		SELECT
			ListingImportId as id,
			MarketPlace as place,
			bucketFolder,
			bucketKey,
			FileName as filename
		FROM ProcQueueListingImport queue
		WHERE RecStatus = "Q";
	`);

	await map(
		jobs,
		async job => {
			switch (job.place) {

				case "STAPLES":

					const buffer = await s3
						.getObject({
							Bucket: "eforcity-attachments-intelligent",
							Key: job.bucketFolder + "/" + job.bucketKey
						})
						.promise()
						.then(response => response.Body);

					await importStaplesListing(buffer);

					await MySql.query(
						"UPDATE ProcQueueListingImport SET RecStatus = ? WHERE ListingImportId = ?",
						["C", job.id]
					);
					logger.info(`[processUploadedListingFile] Success to process file ${job.filename}`);

					break;

				case "TARGET":

					logger.debug(`[processUploadedListingFile] Running on file: ${job.filename}`);

					try {
						const buffer = await s3
							.getObject({
								Bucket: "eforcity-attachments-intelligent",
								Key: job.bucketFolder + "/" + job.bucketKey
							})
							.promise()
							.then(response => response.Body);

						await importTargetListing(buffer);

						await MySql.query(
							"UPDATE ProcQueueListingImport SET RecStatus = ? WHERE ListingImportId = ?",
							["C", job.id]
						);
						logger.info(`[processUploadedListingFile] Success to process file ${job.filename}`);
					} catch (error) {
						logger.error(error, { label: "processUploadedListingFile" });
						await MySql.query(
							"UPDATE ProcQueueListingImport SET RecStatus = ?, Remark = ? WHERE ListingImportId = ?",
							["E", error.message, job.id]
						);
					}
					break;
				default:
					break;
			}
		},
		{
			concurrency: 1
		}
	);

	logger.debug("[processUploadedListingFile] Program completed");
	return true;
}

module.exports = processUploadListingFile;
