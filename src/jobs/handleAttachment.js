"use strict";
const map = require("p-map");
const AWS = require("aws-sdk");
const MySql = require("mysql-plugin");
const logger = require("../utils/logger");
const initialize = require("../config");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: true
});

const bucket = "eforcity-attachments-intelligent";

async function handle() {

	await initialize();

	const rows = await MySql.query(`
		SELECT 
			AttachmentId as id, 
			folder 
		FROM LMSDb.Attachment 
		WHERE RecStatus = "U";
	`);

	await map(
		rows,
		async file => {

			try {

				const buffer = await s3
					.getObject({
						Bucket: bucket,
						Key: "temporary/marketing/" + file.id
					})
					.promise();

				const hash = buffer.ETag.replace(/"/ig, "");

				await s3
					.putObject({
						Bucket: bucket,
						Key: file.folder + "/" + hash,
						Body: buffer.Body
					})
					.promise();

				await MySql.query(`
					UPDATE LMSDb.Attachment 
					SET RecStatus = ?, Hash = ?
					WHERE AttachmentId = ?
				`,
					["C", hash, file.id]
				);

			} catch (error) {
				logger.error(error);
				await MySql.query(`
					UPDATE LMSDb.Attachment 
					SET RecStatus = ?, Remark = ?
					WHERE AttachmentId = ?
				`,
					["D", error.message, file.id]
				);
			}
		},
		{
			concurrency: 1,
			stopOnError: false
		}
	);

	logger.debug("[handleAttachment] Program finished");
}

module.exports = handle;
