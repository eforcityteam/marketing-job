"use strict";
const initialize = require("../config");
const Listing = require("../utils/Listing");

async function update(job) {

	await initialize();

	let platform;

	switch (job?.data?.platform) {

		case "walmart":
			platform = new Listing.Walmart();
			break;

		default:
			throw new Error("Unknown market platform - " + platform);
	}

	await platform.process();
	return {};
}

module.exports = update;
