"use strict";
const map = require("p-map");
const AWS = require("aws-sdk");
const XLSX = require("xlsx");
const MySql = require("mysql-plugin");
const logger = require("../utils/logger");
const initial = require("../config");

const s3 = new AWS.S3({ region: "us-west-2" });

const COLUMNS = [
	"ProcId",
	"PONumber",
	"OrderNumber",
	"OrderDate",
	"ShipBy",
	"DeliveryDate",
	"CustomerName",
	"CustomerShippingAddress",
	"CustomerPhoneNumber",
	"ShipToAddress1",
	"ShipToAddress2",
	"City",
	"State",
	"Zip",
	"Segment",
	"FLIDS",
	"Line",
	"UPC",
	"Status",
	"ItemDescription",
	"ShippingMethod",
	"ShippingTier",
	"ShippingSLA",
	"ShippingConfigSource",
	"Qty",
	"SKU",
	"ItemCost",
	"ShippingCost",
	"Tax",
	"UpdateStatus",
	"UpdateQty",
	"Carrier",
	"TrackingNumber",
	"TrackingUrl",
	"SellerOrderNumber",
	"FulfillmentEntity",
	"ReplacementOrder",
	"OriginalCustomerOrderId"
];

async function processUploadWfsOrder() {

	logger.debug("[processUploadWfsOrder] Program Start");

	await initial();

	const jobs = await MySql.query(`
		SELECT
			ProcId as id,
			Bucket as bucket,
			BucketKey as bucketKey,
			FileName as filename
		FROM LMSDb.ProcQueueWFSOrder
		WHERE RecStatus = "Q";
	`);

	await map(
		jobs,
		async job => {

			const buffer = await s3
				.getObject({
					Bucket: job.bucket,
					Key: job.bucketKey
				})
				.promise()
				.then(response => response.Body);

			try {

				const workbook = XLSX.read(
					buffer,
					{
						type: "buffer",
						cellDates: true,
						cellNF: false,
						cellText: false
					}
				);
				let data = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], {
					header: 0,
					defval: "",
				});

				if (Object.keys(data[0])?.length !== 37) {
					throw Error("Excel column number not match")
				}

				data = data.map((row) => {
					const value = Object.values(row);
					value.unshift(job.id);
					return value;
				});

				await MySql
					.query(`
						INSERT INTO LMSDb.RawWFSOrder
						VALUES ?
						ON DUPLICATE KEY UPDATE ${COLUMNS.map((row) => `\`${row}\` = VALUES(\`${row}\`)`).join(", ")}
					`,
						[
							data
						]
					);

				await MySql
					.query(`
						UPDATE LMSDb.ProcQueueWFSOrder 
						SET RecStatus = ? 
						WHERE ProcId = ?
					`,
						[
							"C", job.id
						]
					);
				logger.debug(`[processUploadWfsOrder] Success to process file ${job.filename}`);
			} catch (error) {
				console.error(error);
				await MySql
					.query(`
						UPDATE LMSDb.ProcQueueWFSOrder 
						SET RecStatus = "E", Remark = ?
						WHERE ProcId = ?
					`,
						[
							error.message, job.id
						]
					);
				logger.debug(`[processUploadWfsOrder] Error:${error.message}`);
			}

		},
		{
			concurrency: 1
		}
	);

	logger.debug("[processUploadWfsOrder] Program completed");
	return true;
}

module.exports = processUploadWfsOrder;
