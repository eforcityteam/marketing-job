"use strict";
const initialize = require("../../config");
const Listing = require("../../utils/Listing");
const logger = require("../../utils/logger");
// const updateKrogerListing = require("./updateKrogerListing");

async function update() {

	await initialize();
	let platform;

	/**
	 * 
	 * eBay
	 * 
	 */
	platform = new Listing.EBay();

	await platform
		.process()
		.catch(error => {
			logger.error(error, { label: "updateListingTable" });
		});
}

module.exports = update;
