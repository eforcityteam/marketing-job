"use strict";
const axios = require("axios");
const MySql = require("mysql-plugin");
const initialize = require("../../config");
const logger = require("../../utils/logger");
const Account = require("../../account/account.model");
const updateListingTitleFromSync = require("../../utils/updateListingTitleFromSync");

const COLUMNS = [
	"MarketPlatformId",
	"SKU",
	"ProductSKU",
	"Title",
	"Quantity",
	"Price",
	"OfferId",
	"Category",
	"CategoryCode",
	"Active"
];

const DETAIL_COLUMNS = [
	"ListingId",
	"MarketplaceListingId",
	"Category",
	"ProductId",
	"Title",
	"ListingStatus"
];

async function scrape() {

	await initialize();

	const account = await Account.get("KROGER");

	let responses = [];

	while (true) {
		let response = await axios
			.get(
				account.credential.endpoint + "/api/offers", {
				params: { max: 100, offset: responses.length },
				headers: { Authorization: account.credential.token },
			})
			.then(response => response.data);

		if (response.offers.length < 1)
			break;
		responses = [].concat(responses, response.offers);
		logger.debug(`[Scrape Listing] Running on ${(responses.length / response.total_count * 100).toFixed(2)}%`);
	}

	await MySql.query(`
		INSERT INTO LMSDb.RawListingMirakl (??)
		VALUES ?
		ON DUPLICATE KEY UPDATE ${COLUMNS.map(column => `${column} = VALUES(${column})`).join(",")}
	`, [
		COLUMNS,
		responses.map(row => [
			account.marketPlatform, row.shop_sku, row.product_sku,
			row.product_title, row.quantity, row.price,
			row.offer_id, row.category_code, row.category_label,
			row.active
		])
	]);

	await MySql.query(`
		UPDATE 
			RawListingMirakl listing,
			ProductDb.ProductCodeMap map
		SET listing.ProductId = map.ProductId
		WHERE 
			map.ProductCode = listing.SKU
				AND 
			map.ProductCodeType = "AC" 
				AND 
			map.ProductCodeClassId = "EFC" 
				AND 
			listing.ProductId IS NULL;
	`);

	// IF (Active = 1, "ACTIVE", "TMPOFF") as ListingStatusId
	await MySql.query(`
		INSERT INTO LMSDb.Listing (
			MarketPlatformId, ProductListingId, ListingStatusId
		)
		SELECT 
			MarketPlatformId,
			SKU as ProductListingId,
			"ACTIVE"
		FROM RawListingMirakl 
		WHERE MarketPlatformId = "KROGER-97"
		ON DUPLICATE KEY UPDATE ListingStatusId = VALUES(ListingStatusId)
	`);

	await MySql.query(`
		INSERT INTO ListingDetail (??)
		SELECT 
			listing.ListingId,
			ProductSKU as MarketplaceListingId,
			CategoryCode,
			kroger.ProductId,
			Title as ProductTitle,
			detail.RecStatus as ListingStatus
		FROM RawListingMirakl kroger
		INNER JOIN Listing listing
		ON kroger.SKU = listing.ProductListingId
		LEFT JOIN ProductDb.ProductCodeMap map
		ON map.ProductId = kroger.ProductId AND map.ProductCodeType = "SKU" AND map.ProductCodeClassId = "EFC"
		LEFT JOIN ErpDb.ProductListingDetail detail
		ON detail.ProductListingId = listing.ProductListingId
		WHERE listing.MarketPlatformId = "KROGER-97" AND kroger.MarketPlatformId = "KROGER-97"
		ON DUPLICATE KEY UPDATE ${DETAIL_COLUMNS.map(column => `${column} = VALUES(${column})`).join(", ")};
	`, [DETAIL_COLUMNS]);

	await MySql.query(`
		INSERT INTO ListingPrice(ListingId, Price)
		SELECT ListingId, Price
		FROM RawListingMirakl mirakl
		INNER JOIN Listing listing
		ON mirakl.MarketPlatformId = listing.MarketPlatformId AND mirakl.SKU = listing.ProductListingId
		ON DUPLICATE KEY UPDATE Price = VALUES(Price);
	`);

	await updateListingTitleFromSync(["KROGER-97"]);
	
	logger.debug("[Scrape Listing] Finished");

	return true;
}

module.exports = scrape;
