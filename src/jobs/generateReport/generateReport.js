"use strict";
const MySql = require("mysql-plugin");
const getListingDatesReport = require("./getListingDatesReport");

async function generate(id) {

	const job = await MySql
		.query(`
			SELECT 
				JobId as id,
				report.JobCode as type,
				payload,
				fileId,
				alert,
				report.UserId,
				defUser.UserDisplayName as Username,
				defUser.UserEmail
			FROM ReportDb.ReportRequest report
			LEFT JOIN AuthDb.DefSecurityUser defUser
			ON defUser.UserId = report.UserId
			WHERE JobId = ?;
		`, [id])
		.then(([response]) => {
			if (!response)
				return null;
			const { UserId, Username, UserEmail, payload, ...task } = response;
			task.payload = JSON.parse(payload);
			task.alert = task.alert === "Y";
			task.user = UserId && {
				id: UserId,
				name: Username,
				email: UserEmail
			};
			return task;
		});

	if (!job)
		return {
			version: 2,
			status: "failure",
			message: "Job missed",
			memory: process.memoryUsage().heapUsed / 1024 / 1024
		};

	switch (job.type){
	case "request-listing-report":
		await getListingDatesReport(job);
		break;
	default:
		throw new Error("Invalid job type");
	}

	return job;
}

module.exports = generate;
