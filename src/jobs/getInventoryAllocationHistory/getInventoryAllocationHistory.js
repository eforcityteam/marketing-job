"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const path = require("path");
const Excel = require("exceljs");
const MySql = require("mysql-plugin");
const moment = require("moment");
const logger = require("../../utils/logger");
const initialize = require("../../config");

const EXCEL_PATH = path.join(__dirname, "./Listing Inventory Template.xlsx");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: true
});

async function handle() {

	await initialize();

	logger.debug("[getInventoryAllocationHistory] Program Started");

	const raw = await MySql
		.query(`
			SELECT procId, payload, bucketKey
			FROM LMSDb.ProcQueueInventoryAllocationHistory
			WHERE RecStatus = "U"
			ORDER BY EnterDate ASC
			LIMIT 1;
		`)
		.then((response) => response[0]);


	if (raw?.payload?.length > 0) {

		await MySql
			.query(`
				UPDATE LMSDb.ProcQueueInventoryAllocationHistory
				SET ?
				WHERE procId = ?
			`,
				[
					{
						RecStatus: "Q",
					},
					raw.procId
				]
			);

		try {
			const payload = JSON.parse(raw.payload);

			const workbook = new Excel.Workbook();

			const buffer = await workbook
				.xlsx
				.readFile(EXCEL_PATH)
				.then(async () => {
					const worksheet = workbook.getWorksheet(1);

					let index = 3;

					await map(
						_.chunk(payload.ids, 100),
						async (chunkedIds, chunkIndex) => {
							logger.debug(`[getInventoryAllocationHistory] Running on chunk: ${chunkIndex + 1}/${Math.ceil(payload.ids.length / 100)}`)

							const queries = JSON.parse(JSON.stringify(payload));
							queries.ids = chunkedIds;

							const conditions = getConditions(queries);
							const rows = await getRows(conditions, queries.start, queries.end);

							rows
								.forEach((row) => {
									row = Object.values(row);
									worksheet.insertRow(index, row);
									index++;
								});

						},
						{ concurrency: 1 }
					);

					const buffer = await workbook.xlsx.writeBuffer();
					return buffer;
				});

			const bucket = "eforcity-attachments-intelligent";
			const key = raw.bucketKey;
			const contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

			await s3
				.upload({
					Bucket: bucket,
					Key: key,
					Body: buffer,
					ContentType: contentType,
				})
				.promise();

			await MySql
				.query(`
					UPDATE LMSDb.ProcQueueInventoryAllocationHistory 
					SET RecStatus = "C"
					WHERE procId = ?
				`,
					[raw.procId]
				);

		} catch (error) {

			await MySql
				.query(`
					UPDATE LMSDb.ProcQueueInventoryAllocationHistory
					SET ?
					WHERE procId = ?
				`,
					[
						{
							RecStatus: "E",
							Remark: error.message
						},
						raw.procId
					]
				);
		}

	} else {
		return logger.debug("[getInventoryAllocationHistory] There is no task.");
	}

	logger.debug("[getInventoryAllocationHistory] Program finished");
}

function getConditions(queries) {

	const conditions = [
		MySql.format("listing.RecStatus = ?", "A"),
		MySql.format(`
			ListingId IN (
				SELECT ListingId FROM ListingInventory
			)
		`)
	];

	if (queries.place) {
		conditions.push(
			MySql.format(`
				listing.MarketPlatformId IN (
					SELECT MarketPlatformId FROM ErpDb.MarketPlatform WHERE MarketplaceId = ?
				)
			`, [
				queries.place
			])
		);
	}

	if (queries.status) {
		conditions.push(
			MySql.format("listing.ListingStatusId IN (?)", [queries.status])
		);
	}

	if (queries.procurementStatus) {

		if (!queries.place) {
			conditions.push(
				MySql.format(`
					listing.ListingId IN (
						SELECT ListingId
						FROM ListingDetail
						WHERE ProductId IN (
							SELECT ProductId FROM ProductDb.ProductExt WHERE procurementStatusId IN (?)
						)
					)
				`,
					[queries.procurementStatus]
				)
			);
		} else {
			conditions.push(
				MySql.format(`
					listing.ListingId IN (
						SELECT listing_id FROM ReportDb.uds_fact_listing WHERE procurement_status LIKE "${queries.procurementStatus}%"
					)
				`)
			);
		}
	}

	if (queries.ids && queries.ids.length > 0) {
		conditions.push(
			MySql.format(`(
				ReferenceNum In (?)
					OR
				listing.ListingId IN (
					SELECT ListingId
					FROM ListingDetail
					WHERE ProductId IN (
						SELECT ProductId
						FROM ProductDb.ProductCodeMap map
						WHERE
							map.ProductCodeType IN ("AC", "SKU", "ASIN")
								AND
							map.ProductCodeClassId IN ("EFC", "JUVO", "AMAZ")
								AND
							map.ProductCode IN (?)
					)
				)
			)`,
				[queries.ids, queries.ids]
			)
		);
	}

	return conditions;
}

function getRows(conditions, start, end) {

	start = moment(start).format("YYYY-MM-DD");
	end = moment(end).add(1, "day").format("YYYY-MM-DD");

	const sql = MySql.format(`
		SELECT
			ListingId,
			MarketPlatformId,
			IFNULL(listing.ReferenceNum, ProductListingId) as ChannelSKU,
			ASIN,
			efcSKU.ProductCode as SKU,
			MarketPlaceListingId,
			latest.Date,
			AssignedQty,

			LaunchDate,
			Title,
			detail.ProductId,
			RawInstock,
			DeliverrInStock,
			MCFInstock,
			OOSRisk,

			TargetQty,
			TargetSFQty,
			TargetMCFQty,
			IF(TargetStatus = "A", "Active", "Inactive"),
			TargetProcurementStatus,
			IF(TargetIsTop = "Y", "Yes", ""),
			IF(TargetIsNew = "Y", "Yes", ""),
			TargetDSS,

			WalmartQty,
			WalmartSFQty,
			WalmartMCFQty,
			IF(WalmartStatus = "A", "Active", "Inactive"),
			WalmartProcurementStatus,
			IF(WalmartIsNew = "Y", "Yes", ""),
			WalmartDSS,

			IF(WFSStatus = "A", "Active", "Inactive"),
			WFSQty,

			eBayQty,
			eBaySFQty,
			eBayMCFQty,
			IF(eBayStatus = "A", "Active", "Inactive"),
			eBayProcurementStatus,
			IF(eBayIsNew = "Y", "Yes", ""),
			eBayDSS,

			OtherQty,
			OtherSFQty,
			OtherMCFQty,
			IF(OtherStatus = "A", "Active", "Inactive"),
			OtherProcurementStatus,
			IF(OtherIsNew = "Y", "Yes", ""),
			OtherDSS,

			AllocationRule
		FROM ListingInventoryLog log
		INNER JOIN Listing listing
		USING (ListingId)
		INNER JOIN ListingDetail detail
		USING (ListingId)
		LEFT JOIN ProductDb.ProductPrimaryCode efcSKU
		ON efcSKU.ProductId = detail.ProductId AND efcSKU.ProductCodeTypeId = "SKU" AND efcSKU.ProductCodeClassId = "EFC"
		INNER JOIN (
			SELECT
				ListingId,
				DATE(LogDate) as Date,
				MAX(LogDate) as LogDate
			FROM ListingInventoryLog
			WHERE 
				ListingId IN (
					SELECT ListingId
					FROM Listing listing
					WHERE ${conditions.join(" AND ")}
				) 
					AND 
				LogDate >= ? AND LogDate <= ?
			GROUP BY ListingId, DATE(LogDate)
		) latest
		USING (ListingId, LogDate)
		LEFT JOIN (
			SELECT
				Date(LogDate) as date,
				ProductChannelInventoryLog.*
			FROM ProductChannelInventoryLog
			INNER JOIN (
				SELECT
					ProductId,
					MAX(LogDate) as LogDate
				FROM ProductChannelInventoryLog
				WHERE
					ProductId IN (
						SELECT ProductId 
						FROM ListingDetail 
						WHERE ListingId IN (
							SELECT ListingId
							FROM Listing listing
							WHERE ${conditions.join(" AND ")}
						)
					)
						AND
					LogDate >= ? AND LogDate <= ?
				GROUP BY ProductId, DATE(LogDate)
			) latest
			USING (ProductId, LogDate)
		) formula
		ON formula.ProductId = detail.ProductId AND formula.Date = latest.Date
	`, [
		start,
		end,
		start,
		end
	]);

	return MySql.query(sql);
}

module.exports = handle;
