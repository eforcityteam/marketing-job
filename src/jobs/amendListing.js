"use strict";
const MySql = require("mysql-plugin");

async function amend() {

	await MySql.query(`
		UPDATE 
			ListingDetail,
			(
				SELECT 
					listing.ListingId, 
					product.ProductId,
					listingDetail.ComponentType
				FROM LMSDb.ListingDetail detail 
				LEFT JOIN LMSDb.Listing listing 
				ON detail.ListingId = listing.ListingId
				LEFT JOIN ErpDb.ProductListingDetail listingDetail
				ON listingDetail.ProductListingId = listing.ProductListingId
				LEFT JOIN ProductDb.ProductPrimaryCode product
				ON product.ProductCodeTypeId = "SKU" 
					AND product.ProductCodeClassId = "EFC" 
					AND product.ProductCode = listingDetail.ProductId 
					AND listingDetail.ComponentType = "S"
				WHERE detail.ProductId IS NULL
					AND (listingDetail.ProductId IS NOT NULL)
					AND (detail.ProductType IS NULL OR (listingDetail.ComponentType = "S" AND product.ProductId IS NOT NULL))
			) temp
		SET ListingDetail.ProductId = temp.ProductId, ListingDetail.ProductType = temp.ComponentType
		WHERE ListingDetail.ListingId = temp.ListingId;
	`);

}

module.exports = amend;
