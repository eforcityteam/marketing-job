"use strict";
const logger = require("../utils/logger");
const getDatabase = require("../utils/getDatabase");

async function update() {

	const knex = await getDatabase();

	await knex.raw(`
		UPDATE
			ListingDetail,
			(
				SELECT * FROM (
					SELECT 
						listing.ListingId,
						map.ProductId
					FROM Listing listing
					LEFT JOIN ListingDetail detail
					ON detail.ListingId = listing.ListingId
					INNER JOIN ProductDb.ProductCodeMap map
					ON listing.ProductListingId = map.ProductCode AND ProductCodeClassId = "EFC" AND ProductCodeType = "AC"
					WHERE detail.ProductId <> map.ProductId OR detail.ProductId IS NULL
				) temp 
			) temp
		SET ListingDetail.ProductId = temp.ProductId
		WHERE ListingDetail.ListingId = temp.ListingId;
	`);

	logger.debug("[updateListingDetailProductId] Program Completed");
}

module.exports = update;
