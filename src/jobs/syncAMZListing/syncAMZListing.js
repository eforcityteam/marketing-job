"use strict";
const _ = require("lodash");
const map = require("p-map");
const retry = require("p-retry");
const stream = require("stream");
const moment = require("moment");
const MySql = require("mysql-plugin");
const { ClickHouse } = require("clickhouse");
const { promisify } = require("util");

const initialize = require("../../config");
const logger = require("../../utils/logger");

const COLUMN_MAP = require("./column.map.json");
const pipeline = promisify(stream.pipeline);

async function synchronize() {

	logger.debug("[syncAMZListing] Started");

	const configs = await initialize();
	const clickhouse = new ClickHouse(configs.clickhouse.juvo);

	const lastUpdate = await MySql
		.query(`
			SELECT MAX(RequestDate) as lastUpdate
			FROM LMSDb.RawListingDetailAmazon
		`)
		.then((row) => moment(row[0]?.lastUpdate).subtract("3", "days").format("YYYY-MM-DD"));

	//Update amz_listing_uk and amz_listing_de
	await update(`
		SELECT 
			channel.sales_channel_id as \`amz.sales_channel_id\`,
			channel.country as \`amz.market\`,
			amz.*
		FROM (SELECT * FROM ods.amz_listing_uk WHERE request_date > '${lastUpdate}') amz
		INNER JOIN (SELECT * FROM dws.fact_listing_1d WHERE efc_channel_id = 'AMAZON-K2') listing
		ON 
			amz.sku = listing.sku
				AND
			amz.listing_id = listing.channel_listing_id
				AND
			IF(amz.fulfilled_by = 'Amazon', 'channel fulfillment', 'self fulfillment') = listing.fulfillment_type
		INNER JOIN dws.dim_channel_1d channel
		ON channel.channel_id = listing.channel_id
		INNER JOIN (
		SELECT
			max(request_date) as request_date,
			sku,
			listing_id
		FROM ods.amz_listing_uk
		WHERE fulfilled_by = 'Amazon'
		GROUP BY sku, listing_id
		) latest
		ON
			amz.sku = latest.sku
				AND
			amz.listing_id = latest.listing_id
				AND
			amz.request_date = latest.request_date
		
		UNION ALL
		
		SELECT
			channel.sales_channel_id as \`amz.sales_channel_id\`,
			channel.country as \`amz.market\`,
			amz.*
		FROM (SELECT * FROM ods.amz_listing_de WHERE request_date > '${lastUpdate}' ) amz
		INNER JOIN dws.fact_listing_1d listing
		ON 
			amz.sku = listing.sku
				AND
			amz.listing_id = listing.channel_listing_id
				AND
			IF(amz.fulfilled_by = 'Amazon', 'channel fulfillment', 'self fulfillment') = listing.fulfillment_type
		INNER JOIN (SELECT * FROM dws.dim_channel_1d WHERE platform = 'amazon' AND country = 'DE' AND channel_status = 'Active' ) channel
		ON channel.channel_id = listing.channel_id
		INNER JOIN (
			SELECT
				max(request_date) as request_date,
				sku,
				listing_id,
				fulfilled_by
			FROM ods.amz_listing_de
			GROUP BY sku, listing_id, fulfilled_by
		) latest
		ON
			amz.sku = latest.sku
				AND
			amz.listing_id = latest.listing_id
				AND
			amz.fulfilled_by = latest.fulfilled_by
				AND
			amz.request_date = latest.request_date
	`, clickhouse, true);

	logger.debug("[syncAMZListing] Added raw data from amz_listing_uk and amz_listing_de");

	//Update amz_listing
	await update(`
		SELECT amz.*
		FROM (SELECT * FROM ods.amz_listing WHERE request_date > '${lastUpdate}') amz
		INNER JOIN (
			SELECT
				max(request_date) as request_date,
				sales_channel_id,
				market,
				sku,
				fulfilled_by
			FROM ods.amz_listing
			GROUP BY sales_channel_id, market, sku, fulfilled_by
		) latest
		ON
			amz.sales_channel_id = latest.sales_channel_id
				AND
			amz.market = latest.market
				AND
			amz.sku = latest.sku
				AND
			amz.fulfilled_by = latest.fulfilled_by
				AND
			amz.request_date = latest.request_date
	`, clickhouse);

	logger.debug("[syncAMZListing] Added raw data from amz_listing");

	//Update MarketPlatformId and ProductId
	await MySql.query(`
		UPDATE
			LMSDb.RawListingDetailAmazon amazon,
			ErpDb.MarketPlatform platform
		SET amazon.MarketPlatformId = platform.MarketPlatformId
		WHERE 
			platform.JuvoSalesChannelId = amazon.SalesChannelId 
				AND 
			amazon.Market = platform.Country 
				AND 
			platform.FulfillmentType = IF(amazon.FulfilledBy = 'Amazon', 'FS', 'WH') 
				AND 
			amazon.FulfilledBy <> '';
	`);

	await MySql.query(`
		UPDATE
			RawListingDetailAmazon amazon,
			ProductDb.ProductCodeMap map
		SET amazon.ProductId = map.ProductId
		WHERE 
			amazon.ASIN = map.ProductCode 
				AND 
			map.ProductCodeType = "ASIN"
				AND
			map.RecStatus = "A";
	`);

	logger.debug("[syncAMZListing] Finished");
}

async function update(query, clickhouse, withPrefix) {

	const rows = [];

	await pipeline(
		clickhouse
			.query(query)
			.stream()
		,
		new stream.Transform({
			objectMode: true,
			transform: async function (data, encoding, callback) {
				rows.push(
					_.map(COLUMN_MAP, (value, key) => data[withPrefix ? `amz.${key}` : key])
				);
				callback();
			}
		})
	);

	await map(
		_.chunk(rows, 5000),
		rows => retry(
			() => MySql
				.query(`
					INSERT INTO LMSDb.RawListingDetailAmazon(??) 
					VALUES ?
					ON DUPLICATE KEY UPDATE 
						${Object.values(COLUMN_MAP).map(item => `\`${item}\` = VALUES(\`${item}\`)`).join(",\n")};
				`, [
					Object.values(COLUMN_MAP),
					rows
				]),
			{
				retries: 3
			}
		),
		{
			concurrency: 1
		}
	);

}

module.exports = synchronize;
