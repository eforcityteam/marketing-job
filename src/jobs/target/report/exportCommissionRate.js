"use strict";
const _ = require("lodash");
const moment = require("moment");
const MySql = require("mysql-plugin");
const logger = require("../../../utils/logger");
const initialize = require("../../../config");

async function exportCommissionRate() {

    await initialize();

    try {
        logger.info(`[TargetCommissionRate] Getting Settlement Report from DB`);

        const date = await MySql.query("SELECT MAX(LastUpdate) as lastUpdate FROM LMSDb.RawSettlementTarget");

        const settlement = await MySql
            .query(`
                SELECT
                    detail.Category as ProductType,
                    settlement.ReferralRate as ReferralFeeRate
                FROM LMSDb.RawSettlementTarget settlement
                LEFT JOIN LMSDb.Listing listing
                ON listing.ProductListingId = SUBSTRING_INDEX(settlement.SellerSKU, '_', 1)
                LEFT JOIN LMSDb.ListingDetail detail
                ON detail.ListingId = listing.ListingId
                WHERE settlement.Type = "PURCHASE" AND ListingStatusId = "ACTIVE" 
                GROUP BY detail.Category, settlement.ReferralRate
                ORDER BY detail.Category;
            `, [moment(date[0].lastUpdate).subtract(12, "hours").toDate()])
            .then(rows =>
                rows.filter(row => row.ProductType).map(row =>
                    _.map(row, (value, key) => {
                        if (key == "ReferralFeeRate") {
                            return value * 100;
                        };
                        return value;
                    })
                )
            );

        const columns = [
            "ProductType", "ReferralFeeRate"
        ];

        for (const [index, _rows] of _.chunk(settlement, 10000).entries()) {
            logger.debug(`[TargetCommissionRate] Running on chunk #${index + 1} of ${_.ceil(settlement.length / 10000)} chunks`);
            await MySql.query(`
                    INSERT INTO LMSDb.RawReferralFeeTarget (??) VALUES ?
                    ON DUPLICATE KEY UPDATE ReferralFeeRate = VALUES(ReferralFeeRate);
                `, [columns, _rows]
            );
        };

        logger.info(`[TargetCommissionRate] All Exportation Done`);

    } catch (error) {
        logger.error(error);
    }
}

module.exports = exportCommissionRate;
