"use strict";
const _ = require("lodash");
const moment = require("moment");
const MySql = require("mysql-plugin");
const excel = require("exceljs");
const map = require("p-map");
const logger = require("../../../utils/logger");
const initialize = require("../../../config");

async function generateExcel() {

    await initialize();

    const summary = await MySql
        .query(`
            SELECT
                Date as date,
                ListingQuality as listingQuality,
                OfferScore as offerScore,
                RatingReviewScore as ratingReviewScore,
                ContentScore as contentScore,
                ItemDefectCount as itemDefectCount,
                DefectRatio as defectRatio
            FROM LMSDb.RawListingQualityScoreWalmart
            WHERE RecStatus = "A"
            ORDER BY Date DESC
            LIMIT 2;
        `)
        .then(response => {
            response = response.map(item => {
                item.date = moment(item.date).format("YYYY-MM-DD");
                return item;
            })
            return response;
        });

    const currentDate = summary[0].date;

    try {

        const workbook = new excel.Workbook(); //creating workbook
        let worksheet;

        /**
         * 
         *   Page 1 : Score Summary
         * 
         */
        worksheet = workbook.addWorksheet("Score Summary"); //creating worksheet

        worksheet.columns = [
            { header: "Date", key: "date" },
            { header: "Listing Quality", key: "listingQuality" },
            { header: "Offer Score", key: "offerScore" },
            { header: "Rating Review Score", key: "ratingReviewScore" },
            { header: "Content Score", key: "contentScore" },
            { header: "Item Defect Count", key: "itemDefectCount" },
            { header: "Defect Ratio", key: "defectRatio" },
        ];

        logger.debug(`[listingQuality - generateExcel] Running on ${currentDate} job.`);

        worksheet.addRows(summary.reverse());

        const listingQualityPercentage = parseFloat(((summary[1].listingQuality - summary[0].listingQuality) / summary[0].listingQuality * 100).toFixed(2));
        const offerScorePercentage = parseFloat(((summary[1].offerScore - summary[0].offerScore) / summary[0].offerScore * 100).toFixed(2));
        const ratingReviewScorePercentage = parseFloat(((summary[1].ratingReviewScore - summary[0].ratingReviewScore) / summary[0].ratingReviewScore * 100).toFixed(2));
        const contentScorePercentage = parseFloat(((summary[1].contentScore - summary[0].contentScore) / summary[0].contentScore * 100).toFixed(2));
        const itemDefectCountPercentage = parseFloat(((summary[1].itemDefectCount - summary[0].itemDefectCount) / summary[0].itemDefectCount * 100).toFixed(2));
        const defectRatioPercentage = parseFloat(((summary[1].defectRatio - summary[0].defectRatio) / summary[0].defectRatio * 100).toFixed(2));

        const colorMap = [
            "000000",
            listingQualityPercentage >= 0 ? "228B22" : "8b0000",
            offerScorePercentage >= 0 ? "228B22" : "8b0000",
            ratingReviewScorePercentage >= 0 ? "228B22" : "8b0000",
            contentScorePercentage >= 0 ? "228B22" : "8b0000",
            itemDefectCountPercentage >= 0 ? "8b0000" : "228B22",
            defectRatioPercentage >= 0 ? "8b0000" : "228B22",
        ];

        worksheet.addRow({
            date: "Performance Compare%",
            listingQuality: listingQualityPercentage,
            offerScore: offerScorePercentage,
            ratingReviewScore: ratingReviewScorePercentage,
            contentScore: contentScorePercentage,
            itemDefectCount: itemDefectCountPercentage,
            defectRatio: defectRatioPercentage,
        });

        worksheet.autoFilter = "A1:G1";

        ["A1", "B1", "C1", "D1", "E1", "F1", "G1"].map(key => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
            };

            worksheet.getCell(key).border = {
                bottom: { style: 'thin' },
            };
        });

        ["A4", "B4", "C4", "D4", "E4", "F4", "G4"].map((key, index) => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
                color: {
                    argb: colorMap[index]
                }
            };
        });

        //Auto width
        worksheet.columns.forEach(function (column, i) {
            var maxLength = 0;
            column["eachCell"]({ includeEmpty: true }, function (cell) {
                var columnLength = cell.value ? cell.value.toString().length : 10;
                if (columnLength > maxLength) {
                    maxLength = columnLength;
                }
            });
            column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
        });

        worksheet.getCell("A6").value = "Index";
        worksheet.getCell("A7").value = { text: "Discon Listings", hyperlink: `#\'Discon Listings\'!A1` };
        worksheet.getCell("A8").value = { text: "Unpublish Listings", hyperlink: `#\'Unpublish Listings\'!A1` };
        worksheet.getCell("A9").value = { text: "Price Issues", hyperlink: `#\'Price Issues\'!A1` };
        worksheet.getCell("A10").value = { text: "Listings Score Compare", hyperlink: `#\'Listings Score Compare\'!A1` };
        worksheet.getCell("A11").value = { text: "Top problematic Attribute List", hyperlink: `#\'Top problematic Attribute List\'!A1` };

        worksheet.getCell("A6").font = {
            name: "Calibri (Body)",
            family: 1,
            size: 13,
            underline: false,
            bold: true,

        };
        worksheet.getCell("A6").border = {
            bottom: { style: 'thin' },
        };
        ["A7", "A8", "A9", "A10", "A11"].map((key, index) => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
                color: { argb: "0000EE" },
                underline: true,
            };
        });

        /**
         * 
         *   Page 2 : Discon Listings
         * 
         */
        worksheet = workbook.addWorksheet("Discon Listings"); //creating worksheet

        worksheet.columns = [
            { header: "SKU", key: "sku" },
            { header: "Item ID", key: "itemID" },
            { header: "Product Name", key: "productName" },
            { header: "Lifecycle Status", key: "lifecycleStatus" },
            { header: "Publish Status", key: "publishStatus" },
            { header: "Product Category", key: "productCategory" },
            { header: "Fulfillment Type", key: "fulfillmentType" }
        ];


        const disconListings = await MySql
            .query(`
                SELECT
                    SKU as sku,
                    ItemID as itemID,
                    ProductName as productName,
                    LifecycleStatus as lifecycleStatus,
                    PublishStatus as publishStatus,
                    ProductCategory as productCategory,
                    FulfillmentType as fulfillmentType
                FROM RawListingWalmart
                WHERE
                    LifecycleStatus <> "RETIRED"
                        AND
                    ProductId IN (
                        SELECT ProductId FROM ProductDb.ProductExt WHERE ActiveTag = 0
                    )
            `);

        worksheet.addRows(disconListings);

        worksheet.autoFilter = "A1:G1";

        ["A1", "B1", "C1", "D1", "E1", "F1", "G1"].map(key => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
            };

            worksheet.getCell(key).border = {
                bottom: { style: 'thin' },
            };
        });

        //Auto width
        worksheet.columns.forEach(function (column, i) {
            var maxLength = 0;
            column["eachCell"]({ includeEmpty: true }, function (cell) {
                var columnLength = cell.value ? cell.value.toString().length : 10;
                if (columnLength > maxLength) {
                    maxLength = columnLength;
                }
            });
            column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
        });


        /**
         * 
         *   Page 3 : Unpublish Listings
         * 
         */
        worksheet = workbook.addWorksheet("Unpublish Listings"); //creating worksheet

        worksheet.columns = [
            { header: "SKU", key: "sku" },
            { header: "Item ID", key: "itemID" },
            { header: "Product Name", key: "productName" },
            { header: "Lifecycle Status", key: "lifecycleStatus" },
            { header: "Publish Status", key: "publishStatus" },
            { header: "Product Category", key: "productCategory" },
            { header: "Fulfillment Type", key: "fulfillmentType" },
            { header: "Status Change Reason", key: "statusChangeReason" }
        ];

        const unpublishListings = await MySql
            .query(`
                SELECT
                    SKU as sku,
                    ItemID as itemID,
                    ProductName as productName,
                    LifecycleStatus as lifecycleStatus,
                    PublishStatus as publishStatus,
                    ProductCategory as productCategory,
                    FulfillmentType as fulfillmentType,
                    StatusChangeReason as statusChangeReason
                FROM RawListingWalmart
                WHERE
                    LifecycleStatus <> "RETIRED"
                        AND
                    ProductId IN (
                        SELECT ProductId FROM ProductDb.ProductExt WHERE ActiveTag = 1
                    )
                        AND
                    PublishStatus <> "PUBLISHED";
            `);

        worksheet.addRows(unpublishListings);

        worksheet.autoFilter = "A1:H1";

        ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1"].map(key => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
            };

            worksheet.getCell(key).border = {
                bottom: { style: 'thin' },
            };
        });

        //Auto width
        worksheet.columns.forEach(function (column, i) {
            var maxLength = 0;
            column["eachCell"]({ includeEmpty: true }, function (cell) {
                var columnLength = cell.value ? cell.value.toString().length : 10;
                if (columnLength > maxLength) {
                    maxLength = columnLength;
                }
            });
            column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
        });

        /**
         * 
         *   Page 4 : Price Issues
         * 
         */
        worksheet = workbook.addWorksheet("Price Issues"); //creating worksheet

        worksheet.columns = [
            { header: "SKU", key: "sku" },
            { header: "Item ID", key: "itemID" },
            { header: "Product Name", key: "productName" },
            { header: "Lifecycle Status", key: "lifecycleStatus" },
            { header: "Publish Status", key: "publishStatus" },
            { header: "Product Category", key: "productCategory" },
            { header: "Fulfillment Type", key: "fulfillmentType" },
            { header: "Status Change Reason", key: "statusChangeReason" },
            { header: "Current Price", key: "currentPrice" },
            { header: "Estimated Breakeven", key: "estimatedBreakeven" },
        ];

        const pricesIssues = await MySql
            .query(`
                SELECT
                    walmart.SKU as sku,
                    ItemID as itemID,
                    ProductName as productName,
                    LifecycleStatus as lifecycleStatus,
                    walmart.PublishStatus as publishStatus,
                    ProductCategory as productCategory,
                    FulfillmentType as fulfillmentType,
                    StatusChangeReason as statusChangeReason,
                    walmart.Price as currentPrice,
                    breakeven.EstimatedBreakeven as estimatedBreakeven
                FROM RawListingWalmart walmart
                LEFT JOIN Listing listing
                ON listing.ReferenceNum = walmart.SKU
                LEFT JOIN ListingBreakeven breakeven
                ON breakeven.ListingId = listing.ListingId
                WHERE
                    LifecycleStatus <> "RETIRED"
                        AND
                    walmart.ProductId IN (
                        SELECT ProductId FROM ProductDb.ProductExt WHERE ActiveTag = 1
                    )
                        AND
					walmart.PublishStatus <> "PUBLISHED"
                        AND
                    StatusChangeReason LIKE "%Reasonable price%"
                        AND
                    listing.MarketPlatformId IN ("WALMART-FS", "WALMART-26");
            `);

        worksheet.addRows(pricesIssues);

        worksheet.autoFilter = "A1:J1";

        ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "I1", "J1"].map(key => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
            };

            worksheet.getCell(key).border = {
                bottom: { style: 'thin' },
            };
        });

        //Auto width
        worksheet.columns.forEach(function (column, i) {
            var maxLength = 0;
            column["eachCell"]({ includeEmpty: true }, function (cell) {
                var columnLength = cell.value ? cell.value.toString().length : 10;
                if (columnLength > maxLength) {
                    maxLength = columnLength;
                }
            });
            column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
        });

        /**
         * 
         *   Page 5 Listings Score Compare
         * 
         */
        worksheet = workbook.addWorksheet("Listings Score Compare"); //creating next page worksheet

        worksheet.columns = [
            { header: "SKU", key: "sku" },
            { header: "Item ID", key: "itemID" },
            { header: "Product Id", key: "productId" },
            { header: "Updated Time", key: "updatedTimestamp" },
            { header: "Priority", key: "priority" },
            { header: "Score (4 days ago)", key: "four_day_ago" },
            { header: "Score (3 days ago)", key: "three_day_ago" },
            { header: "Score (2 days ago)", key: "two_day_ago" },
            { header: "Score (Last Day)", key: "one_day_ago" },
            { header: "Score (Today)", key: "today" },
            { header: "Score Change%", key: "percentage" },
        ];

        const scoresList = await MySql
            .query(`
                SELECT
                    ItemID as itemID,
                    SKU as sku,
                    ProductId as productId,
                    UpdatedTimestamp as updatedTimestamp,
                    Priority as priority, 
                    Score as score,
                    LoadDate as loadDate
                FROM LMSDb.RawListingQualityWalmart 
                WHERE
                    DATE(LoadDate) >= ? - INTERVAL 4 DAY
                    AND RecStatus = "A"
            `,
                [
                    currentDate
                ]
            )
            .then(response => _
                .chain(response)
                .groupBy("itemID")
                .map((value) => {
                    // console.log(value)
                    const four_day_ago = value.find(item => moment(item.loadDate).format("YYYY-MM-DD") == moment(currentDate).subtract(4, "days").format("YYYY-MM-DD"))?.score ?? null;
                    const three_day_ago = value.find(item => moment(item.loadDate).format("YYYY-MM-DD") == moment(currentDate).subtract(3, "days").format("YYYY-MM-DD"))?.score ?? null;
                    const two_day_ago = value.find(item => moment(item.loadDate).format("YYYY-MM-DD") == moment(currentDate).subtract(2, "days").format("YYYY-MM-DD"))?.score ?? null;
                    const one_day_ago = value.find(item => moment(item.loadDate).format("YYYY-MM-DD") == moment(currentDate).subtract(1, "days").format("YYYY-MM-DD"))?.score ?? null;
                    const today = value.find(item => moment(item.loadDate).format("YYYY-MM-DD") == moment(currentDate).format("YYYY-MM-DD"))?.score ?? null;

                    const lastEffectDayScore =
                        four_day_ago ? four_day_ago :
                            (
                                three_day_ago ? three_day_ago :
                                    (
                                        two_day_ago ? two_day_ago :
                                            (
                                                one_day_ago ? one_day_ago : today
                                            )
                                    )
                            );

                    const percentage = today ? parseFloat(((today - lastEffectDayScore) / lastEffectDayScore * 100).toFixed(2)) || null : null;

                    return {
                        itemID: value[0].itemID,
                        sku: value[0].sku,
                        productId: value[0].productId,
                        updatedTimestamp: value[0].updatedTimestamp,
                        priority: value[0].priority,
                        four_day_ago: four_day_ago,
                        three_day_ago: three_day_ago,
                        two_day_ago: two_day_ago,
                        one_day_ago: one_day_ago,
                        today: today,
                        percentage: percentage,
                    }
                })
                .sortBy("percentage")
                .value()
            );

        worksheet.addRows(scoresList);

        worksheet.autoFilter = "A1:K1";

        ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "I1", "J1", "K1"].map(key => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
            };

            worksheet.getCell(key).border = {
                bottom: { style: 'thin' },
            };
        });

        //Auto width
        worksheet.columns.forEach(function (column, i) {
            var maxLength = 0;
            column["eachCell"]({ includeEmpty: true }, function (cell) {
                var columnLength = cell.value ? cell.value.toString().length : 10;
                if (columnLength > maxLength) {
                    maxLength = columnLength;
                }
            });
            column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
        });

        /**
         * 
         *   Page 6  Top problematic Attribute List
         * 
         */
        worksheet = workbook.addWorksheet("Top problematic Attribute List"); //creating next page worksheet

        worksheet.columns = [
            { header: "Attribute Name", key: "attributeName", width: 30 },
            { header: "Count", key: "count", width: 30 },
        ];

        const issuesLists = await MySql
            .query(`
                SELECT
                    AttributeName as attributeName,
                    COUNT(*) as count
                FROM LMSDb.RawListingQualityIssuesWalmart
                WHERE 
                    Score <= 70 
                    AND LoadDate = ?
                GROUP BY AttributeName
                ORDER BY count DESC;
            `,
                [
                    currentDate
                ]
            );

        let row = 2;
        let wordExceedCounter = 0;

        for (let issue of issuesLists) {

            worksheet.addRow({
                attributeName: issue.attributeName,
                count: issue.count
            });

            //Add internal hyper link jumping
            worksheet.getCell(`A${row}`).value = { text: issue.attributeName, hyperlink: `#\'${issue.attributeName.length >= 31 ? `${issue.attributeName.slice(0, 26)}_${++wordExceedCounter}` : issue.attributeName}\'!A1` };
            worksheet.getCell(`A${row++}`).font = {
                color: { argb: "0000EE" },
                underline: true,
            };
        }

        worksheet.autoFilter = "A1:B1";

        ["A1", "B1"].map(key => {
            worksheet.getCell(key).font = {
                name: "Calibri (Body)",
                family: 1,
                size: 13,
                underline: false,
                bold: true,
            };

            worksheet.getCell(key).border = {
                bottom: { style: 'thin' },
            };
        });

        //Auto width
        worksheet.columns.forEach(function (column, i) {
            var maxLength = 0;
            column["eachCell"]({ includeEmpty: true }, function (cell) {
                var columnLength = cell.value ? cell.value.toString().length : 10;
                if (columnLength > maxLength) {
                    maxLength = columnLength;
                }
            });
            column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
        });

        /**
         * 
         *   Others Page: issue details pages
         * 
         */
        wordExceedCounter = 0;

        await map(
            _.chunk(issuesLists, 50),
            async (issuesList, index) => {

                logger.debug(`[listingQuality - generateExcel] Running on chunk #${index + 1}/${Math.ceil(issuesLists.length / 50)}.`);

                const issuesDetail = await MySql
                    .query(`
                        SELECT
                            ItemID as itemID,
                            SKU as sku,
                            ProductId as productId,
                            Score as score,
                            AttributeName as attributeName,
                            AttributeValue as attributeValue,
                            IssueCount as issueCount,
                            Issues as issues
                        FROM LMSDb.RawListingQualityIssuesWalmart
                        WHERE 
                            AttributeName in (?)
                            AND Score <= 70 
                            AND LoadDate = ?
                        ORDER BY score ASC;
                    `,
                        [
                            issuesList.map(item => item.attributeName),
                            currentDate
                        ]
                    )
                    .then(response => _.groupBy(response, "attributeName"));

                for (let issue of issuesList) {

                    worksheet = workbook.addWorksheet(issue.attributeName.length >= 31 ? `${issue.attributeName.slice(0, 26)}_${++wordExceedCounter}` : issue.attributeName); //creating worksheet for each terms

                    //Set First row title
                    worksheet.getRow(1).getCell(1).value = issue.attributeName;
                    worksheet.getRow(1).getCell(2).value = { text: "Back to Attribute List", hyperlink: `#\'Top problematic Attribute List\'!A1` };
                    worksheet.getRow(1).getCell(2).font = {
                        name: "Calibri (Body)",
                        family: 1,
                        size: 14,
                        underline: true,
                        bold: true,
                        color: { argb: "0000EE" },
                    };

                    worksheet.getCell("A1").font = {
                        name: "Calibri (Body)",
                        family: 1,
                        size: 14,
                        underline: true,
                        bold: true
                    };

                    //  First column header 
                    worksheet.getRow(3).values = [
                        "SKU", "Item ID",
                        "Product Id", "Score",
                        "Issues",
                        "Attribute Value"
                    ];

                    worksheet.columns = [
                        { key: "sku", width: 20 },
                        { key: "itemID", width: 20 },
                        { key: "productId", width: 20 },
                        { key: "score", width: 20 },
                        { key: "issues", width: 100 },
                        { key: "attributeValue", width: 100 },
                    ];

                    worksheet.addRows(issuesDetail[issue.attributeName]);

                    worksheet.autoFilter = "A3:F3";

                    // First column header styling
                    ["A3", "B3", "C3", "D3", "E3", "F3"].map(key => {
                        worksheet.getCell(key).font = {
                            name: "Calibri (Body)",
                            family: 1,
                            size: 13,
                            underline: false,
                            bold: true,
                        };

                        worksheet.getCell(key).border = {
                            bottom: { style: 'thin' },
                        };
                    });

                    //Auto width
                    worksheet.columns.forEach(function (column, i) {
                        var maxLength = 0;
                        column["eachCell"]({ includeEmpty: true }, function (cell) {
                            var columnLength = cell.value ? cell.value.toString().length : 10;
                            if (columnLength > maxLength) {
                                maxLength = columnLength;
                            }
                        });
                        column.width = maxLength < 10 ? 10 : maxLength + 3; //+3 for auto filter space
                    });

                }

            },
            { concurrency: 1 }
        );

        // await workbook.xlsx.writeFile("./test.xlsx");
        // console.log("done")

        const buffer = await workbook.xlsx.writeBuffer();
        const fileName = `walmart-listing-quality-report-${moment(currentDate).format("YYYY-MM-DD")}.xlsx`;

        logger.debug(`[listingQuality - generateExcel] ${fileName} excel export done!`);

        return {
            fileName: fileName,
            buffer: buffer,
            status: "C",
        };

    } catch (error) {
        console.error(error?.response?.status ?? error.message);
        return {
            status: "E",
            fileName: `walmart-listing-quality-report-${moment(currentDate).format("YYYY-MM-DD")}.xlsx`,
            message: error?.response?.status ?? error.message
        };
    }

}
// generateExcel()
module.exports = generateExcel; 
