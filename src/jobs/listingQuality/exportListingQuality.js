"use strict";
const AWS = require("aws-sdk");
const MySql = require("mysql-plugin");
const initialize = require("../../config");
const logger = require("../../utils/logger");
const generateExcel = require("./utils/generateExcel");

const s3 = new AWS.S3({
    region: "us-west-2",
    useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

async function exportListingQuality() {

    await initialize();

    /**
     * 
     *  Check if need to run job
     * 
     */

    const [sourceDate, lastUpdate] = await MySql
        .query(`
            SELECT MAX(DATE(Date)) as date
            FROM LMSDb.RawListingQualityScoreWalmart;

            SELECT MAX(DATE(LastUpdate)) as date
			FROM LMSDb.Attachment
			WHERE AttachmentTypeId = "WALQLY";
        `);

    if (sourceDate[0]?.date === lastUpdate[0]?.date) {
        logger.debug(`[listingQuality - exportListingQuality] job is up to date, end process.`);
        return false;
    }

    /**
     * 
     *  main job
     * 
     */
    logger.debug(`[listingQuality - exportListingQuality] start to process.`);

    const data = await generateExcel();

    const bucket = "eforcity-attachments-intelligent";
    const key = `walmart-listing-quality-report/${data.fileName}`;
    const body = data.buffer;
    const contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    const size = data?.buffer?.length;
    const fileName = data.fileName;

    if (data.status === "C") {

        await s3
            .upload({
                Bucket: bucket,
                Key: key,
                Body: body,
                ContentType: contentType,
            })
            .promise();

        await MySql
            .query(`
                INSERT INTO LMSDb.Attachment (AttachmentTypeId, BucketFolder, BucketKey, FileName, FileSize, UserId, RecStatus)
                VALUES (?) 
            `,
                [
                    [
                        "WALQLY", bucket, key, fileName, size, "wade.li", "C"
                    ]
                ]
            );

        logger.debug(`[listingQuality - exportListingQuality] ${key} job done!`);

    } else {

        await MySql
            .query(`
                INSERT INTO LMSDb.Attachment (AttachmentTypeId, BucketFolder, BucketKey, FileName, FileSize, UserId, RecStatus, Remark)
            `,
                [
                    [
                        "WALQLY", bucket, key, fileName, null, "wade.li", "E", data.message
                    ]
                ]
            );

        logger.info(`[listingQuality - exportListingQuality] ${key} error occur : ${data.message}!`);

    }

}
// exportListingQuality()
module.exports = exportListingQuality;
