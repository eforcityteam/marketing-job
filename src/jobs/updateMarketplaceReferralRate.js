"use strict";
const logger = require("../utils/logger");
const getDatabase = require("../utils/getDatabase");

async function update() {

	const knex = await getDatabase();

	/**
	 * 
	 * Update market place referral fee from settlement report
	 * 
	 */
	await knex.raw(`
		INSERT INTO LMSDb.MarketPlaceReferralFee(MarketplaceId, CategoryId, Rates)
		SELECT 
			"TARG", 
			CategoryId, 
			MAX(ReferralRate) * 100 as Rates
		FROM LMSDb.RawSettlementTarget settlement
		LEFT JOIN LMSDb.Listing listing
		ON listing.MarketPlatformId = "TARGET-96" AND listing.ReferenceNum = settlement.SellerSKU
		LEFT JOIN LMSDb.ListingDetail detail
		ON detail.ListingId = listing.ListingId
		LEFT JOIN ProductDb.ProductMain product
		ON product.ProductId = detail.ProductId
		WHERE CategoryId IS NOT NULL
		GROUP BY CategoryId
		UNION ALL
		SELECT 
			"WALMA", 
			CategoryId, 
			MAX(CommissionRate) as Rates
		FROM LMSDb.RawSettlementWalmart settlement
		LEFT JOIN LMSDb.Listing listing
		ON settlement.MarketPlatformId = listing.MarketPlatformId AND settlement.ProductId = listing.ReferenceNum
		LEFT JOIN LMSDb.ListingDetail detail
		ON detail.ListingId = listing.ListingId
		LEFT JOIN ProductDb.ProductMain product
		ON product.ProductId = detail.ProductId
		WHERE CategoryId IS NOT NULL AND TransactionDate >= NOW() - INTERVAL 1 MONTH
		GROUP BY CategoryId
		ON DUPLICATE KEY UPDATE Rates = VALUES(Rates);
	`);

	/**
	 * 
	 * Update MarketPlatform Referral Fee table
	 * 
	 */
	await knex.raw(`
		INSERT INTO LMSDb.MarketplaceListingReferralFee (
			MarketPlatformId,
			MarketplaceListingId,
			ReferralRate,
			ReferralFee
		)
		SELECT * FROM (
			SELECT 
				settlement.MarketPlatformId, 
				MarketplaceListingId, 
				CommissionRate,
				Commission
			FROM LMSDb.RawSettlementWalmart settlement
			RIGHT JOIN LMSDb.Listing listing
			ON settlement.MarketPlatformId = listing.MarketPlatformId AND settlement.ProductId = listing.ReferenceNum
			LEFT JOIN LMSDb.ListingDetail detail
			ON detail.ListingId = listing.ListingId
			WHERE 
				TransactionDate >= NOW() - INTERVAL 1 MONTH 
					AND listing.MarketPlatformId IN ("WALMART-FS", "WALMART-26") 
					AND Commission > 0
					AND ShippedQuantity = 1
			GROUP BY settlement.MarketPlatformId, MarketplaceListingId
			ORDER BY TransactionDate DESC
		) walmart 
		
		UNION ALL
		
		SELECT * FROM (
			SELECT 
				"TARGET-96", 
				TCIN, 
				ReferralRate * 100, 
				TRUNCATE(ReferralFee/Quantity, 2) as ReferralFee
			FROM LMSDb.RawSettlementTarget target
			WHERE ReferralFee > 0 AND target.OriginalOrderDate >= "2021-01-01"
			GROUP BY TCIN
			ORDER BY OriginalOrderDate DESC
		) target
		
		ON DUPLICATE KEY UPDATE ReferralRate = VALUES(ReferralRate), ReferralFee = VALUES(ReferralFee);
	`);

	logger.debug("Program Completed", { label: "updateMarketplaceReferralRate" });
	return {};
}

module.exports = update;
