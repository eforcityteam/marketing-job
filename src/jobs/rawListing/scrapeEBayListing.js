"use strict";
const xml = require("xml");
const _ = require("lodash");
const map = require("p-map");
const axios = require("axios");
const delay = require("delay");
const moment = require("moment");
const retry = require("p-retry");
const MySql = require("mysql-plugin");

const initialize = require("../../config");
const logger = require("../../utils/logger");
const parse = require("../../utils/parseXml");
const getStartEndArray = require("../../utils/getStartEndArray");

async function scrape() {

	await initialize();

	const accounts = await getAccounts();

	await map(
		accounts,
		async account => {

			await getByItemID(account);

			await map(
				getStartEndArray(
					moment().subtract(1, "days").toDate(),
					moment().toDate(),
					60,
					119
				),
				dates => getByStartDates(account, dates),
				{
					concurrency: 1
				}
			);

			await map(
				getStartEndArray(
					moment().subtract(1, "days").toDate(),
					moment().add(1, "days").toDate(),
					60,
					119
				),
				dates => getByEndDates(account, dates),
				{
					concurrency: 1
				}
			);

			await getByActiveSKUs(account);

			logger.debug(`[scrapeEBayListing] ${account.name} Finished`);
		},
		{
			stopOnError: false
			// concurrency: 1
		}
	);

	await MySql.query(`
		UPDATE 
			LMSDb.RawListingEBay eBay,
			ProductDb.ProductCodeMap map
		SET eBay.ProductId = map.ProductId
		WHERE 
			map.ProductCode = SUBSTRING_INDEX(SKU, '_', 1)
				AND 
			ProductCodeClassId = "EFC" 
				AND 
			ProductCodeType = "AC" 
				AND 
			eBay.ProductId IS NULL;
	`);

	logger.debug("[scrapeEBayListing] Finished");
}

async function getByItemID(account) {

	const listings = await MySql
		.query(`
			SELECT SKU, itemId, startTime, endTime
			FROM LMSDb.RawListingEBay 
			WHERE NOW() > EndTime AND ListingStatus = "Active" AND MarketPlatformId = ?
		`,
			[account.platform]
		);

	await map(
		listings,
		async listing => {

			const body = xml({
				GetItemRequest: [
					{ _attr: { xmlns: "urn:ebay:apis:eBLBaseComponents" } },
					{ RequesterCredentials: [{ eBayAuthToken: account.meta.auth_token }] },
					{ ItemID: listing.itemId },
					{ DetailLevel: "ReturnAll" }
				]
			}, { declaration: true, indent: true });

			const response = await retry(
				() => axios
					.post(
						"https://proxy-ebay-api.eforcity.com/ws/api.dll",
						body,
						{
							headers: {
								"X-EBAY-API-CALL-NAME": "GetItem",
								"X-EBAY-API-APP-NAME": account.meta.app_id,
								"X-EBAY-API-SITEID": account.meta.site_id,
								"X-EBAY-API-COMPATIBILITY-LEVEL": 1091,
								"Content-Type": "text/xml"
							}
						}
					)
					.then(response => parse(response.data))
			);

			console.log(listing.SKU);

			if (response.GetItemResponse.Ack != "Failure") {
				const object = parseItem(response.GetItemResponse?.Item);
				object.MarketPlatformId = account.platform;
				await MySql.query(
					"REPLACE INTO LMSDb.RawListingEBay SET ?",
					object
				);
			} else if (response.GetItemResponse.Errors.ErrorCode == "17") {
				await MySql.query(
					"UPDATE LMSDb.RawListingEBay SET ListingStatus = ? WHERE ItemID = ?",
					[
						"Completed",
						listing.itemId
					]
				);
			}

			await delay(1000);

		},
		{
			concurrency: 1
		}
	);

}

async function getByStartDates(account, { start, end }) {

	let page = 1;

	while (true) {

		logger.debug(`[scrapeEBayListing][${account.platform}][${moment(start).format()}] Running on page #${page}`);

		const body = xml({
			GetSellerListRequest: [
				{ _attr: { xmlns: "urn:ebay:apis:eBLBaseComponents" } },
				{ RequesterCredentials: [{ eBayAuthToken: account.meta.auth_token }] },
				{ Sort: 2 },
				{ StartTimeFrom: moment(start).toISOString() },
				{ StartTimeTo: moment(end).toISOString() },
				{ DetailLevel: "ReturnAll" },
				{
					Pagination: [
						{ EntriesPerPage: 200 },
						{ PageNumber: page++ }
					]
				}
			]
		}, { declaration: true, indent: true });

		if (await request(account, body))
			break;
	}
}

async function getByActiveSKUs(account) {

	const paris = await MySql
		.query(`
			SELECT SKU, StartTime as date
			FROM RawListingEBay 
			WHERE ListingStatus = "ACTIVE" AND MarketPlatformId = ? AND SKU < "Z"
		`, [
			account.platform
		]);

	await map(
		paris,
		async (listing, index) => {

			logger.debug(`[scrapeEBayListing][${account.platform}] Updating chunk #${index + 1} of ${paris.length} chunks`);

			const body = xml({
				GetSellerListRequest: [
					{ _attr: { xmlns: "urn:ebay:apis:eBLBaseComponents" } },
					{ RequesterCredentials: [{ eBayAuthToken: account.meta.auth_token }] },
					{ SKUArray: [{ SKU: listing.SKU }] },
					{ DetailLevel: "ReturnAll" },
					{ StartTimeFrom: moment(listing.date).subtract(2, "days").toISOString() },
					{ StartTimeTo: moment(listing.date).add(2, "days").toISOString() },
					{
						Pagination: [
							{ EntriesPerPage: 200 },
							{ PageNumber: 1 }
						]
					}
				]
			}, { declaration: true, indent: true });

			await request(account, body);
			await delay(500);

		},
		{
			concurrency: 1
		}
	);

}

async function getByEndDates(account, { start, end }) {

	let page = 1;

	while (true) {

		logger.debug(`[scrapeEBayListing][${account.platform}][${moment(start).format()}] Running on page #${page}`);

		const body = xml({
			GetSellerListRequest: [
				{ _attr: { xmlns: "urn:ebay:apis:eBLBaseComponents" } },
				{ RequesterCredentials: [{ eBayAuthToken: account.meta.auth_token }] },
				{ Sort: 2 },
				{ EndTimeFrom: moment(start).toISOString() },
				{ EndTimeTo: moment(end).toISOString() },
				{ DetailLevel: "ReturnAll" },
				{
					Pagination: [
						{ EntriesPerPage: 200 },
						{ PageNumber: page++ }
					]
				}
			]
		}, { declaration: true, indent: true });

		if (await request(account, body))
			break;
	}
}


function parseItem(item) {
	return {
		ItemID: item.ItemID,
		CategoryID: item?.PrimaryCategory?.CategoryID,
		CategoryName: item?.PrimaryCategory?.CategoryName,
		Quantity: item.Quantity,
		CurrentPrice: item.SellingStatus?.CurrentPrice,
		ListingStatus: item.SellingStatus?.ListingStatus,
		Title: item.Title,
		SKU: item.SKU,
		ProductListingId: item.SKU.split("_")[0],
		StartTime: item.ListingDetails?.StartTime,
		EndTime: item.ListingDetails?.EndTime,
		EndingReason: item.ListingDetails?.EndingReason,
		HitCount: item.HitCount
	};
}

async function request(account, body) {
	const response = await retry(
		() => axios
			.post(
				"https://proxy-ebay-api.eforcity.com/ws/api.dll",
				body,
				{
					headers: {
						"X-EBAY-API-CALL-NAME": "GetSellerList",
						"X-EBAY-API-APP-NAME": account.meta.app_id,
						"X-EBAY-API-SITEID": account.meta.site_id,
						"X-EBAY-API-COMPATIBILITY-LEVEL": 1091,
						"Content-Type": "text/xml"
					}
				}
			)
			.then(response => parse(response.data))
	);

	if (response?.GetSellerListResponse?.Errors?.ErrorCode == "340")
		return true;

	let items = response?.GetSellerListResponse?.ItemArray?.Item;

	if (items && !Array.isArray(items))
		items = [items];

	if (items && items.length > 0) {
		items = items.map(parseItem);
		await retry(
			() => MySql.query(
				"REPLACE INTO LMSDb.RawListingEBay(MarketPlatformId, ??) VALUES ?",
				[
					Object.keys(items[0]),
					items.map(item => [account.platform, ...Object.values(item)])
				]
			),
			{
				retries: 5
			}
		);
	}

	return false;
}

function getAccounts() {
	return MySql
		.query(`
			SELECT 
				CredentialId as id, 
				FullName as name,
				Remark as platform
			FROM AuthDb.ProgramCredential program
			LEFT JOIN ErpDb.MarketPlatform platform
			ON platform.MarketPlatformId = program.Remark
			WHERE ProgramAbbr = "ebay" AND program.RecStatus = "A" AND platform.MonitorStatus = "A";
			SELECT 
				CredentialId as id, 
				MetaKey as \`key\`,
				MetaValue as \`value\`
			FROM AuthDb.ProgramCredentialMeta 
			WHERE CredentialId IN (
				SELECT CredentialId as id
				FROM AuthDb.ProgramCredential 
				WHERE ProgramAbbr = "ebay" AND RecStatus = "A"
			);
		`)
		.then(response => {
			const [accounts, meta] = response;
			return accounts
				.map(account => {
					account.meta = meta
						.filter(item => item.id == account.id)
						.reduce((prev, curr) => {
							prev[curr.key] = curr.value;
							return prev;
						}, {});
					return account;
				});
		});
}

module.exports = scrape;
module.exports.request = request;
module.exports.getAccounts = getAccounts;
module.exports.getByActiveSKUs = getByActiveSKUs;
