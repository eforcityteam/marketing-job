"use strict";
const _ = require("lodash");
const axios = require("axios");
const moment = require("moment");
const MySql = require("mysql-plugin");
const cleanStack = require("clean-stack");
const initialize = require("../../config");
const sign = require("../../utils/signMWS");
const logger = require("../../utils/logger");
const parse = require("../../utils/parseAmazonListingReport");
const Account = require("../../account/account.model.js");

async function request() {

	await initialize();

	// "INSTEN_CA", "INSTEN_MX",
	// for (const id of ["EFC_CA", "INSTEN_AU", "INSTEN_US"]) {

	// 	const account = await Account.get(id);

	// 	const reports = await MySql
	// 		.query(`
	// 			SELECT GeneratedReportId as id
	// 			FROM LMSDb.ProcQueueAmazonReport 
	// 			WHERE MarketPlatformId = ?
	// 				AND GeneratedReportId IS NOT NULL 
	// 				AND ReportType = "_GET_MERCHANT_LISTINGS_ALL_DATA_"
	// 				AND RecStatus = "U"
	// 			`,
	// 			account.marketPlatform
	// 		)
	// 		.then(rows => rows.map(item => item.id));

	// 	for (const id of reports) {

	// 		const params = {
	// 			Version: "2009-01-01",
	// 			Action: "GetReport",
	// 			ReportId: id
	// 		};

	// 		const _params = {
	// 			SellerId: account.credential.seller_id,
	// 			SignatureMethod: "HmacSHA256",
	// 			SignatureVersion: "2",
	// 			AWSAccessKeyId: account.credential.access_key,
	// 			["MarketplaceId.Id.1"]: account.credential.market_place_id,
	// 			Timestamp: moment().toISOString()
	// 		};

	// 		_.assign(params, _params);
	// 		_.assign(params, {
	// 			Signature: sign(
	// 				"Reports",
	// 				"POST",
	// 				params,
	// 				account.credential.url,
	// 				account.credential.secret_access_key
	// 			)
	// 		});

	// 		try {
	// 			const json = await axios
	// 				.post(
	// 					"https://" + account.credential.url + "/Reports/2009-01-01",
	// 					{},
	// 					{ params }
	// 				)
	// 				.then(response => response.data)
	// 				.then(parse);

	// 			await MySql.query(
	// 				"REPLACE INTO LMSDb.RawListingAmazon (MarketPlatformId, ??) VALUES ?", [
	// 				Object.keys(json[0]),
	// 				json.map(item => [
	// 					account.marketPlatform,
	// 					...Object.values(item)
	// 				])
	// 			]);

	// 			await MySql.query(
	// 				"UPDATE LMSDb.ProcQueueAmazonReport SET ? WHERE GeneratedReportId = ?", [
	// 				{ RecStatus: "C" }, id
	// 			]);

	// 		} catch (error) {
	// 			error.stack = cleanStack(error.stack);
	// 			console.error(error.message);
	// 			await MySql.query(
	// 				"UPDATE LMSDb.ProcQueueAmazonReport SET ? WHERE GeneratedReportId = ?", [
	// 				{
	// 					RecStatus: "D",
	// 					Remark: error?.message
	// 				},
	// 				id
	// 			]);
	// 		}
	// 	}
	// }
	// logger.debug("[extractAWSListingReport] Program completed");
}

module.exports = request;
