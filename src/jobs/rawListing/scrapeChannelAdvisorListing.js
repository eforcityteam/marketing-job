"use strict";
const _ = require("lodash");
const map = require("p-map");
const axios = require("axios");
const retry = require("p-retry");
const moment = require("moment");
const urlizer = require("urlizer");
const MySql = require("mysql-plugin");
const initialize = require("../../config");
const logger = require("../../utils/logger");

const Account = require("../../account/account.model.js");

async function scrape() {

	await initialize();

	const infos = await MySql
		.query(`
			-- LastUpdate
			SELECT MAX(UpdateDateUtc) as lastUpdate
			FROM LMSDb.RawListingChannelAdvisor;

			-- Quantity
			SELECT MAX(QuantityUpdateDateUtc) as quantityUpdate 
			FROM LMSDb.RawListingChannelAdvisor;

			-- Max ID, for create listing
			SELECT MAX(ID) as id 
			FROM LMSDb.RawListingChannelAdvisor;
		`)
		.then(rows => _.defaults.apply(_, rows.map(row => row[0])));

	let responses = [];

	logger.debug("[Scrape Listing] Scraping by last update");

	/**
	 * 
	 * Checking on LastUpdate
	 * 
	 */
	responses.push(await requestUrl(
		"https://api.channeladvisor.com/v1/Products",
		{
			$select: "ID,ProfileID,IsAvailableInStore,Sku,CreateDateUtc,UpdateDateUtc,QuantityUpdateDateUtc",
			$expand: "Labels",
			$count: true,
			$filter: "UpdateDateUtc ge " + moment(infos.lastUpdate).subtract(3, "hours").format()
		}
	));

	logger.debug("[Scrape Listing] Scraping by last quantity update");

	/**
	 * 
	 * Checking on QuantityUpdate
	 * 
	 */
	responses.push(await requestUrl(
		"https://api.channeladvisor.com/v1/Products",
		{
			$select: "ID,ProfileID,IsAvailableInStore,Sku,CreateDateUtc,UpdateDateUtc,QuantityUpdateDateUtc",
			$expand: "Labels",
			$count: true,
			$filter: "QuantityUpdateDateUtc ge " + moment(infos.quantityUpdate).subtract(3, "hours").format()
		}
	));

	logger.debug("[Scrape Listing] Scraping by ID update");

	/**
	 * 
	 * ID
	 * 
	 */
	responses.push(await requestUrl(
		"https://api.channeladvisor.com/v1/Products",
		{
			$select: "ID,ProfileID,IsAvailableInStore,Sku,CreateDateUtc,UpdateDateUtc,QuantityUpdateDateUtc",
			$expand: "Labels",
			$count: true,
			$filter: "ID ge " + infos.id
		}
	));

	await map(
		_.chunk(responses.flat(), 10000),
		async responses => {
			const labels = responses.map(item => item.Labels).flat();
			await MySql.query(`
			
				DELETE FROM LMSDb.RawListingLabelChannelAdvisor 
				WHERE ID IN (?);

				REPLACE INTO LMSDb.RawListingChannelAdvisor (
					ID, ProfileID, 
					CreateDateUtc, UpdateDateUtc, QuantityUpdateDateUtc, 
					IsAvailableInStore, SKU, ProductListingId
				)
				VALUES ?;
				
				REPLACE INTO LMSDb.RawListingLabelChannelAdvisor (ID, ProfileID, Label)
				VALUES ?;
			`, [
				responses.map(item => item.ID),
				responses.map(item => {
					return [
						item.ID, item.ProfileID,
						item.CreateDateUtc, item.UpdateDateUtc, item.QuantityUpdateDateUtc,
						item.IsAvailableInStore, item.Sku, item.Sku.split("_")[0]
					];
				}),
				labels.map(Object.values),

			]);
		},
		{ concurrency: 1 }
	);

	await MySql.query(`
		UPDATE LMSDb.RawListingChannelAdvisor 
		SET ProductListingId = SUBSTRING_INDEX(SKU, '_', 1) 
		WHERE ProductListingId IS NULL;

		UPDATE
			LMSDb.RawListingChannelAdvisor,
			(
				SELECT * FROM (
					SELECT ID, map.ProductId
					FROM LMSDb.RawListingChannelAdvisor channelAdvisor
					INNER JOIN ProductDb.ProductCodeMap map
					ON map.ProductCode = channelAdvisor.ProductListingId AND map.ProductCodeType = "AC" AND ProductCodeClassId = "EFC"
					WHERE channelAdvisor.ProductId IS NULL
				) temp
			) temp
		SET RawListingChannelAdvisor.ProductId = temp.ProductId 
		WHERE RawListingChannelAdvisor.ID = temp.ID;
	`);

	logger.debug("[Scrape Listing] Inserted");

	return {};
}

async function requestUrl(url, params) {

	const account = await Account.get("channel_advisor");

	/**
 	* 
	* Main Request
	* 
	*/
	params["access_token"] = await account.getAccessToken();

	const responses = [];

	let response = await retry(
		() => axios
			.get(url, { params })
			.then(response => response.data),
		{ retries: 2 }
	);

	const count = response["@odata.count"];

	responses.push(...response.value);

	let nexUrl = response["@odata.nextLink"];

	/**
	 * 
	 * Replace unnecessary and access token queries
	 * 
	 */
	if (nexUrl) {
		urlizer.removeQuery(nexUrl, "$count");
		urlizer.replaceQuery("access_token", await account.getAccessToken());
	}

	/**
	 * 
	 * Get the remaining urls
	 * 
	 */
	while (nexUrl) {
		response = await retry(
			() => axios
				.get(nexUrl)
				.then(response => response.data),
			{ retries: 2 }
		);
		nexUrl = response["@odata.nextLink"];
		responses.push(...response.value);
		logger.debug(`[Scrape Listing] Running ${(responses.length / count * 100).toFixed(2)}%`);
	}

	return responses;
}

// async function getToken(renewToken, appId, sharedSecret) {

// 	const response = await retry(() => axios.post(
// 		"https://api.channeladvisor.com/oauth2/token",
// 		qs.stringify({ grant_type: "refresh_token", refresh_token: renewToken }),
// 		{
// 			headers: {
// 				Authorization: "Basic " + base64.encode([appId, sharedSecret].join(":")),
// 				"Content-Type": "application/x-www-form-urlencoded"
// 			}
// 		}
// 	));
// 	return response.data.access_token;
// }

// getToken = mem(getToken, { maxAge: 1000 * 60 * 1 });

module.exports = scrape;
