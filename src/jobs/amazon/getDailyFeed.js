"use strict";
const MySql = require("mysql-plugin");
const Account = require("../../account/account.model.js");
const logger = require("../../utils/logger");

async function getDailyFeed() {

	const REPORTS = [
		"_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_"
	];

	for (const REPORT of REPORTS) {

		for (const id of ["INSTEN_US", "INSTEN_CA", "INSTEN_MX", "INSTEN_AU"]) {

			const account = await Account.get(id);
			const response = await account.getReportList(REPORT);

			if (response.ReportInfo.length > 0)
				await MySql.query(`
					INSERT INTO LMSDb.ProcQueueAmazonReport (
						MarketPlatformId, 
						ReportType, 
						ReportRequestId, GeneratedReportId, 
						ProcessingStatus, 
						AvailableDate
					) VALUES ?
					ON DUPLICATE KEY UPDATE GeneratedReportId = VALUES(GeneratedReportId)
				`, [
					response.ReportInfo.map(item => [
						account.meta?.fba_marketplatformId,
						item.ReportType,
						item.ReportRequestId,
						item.ReportId,
						"__DONE__",
						item.AvailableDate
					])
				]);
		}
	}

	logger.debug({ message: "[Amazon - getDailyFeed] Program completed" });
	return {};
}

module.exports = getDailyFeed;
