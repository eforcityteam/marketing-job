"use strict";
const md5 = require("md5");
const map = require("p-map");
const MySql = require("mysql-plugin");
const csvtojson = require("csvtojson");
const logger = require("../../utils/logger");
const Account = require("../../account/account.model.js");

async function parse() {

	const rows = await MySql.query(`
		SELECT 
			GeneratedReportId as id,
			meta.AccountId as accountId
		FROM LMSDb.ProcQueueAmazonReport report
		LEFT JOIN OrderIMDb.AccountMeta meta
		ON report.MarketPlatformId = meta.MetaValue AND MetaKey = "fba_marketplatformId"
		WHERE 
			ReportType = "_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_" 
			AND ProcessingStatus = "__DONE__" 
			AND RecStatus IN ("U", "E");
	`);

	await map(
		rows,
		async row => {

			logger.debug("[parseFBAReport] parsing report #" + row.id);
			const account = await Account.get(row.accountId);
			try {
				const rows = await account
					.getReport(row.id)
					.then(csv => csvtojson({ delimiter: "\t" }).fromString(csv))
					.then(rows => rows.map(item => ({
						reportId: row.id,
						hash: md5(JSON.stringify(item)),
						...item
					})));
				if (rows.length > 0)
					await MySql
						.query(`
							INSERT INTO LMSDb.RawListingAmazonFBAReturn (
								ReportId, Hash, ReturnDate, 
								OrderId, 
								SKU, ASIN, FNSKU, 
								ProductName, 
								Quantity, 
								FulfillmentCenterId,
								DetailedDisposition, 
								Reason, Status, 
								LicensePlateNumber, CustomerComments
							)
							VALUES ?
							ON DUPLICATE KEY UPDATE Hash = VALUES(Hash)
						`, [
							rows.map(row => Object.values(row))
						]);
				await MySql.query(
					"UPDATE LMSDb.ProcQueueAmazonReport SET RecStatus = \"C\" WHERE GeneratedReportId = ?",
					[row.id]
				);
			} catch (error) {
				console.error(error.message);
				await MySql.query(
					"UPDATE LMSDb.ProcQueueAmazonReport SET RecStatus = \"E\" WHERE GeneratedReportId = ?",
					[row.id]
				);
				return {};
			}
			return {};
		},
		{
			concurrency: 1
		}
	);

	logger.debug("[parseFBAReport] Program completed");
	return {};
}

module.exports = parse;
