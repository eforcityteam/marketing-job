"use strict";
const _ = require("lodash");
const axios = require("axios");
const moment = require("moment");
const MySql = require("mysql-plugin");
const cleanStack = require("clean-stack");
const initialize = require("../../config");
const sign = require("../../utils/signMWS");
const logger = require("../../utils/logger");
const parseXml = require("../../utils/parseXml");
const Account = require("../../account/account.model.js");

async function request() {

	await initialize();

	for (const id of ["EFC_CA", "INSTEN_AU", "INSTEN_US", "INSTEN_CA", "INSTEN_MX"]) {

		const account = await Account.get(id);

		const reports = await MySql
			.query(`
				SELECT ReportRequestId as id
				FROM LMSDb.ProcQueueAmazonReport 
				WHERE MarketPlatformId = ? 
					AND GeneratedReportId IS NULL 
					AND RecStatus = "U"
				`,
				account.marketPlatform
			)
			.then(rows => rows.map(item => item.id));

		for (const id of reports) {

			const params = {
				Version: "2009-01-01",
				Acknowledged: false,
				Action: "GetReportRequestList",
				"ReportRequestIdList.Id.1": id
			};

			const _params = {
				SellerId: account.credential.seller_id,
				SignatureMethod: "HmacSHA256",
				SignatureVersion: "2",
				AWSAccessKeyId: account.credential.access_key,
				["MarketplaceId.Id.1"]: account.credential.market_place_id,
				Timestamp: moment().toISOString()
			};

			_.assign(params, _params);
			_.assign(params, {
				Signature: sign(
					"Reports",
					"POST",
					params,
					account.credential.url,
					account.credential.secret_access_key
				)
			});

			try {
				const response = await axios
					.post(
						"https://" + account.credential.url + "/Reports/2009-01-01",
						{},
						{ params }
					)
					.then(response => response.data)
					.then(parseXml);

				await MySql.query(
					"UPDATE LMSDb.ProcQueueAmazonReport SET ? WHERE ReportRequestId = ?",
					[
						{
							GeneratedReportId: response?.GetReportRequestListResponse?.GetReportRequestListResult?.ReportRequestInfo?.GeneratedReportId,
							ProcessingStatus: response?.GetReportRequestListResponse?.GetReportRequestListResult?.ReportRequestInfo?.ReportProcessingStatus
						},
						id
					]
				);
			} catch (error) {
				error.stack = cleanStack(error.stack);
				console.error(error);
				await MySql.query(
					"UPDATE LMSDb.ProcQueueAmazonReport SET ? WHERE ReportRequestId = ?",
					[
						{
							RecStatus: "D",
							Remark: error?.message
						},
						id
					]
				);
			}
		}
	}
	logger.debug("[requestAWSReportId] Program completed");
}

module.exports = request;
