"use strict";
const _ = require("lodash");
const axios = require("axios");
const moment = require("moment");
const MySql = require("mysql-plugin");
const cleanStack = require("clean-stack");

const initialize = require("../../config");
const sign = require("../../utils/signMWS");
const logger = require("../../utils/logger");
const parseXml = require("../../utils/parseXml");
const Account = require("../../account/account.model.js");

const REPORTS = [
	// "_GET_MERCHANT_LISTINGS_ALL_DATA_",
	"_GET_REFERRAL_FEE_PREVIEW_REPORT_",
	"_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_"
];

async function request() {

	await initialize();

	for (const id of ["EFC_CA", "INSTEN_AU", "INSTEN_US", "INSTEN_CA", "INSTEN_MX"]) {

		const account = await Account.get(id);

		for (const report of REPORTS) {

			const params = {
				Version: "2009-01-01",
				Action: "RequestReport",
				ReportType: report,
			};

			const _params = {
				SellerId: account.credential.seller_id,
				SignatureMethod: "HmacSHA256",
				SignatureVersion: "2",
				AWSAccessKeyId: account.credential.access_key,
				["MarketplaceId.Id.1"]: account.credential.market_place_id,
				Timestamp: moment().toISOString()
			};

			_.assign(params, _params);
			_.assign(params, {
				Signature: sign(
					"Reports",
					"POST",
					params,
					account.credential.url,
					account.credential.secret_access_key
				)
			});

			try {
				const response = await axios
					.post(
						"https://" + account.credential.url + "/Reports/2009-01-01",
						{},
						{ params }
					)
					.then(response => response.data)
					.then(parseXml);

				await MySql.query(
					"INSERT INTO LMSDb.ProcQueueAmazonReport SET ?", {
					MarketPlatformId: account.marketPlatform,
					ReportType: report,
					ReportRequestId: response?.RequestReportResponse?.RequestReportResult?.ReportRequestInfo?.ReportRequestId,
					ProcessingStatus: response?.RequestReportResponse?.RequestReportResult?.ReportRequestInfo?.ReportProcessingStatus
				});

			} catch (error) {
				error.stack = cleanStack(error.stack);
				await MySql.query(
					"INSERT INTO LMSDb.ProcQueueAmazonReport SET ?", {
					MarketPlatformId: account.marketPlatform,
					ReportType: report,
					RecStatus: "D",
					Remark: error?.message
				});
			}
		}
	}

	await MySql.query(`
		UPDATE LMSDb.ProcQueueAmazonReport 
		SET RecStatus = "C" 
		WHERE 
			ProcessingStatus = "_DONE_NO_DATA_" 
				AND 
			RecStatus = "U";
	`);

	logger.debug("[requestAWSReport] Program completed");
}

module.exports = request;
