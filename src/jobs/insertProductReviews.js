"use strict";
const MySql = require("mysql-plugin");
const logger = require("../utils/logger");

async function insert() {

	logger.debug("[insertProductReviews] Inserting Product Reviews");

	await MySql.query(`
		INSERT INTO ProductReviewLine(ProductId, ListingReviewId, ReviewDate)
		SELECT ProductId, ListingReviewId, CommentDate
		FROM LMSDb.ListingReviewLine line 
		LEFT JOIN LMSDb.ListingDetail detail
		ON line.ListingId = detail.ListingId
		WHERE 
			ReviewRating <= 3
				AND 
			ProductId IS NOT NULL 
				AND 
			ProductId > 0
				AND
			ListingReviewId NOT IN (
				SELECT _line.ListingReviewId FROM ProductReviewLine _line
			)
				AND
			line.ReviewContent <> "" 
				AND 
			line.ReviewContent IS NOT NULL
				AND
			CommentDate >= NOW() - INTERVAL 1 WEEK
				AND
			detail.listingId IS NOT NULL
		GROUP BY ListingReviewId;
	`);

	logger.debug("[insertProductReviews] Update product header");

	await MySql.query(`
		INSERT INTO ProductReviewHeader(ProductId, LastReviewDate)
		SELECT ProductId, MAX(ReviewDate)
		FROM ProductReviewLine 
		WHERE RecStatus = "Q"
		GROUP BY ProductId
		ON DUPLICATE KEY UPDATE RecStatus = "Q", LastReviewDate = VALUES(LastReviewDate);
	`);

	logger.debug("[insertProductReviews] Finished");
	return true;
}

module.exports = insert;
