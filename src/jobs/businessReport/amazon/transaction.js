"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const retry = require("p-retry");
const moment = require("moment");
const MySql = require("mysql-plugin");
const csvtojson = require("csvtojson");
const renameKeys = require("rename-keys");

const initial = require("../../../config");
const logger = require("../../../utils/logger");
const COLUMN_MAP = require("./utils/COLUMNS_MAP.json");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const COLUMNS = [
	"ReportId",
	"DateTime", "SettlementId", "Type",
	"OrderId", "SKU", "Description", "Quantity",
	"Marketplace", "AccountType", "Fulfillment",
	"OrderCity", "OrderState", "OrderPostal",
	"TaxCollectionModel",
	"ProductSales", "ProductSalesTax",
	"ShippingCredits", "ShippingCreditsTax",
	"GiftWrapCredits", "GiftWrapCreditsTax",
	"PromotionalRebates", "PromotionalRebatesTax", "MarketplaceWithheldTax",
	"SellingFees", "FBAFees", "OtherTransactionFees",
	"Other", "Total"
];

async function handle() {

	await initial();

	const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId IN (
				SELECT ReportClassId FROM DefBusinessReportClass WHERE MarketPlaceId = "AMAZ"
			) 
				AND 
			ReportTypeId = "TRANSAC" 
				AND 
			RecStatus = "Q";	
	`);

	logger.debug(`[Business Report - Amazon] There are total ${tasks.length} tasks`);

	await map(
		tasks,
		async task => {

			logger.debug(`[Business Report - Amazon] Extracting file ${task.filename}`);

			try {
				const buffer = await s3
					.getObject({
						Bucket: task.bucket,
						Key: task.key
					})
					.promise()
					.then(response => response.Body.toString("utf-8"));

				await etl(task.id, buffer);

				await MySql.query(`
					UPDATE BusinessReport
					SET RecStatus = "C"
					WHERE ReportId = ?
				`, [
					task.id
				]);

			} catch (error) {

				logger.error({
					...error,
					label: "[Business Report - Amazon]"
				});

				await MySql.query(`
					UPDATE BusinessReport
					SET RecStatus = "E", Remark = ?
					WHERE ReportId = ?
				`, [
					error.message,
					task.id
				]);
			}

		},
		{
			concurrency: 1,
			stopOnError: process.env.NODE_ENV !== "production"
		}
	);

	logger.debug("[Business Report - Amazon] Program Completed");
	return true;
}

async function etl(id, string) {

	let rows = string.split("\n");

	/**
	 * 
	 * Find the start index of the csv file
	 * 
	 */
	const startIndex = rows.findIndex(sentence => /(date\/time)|(fecha\/hora)/.test(sentence));

	/**
	 * 
	 * Remove the reminder from csv file
	 * 
	 */
	rows.splice(0, startIndex);

	/**
	 * 
	 * Convert into json object
	 * 
	 */
	rows = await csvtojson({ flatKeys: true })
		.fromString(rows.join("\n"))
		.then(rows => rows.map(object => {

			const isSpanish = !!object["fecha/hora"];

			if (isSpanish) {
				object = renameKeys(object, function (key) {
					return COLUMN_MAP[key.trim()];
				});
			}

			let datetime = moment(object["date/time"]);

			if (!datetime.isValid()) {
				datetime = moment(object["date/time"], "D MMM YYYY h:mm:ss a z", "es", false);
				if (!datetime.isValid()){
					datetime = moment(object["date/time"], "D MMM, YYYY h:mm:ss a z", "us", false);
				}
			}

			if (!datetime.isValid()){
				console.log(object["date/time"]);
			}

			object["date/time"] = datetime.toDate();

			return [
				id,
				object["date/time"], object["settlement id"], object["type"],
				object["order id"], object["sku"], object["description"], object["quantity"],
				object["marketplace"], object["account type"], object["fulfillment"],
				object["order city"], object["order state"], object["order postal"],
				object["tax collection model"],
				object["product sales"], object["product sales tax"],
				object["shipping credits"], object["shipping credits tax"],
				object["gift wrap credits"], object["giftwrap credits tax"],
				object["promotional rebates"], object["promotional rebates tax"], object["marketplace withheld tax"],
				object["selling fees"], object["fba fees"], object["other transaction fees"],
				object["other"], object["total"]
			];
		}));

	await map(
		_.chunk(rows, 10000),
		async rows => {
			await retry(
				async () => {
					await MySql.query(
						"INSERT INTO LMSDb.RawSettlementAmazon (??) VALUES ?",
						[COLUMNS, rows]
					);
				},
				{
					retries: 3
				}
			);
		},
		{
			concurrency: 1
		}
	);

	logger.debug("[Business Report - Amazon] Inserting data into LMSDb.RawPayoutTarget");
}

module.exports = handle;
module.exports.etl = etl;
