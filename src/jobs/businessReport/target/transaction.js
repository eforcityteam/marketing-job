"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const retry = require("p-retry");
const MySql = require("mysql-plugin");
const exceltojson = require("convert-excel-to-json");
const logger = require("../../../utils/logger");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const TRANSFER = [
	"ReportId",
	"TransferId", "Type",
	"PayoutId", "PaymentId",
	"TCIN", "SellerSKU",
	"OrderNum",
	"OriginalOrderDate", "ReturnDate",
	"UnitPrice", "Quantity",
	"LineTotal", "ReferralFee", "Shipping", "Tax",
	"ReferralRate", "PaymentAmount",
	"OrderId"
];

const PAYOUT = [
	"ReportId",
	"PayoutId",
	"Amount", "Currency",
	"Date", "Destination"
];

async function handle() {

	const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId = "TARGET" 
				AND 
			ReportTypeId = "TRANSAC" 
				AND 
			RecStatus = "Q"
	`);

	logger.debug(`[Business Report - Target] There are total ${tasks.length} tasks`);

	await map(
		tasks,
		async task => {

			logger.debug(`[Business Report - Target] Extracting file ${task.filename}`);

			try {
				const buffer = await s3
					.getObject({
						Bucket: task.bucket,
						Key: task.key
					})
					.promise()
					.then(response => response.Body);

				await etl(task.id, buffer);

				await MySql.query(`
					UPDATE BusinessReport
					SET RecStatus = "C"
					WHERE ReportId = ?
				`, [
					task.id
				]);

			} catch (error) {

				logger.error({
					...error,
					label: "[Business Report - Target]"
				});

				await MySql.query(`
					UPDATE BusinessReport
					SET RecStatus = "E", Remark = ?
					WHERE ReportId = ?
				`, [
					error.message,
					task.id
				]);
			}

		},
		{
			concurrency: 1,
			stopOnError: process.env.NODE_ENV !== "production"
		}
	);

	logger.debug("[Business Report - Target] Program Completed");
	return true;
}

async function etl(id, buffer) {

	logger.debug("[Business Report - Target] Extracting data from the buffer");

	const {
		Transfers: transfers,
		Payouts: payouts
	} = exceltojson({
		source: buffer,
		columnToKey: {
			"*": "{{columnHeader}}"
		},
		sheets: ["Transfers", "Payouts"],
		header: {
			rows: 1
		}
	});

	logger.debug("[Business Report - Target] Inserting data into LMSDb.RawSettlementTarget");

	await map(
		_.chunk(transfers, 5000),
		async transfers => {
			await retry(
				() => MySql.query(`
					INSERT INTO RawSettlementTarget (??)
					VALUES ?
					ON DUPLICATE KEY UPDATE ${TRANSFER.map(column => `${column} = VALUES(${column})`).join(",")}
				`, [
					TRANSFER,
					transfers.map(transfer => [
						id,
						transfer["Transfer ID"], transfer["Type"],
						transfer["Payout ID"], transfer["Payment ID"],
						transfer["TCIN"], transfer["Seller SKU"],
						transfer["Order Number"],
						transfer["Original Order Date"], transfer["Return Date"],
						transfer["Unit Price"], transfer["Quantity"],
						transfer["Line total less discounts"], transfer["Referral Fee"], transfer["Shipping and Services"], transfer["Tax Amount"],
						transfer["Referral %"], transfer["Payment Amount"],
						transfer["Order ID"],
					])
				]),
				{
					retries: 5
				}
			);
		},
		{
			concurrency: 1
		}
	);

	logger.debug("[Business Report - Target] Inserting data into LMSDb.RawPayoutTarget");

	await retry(
		() => MySql.query(`
			INSERT INTO RawPayoutTarget (??) 
			VALUES ?
			ON DUPLICATE KEY UPDATE ${PAYOUT.map(column => `${column} = VALUES(${column})`).join(",")}
		`,
			[
				PAYOUT,
				payouts.map(payout => [
					id,
					payout["Payout ID"], payout["Amount"],
					payout["Currency"], payout["Date"],
					payout["Destination"]
				])
			]
		),
		{
			retries: 5
		}
	);
}

module.exports = handle;
