"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const csv = require("csvtojson")
const MySql = require("mysql-plugin");
const moment = require("moment-timezone");
const logger = require("../../../utils/logger");
const initial = require("../../../config");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const TRANSFER = [
	"ReportId", "Date",
	"TransactionId", "ReturnId",
	"CancelId", "CaseId",
	"DisputeId", "Title",
	"Item", "SalePrice",
	"FeeType", "Amount",
	"TotalSaleAmount", "AdRate",
	"VIN", "ReceivedTopRatedDiscount",
	"PromotionalSavings", "OrderNum",
	"IsTransactionNetted", "DiscountApplied",
	"Remark"
];

async function handle() {

	await initial();

	const tasks = await MySql.query(`
        SELECT 
            report.ReportId as id,
            report.FullName as filename,
            report.BucketFolder as bucket,
            report.BucketKey as \`key\`
        FROM BusinessReport report
        LEFT JOIN DefBusinessReportClass class
        ON class.ReportClassId = report.ReportClassId
        WHERE 
            class.MarketPlaceId = "EBAY" 
                AND 
            report.ReportTypeId = "TRANSAC" 
                AND 
            report.RecStatus = "Q"
    `);

	logger.debug(`[Business Report - eBay] There are total ${tasks.length} tasks`);

	await map(
		tasks,
		async task => {

			logger.debug(`[Business Report - eBay] Extracting file ${task.filename}`);

			try {
				const buffer = await s3
					.getObject({
						Bucket: task.bucket,
						Key: task.key
					})
					.promise()
					.then(response => response.Body.toString("utf-8").split("\n"));

				await etl(buffer, task.id);

				await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "C"
                	WHERE ReportId = ?
                `, [
					task.id
				]);

			} catch (error) {

				logger.error({
					...error,
					label: "[Business Report - eBay]"
				});

				await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "E", Remark = ?
                	WHERE ReportId = ?
                `, [
					error.message,
					task.id
				]);
			}

		},
		{
			concurrency: 1
		}
	);

	logger.debug("[Business Report - eBay] Program Completed");
	return true;

}

async function etl(buffer, id) {

	logger.debug("[Business Report - eBay] Extracting data from the buffer");

	let headers = buffer.find(strings => /Fee Type|VIN\/Serial Number|ReceivedTopRatedDiscount/ig.test(strings));
	const headersIndex = buffer.indexOf(headers);

	headers = headers.split(",");
	let rows = buffer.slice(headersIndex);

	rows = await csv().fromString(rows.join("\n"));

	rows = rows
		.filter(row => Object.keys(row).length > 6)
		.map(row => {
			const remark = row?.Title;

			row.Remark = remark;
			row.Amount = parseFloat(row.Amount?.replace(/,|\$/g, "")?.trim()) || null;
			row.ReceivedTopRatedDiscount = parseFloat(row.ReceivedTopRatedDiscount?.replace(/,|\$/g, "")?.trim()) || null;
			row["Discount Applied"] = parseFloat(row["Discount Applied"]?.replace(/,|\$/g, "")?.trim()) || null;
			row.Date = moment.tz(new Date(row.Date), "America/Los_Angeles").utc().toDate();
			row.TransactionId = /Transaction ID:\s+(?<transaction_id>\d+)/ig.exec(remark)?.groups?.transaction_id;
			row.ReturnId = /Return ID:\s+(?<return_id>\d+)/ig.exec(remark)?.groups?.return_id;
			row.CancelId = /Cancel ID:\s+(?<cancel_id>\d+)/ig.exec(remark)?.groups?.cancel_id;
			row.CaseId = /Case ID:\s+(?<case_id>\d+)/ig.exec(remark)?.groups?.case_id;
			row.DisputeId = /Dispute ID:\s+(?<dispute_id>\d+)/ig.exec(remark)?.groups?.dispute_id;
			row.Title = remark?.split(";")[0]?.replace(/"|'/ig, "");
			row.SalePrice = parseFloat(/Sale Price:.+\$+(?<sale_price>\d+\.?\d*)\.\s+/ig.exec(remark)?.groups?.sale_price?.replace(/,|\$/g, "")?.trim()) || null;
			row.TotalSaleAmount = parseFloat(/Final amount:\s+\$+(?<final_amount>\d+\.?\d*)\./ig.exec(remark)?.groups?.final_amount?.replace(/,|\$/g, "")?.trim()) || null;
			row.AdRate = parseFloat(/Ad rate:\s+(?<ad_rate>\d+\.?\d*)/ig.exec(remark)?.groups?.ad_rate?.trim()) || null;

			return row;
		});

	logger.debug("[Business Report - eBay] Inserting data into LMSDb.RawSettlementEBay");

	await map(
		_.chunk(rows, 5000),
		async (row, index) => {
			logger.debug(`[Business Report - eBay] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

			try {
				await MySql.query(
					`
                        INSERT INTO RawSettlementEBay (??)
                        VALUES ?
                    `,
					[
						TRANSFER,
						row.map(transfer => [
							id, transfer["Date"],
							transfer["TransactionId"], transfer["ReturnId"],
							transfer["CancelId"], transfer["CaseId"],
							transfer["DisputeId"], transfer["Title"],
							transfer["Item"], transfer["SalePrice"],
							transfer["Fee Type"], transfer["Amount"],
							transfer["TotalSaleAmount"], transfer["AdRate"],
							transfer["VIN/Serial Number"], transfer["ReceivedTopRatedDiscount"],
							transfer["Promotional Savings"], transfer["Order Number"],
							transfer["Is Transaction Netted"], transfer["Discount Applied"],
							transfer["Remark"]
						])
					]
				);
			} catch (error) {
				console.error(error?.message)
			}
		},
		{
			concurrency: 1
		}
	);

}

module.exports = handle;
