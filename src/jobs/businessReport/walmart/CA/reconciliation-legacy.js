"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const moment = require("moment");
const csv = require("csvtojson");
const mapObject = require("map-obj");
const MySql = require("mysql-plugin");
const logger = require("../../../../utils/logger");
const initial = require("../../../../config");

const s3 = new AWS.S3({
    region: "us-west-2",
    useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const RECONCILIATION_HEADER_MAP = {
    "Sales Order ID": "WalmartOrder",
    "Sales Order Line Number": "WalmartOrderLine",
    "Purchase Order ID": "WalmartPO",
    "Purchase Order Line Number": "WalmartPOLine",
    "Transaction Type": "TransactionType",
    "Customer Invoice Number": "CustomerInvoiceNumber",
    "Customer Invoice Date": "CustomerInvoiceDate",
    "Seller Invoice Number": "SellerInvoiceNumber",
    "Seller Invoice Date": "SellerInvoiceDate",
    "Seller Invoice Disposition Date": "SellerInvoiceDispositionDate",
    "Shipped Qty": "ShippedQty",
    "Offer ID": "OfferId",
    "Walmart SKU ID": "WalmartSKUId",
    "Seller SKU ID": "SellerSKUId",
    "Product Tax Code": "ProductTaxCode",
    "Item Name": "PartnerItemName",
    "GTIN": "PartnerGTIN",
    "Ship To City": "ShipToCity",
    "Ship to Province": "ShipToProvince",
    "Postal Code": "ZipCode",
    "Shipping Method": "ShippingMethod",
    "Line Amount": "LineAmount",
    "Line Shipping Charge": "LineShippingCharge",
    "Line Fee": "LineFee",
    "Line Tax": "LineTax",
    "GST/HST": "GST_HST",
    "PST/QST Remit by Seller": "PST_QSTRemitBySeller",
    "PST/QST Remit by Walmart": "PST_QSTRemitByWalmart",
    "Tender From Customer": "TotalTenderToFromCustomer",
    "Referral Rate": "CommissionRate",
    "Referral Amount": "CommissionFromSale",
    "Effective Tax Rate": "EffectiveTaxRate",
    "ReferralTax": "CommissionTax",
    "Payable To Partner": "PayableToPartner",
};

const TRANSFER = Object.values(RECONCILIATION_HEADER_MAP);
TRANSFER.unshift("ReportId", "Country");

async function handle() {

    await initial();

    const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId = "WALM-CA" 
				AND 
			ReportTypeId = "PAYMENT" 
				AND 
            RecStatus = "Q";
    `);

    logger.debug(`[Business Report - Walmart - Reconciliation(CA) - Legacy] There are total ${tasks.length} tasks`);

    await map(
        tasks,
        async task => {

            logger.debug(`[Business Report - Walmart - Reconciliation(CA) - Legacy] Extracting file ${task.filename}`);

            try {
                const buffer = await s3
                    .getObject({
                        Bucket: task.bucket,
                        Key: task.key
                    })
                    .promise()
                    .then(response => response.Body.toString("utf-8"));

                await etl(task.id, buffer);

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "C"
                	WHERE ReportId = ?
                `, [
                    task.id
                ]);

            } catch (error) {
                console.log(error);
                logger.error({
                    ...error,
                    label: "[Business Report - Walmart - Reconciliation(CA) - Legacy]"
                });

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "E", Remark = ?
                	WHERE ReportId = ?
                `, [
                    error.message,
                    task.id
                ]);
            }

        },
        {
            concurrency: 1,
            stopOnError: process.env.NODE_ENV !== "production"
        }
    );

    logger.debug("[Business Report - Walmart - Reconciliation(CA) - Legacy] Program Completed");
    return true;

}

async function etl(id, buffer) {

    let rows = await csv().fromString(buffer);
    rows = rows.map(row => {
        row = mapObject(row, (key, value) => [RECONCILIATION_HEADER_MAP[key], value]);
        row.CustomerInvoiceDate = moment(row.CustomerInvoiceDate, "DD-MMM-YY").format();
        row.SellerInvoiceDate = moment(row.SellerInvoiceDate, "DD-MMM-YY").format();
        row.SellerInvoiceDispositionDate = moment(row.SellerInvoiceDispositionDate, "DD-MMM-YY").format();
        return row;
    });

    logger.debug("[Business Report - Walmart - Reconciliation(CA) - Legacy] Inserting data into LMSDb.RawPaymentReportLegacyWalmart");

    await map(
        _.chunk(rows, 5000),
        async (row, index) => {
            logger.debug(`[Business Report - Walmart - Reconciliation(CA) - Legacy] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

            try {
                await MySql.query(
                    `
                        INSERT INTO RawPaymentReportLegacyWalmart (??)
                        VALUES ?
                    `,
                    [
                        TRANSFER,
                        row.map(transfer => [
                            id, "CA",
                            transfer["WalmartOrder"], transfer["WalmartOrderLine"],
                            transfer["WalmartPO"], transfer["WalmartPOLine"],
                            transfer["TransactionType"], transfer["CustomerInvoiceNumber"],
                            transfer["CustomerInvoiceDate"], transfer["SellerInvoiceNumber"],
                            transfer["SellerInvoiceDate"], transfer["SellerInvoiceDispositionDate"],
                            transfer["ShippedQty"], transfer["OfferId"],
                            transfer["WalmartSKUId"],transfer["SellerSKUId"],
                            transfer["ProductTaxCode"],transfer["PartnerItemName"],
                            transfer["PartnerGTIN"],transfer["ShipToCity"],
                            transfer["ShipToProvince"],transfer["ZipCode"],
                            transfer["ShippingMethod"],transfer["LineAmount"],
                            transfer["LineShippingCharge"],transfer["LineFee"],
                            transfer["LineTax"],transfer["GST_HST"],
                            transfer["PST_QSTRemitBySeller"],transfer["PST_QSTRemitByWalmart"],
                            transfer["TotalTenderToFromCustomer"],transfer["CommissionRate"],
                            transfer["CommissionFromSale"],transfer["EffectiveTaxRate"],
                            transfer["CommissionTax"],transfer["PayableToPartner"]
                        ])
                    ]
                );
            } catch (error) {
                console.error(error?.message)
            }
        },
        {
            concurrency: 1
        }
    );

}

module.exports = handle;
