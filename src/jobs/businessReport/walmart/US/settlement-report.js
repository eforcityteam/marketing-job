"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const csv = require("csvtojson");
const moment = require("moment-timezone");
const mapObject = require("map-obj");
const MySql = require("mysql-plugin");
const logger = require("../../../../utils/logger");
const initial = require("../../../../config");

const s3 = new AWS.S3({
    region: "us-west-2",
    useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const RECONCILIATION_HEADER_MAP = {
    "Walmart.com Order #": "WalmartOrder",
    "Walmart.com Order Line #": "WalmartOrderLine",
    "Walmart.com PO #": "WalmartPO",
    "Walmart.com P.O. Line #": "WalmartPOLine",
    "Transaction Type": "TransactionType",
    "Reason Code": "ReasonCode",
    "Detail": "Detail",
    "WFSReferenceID": "WFSReferenceId",
    "Transaction Date/Time": "TransactionDateTime",
    "Qty": "Qty",
    "Partner ID": "PartnerId",
    "Partner GTIN": "PartnerGTIN",
    "Partner Item Name": "PartnerItemName",
    "Category": "Category",
    "Sub Category": "SubCategory",
    "Net Payable": "NetPayable",
};

const TRANSFER = Object.values(RECONCILIATION_HEADER_MAP);
TRANSFER.unshift("ReportId");

async function handle() {

    await initial();

    const tasks = await MySql.query(`
        SELECT 
            ReportId as id,
            FullName as filename,
            BucketFolder as bucket,
            BucketKey as \`key\`
        FROM BusinessReport report
        WHERE 
            ReportClassId = "WALM-US" 
                AND 
            ReportTypeId = "SETTLE" 
                AND 
            RecStatus = "Q";
    `);

    logger.debug(`[Business Report - Walmart - Settlement] There are total ${tasks.length} tasks`);

    await map(
        tasks,
        async task => {

            logger.debug(`[Business Report - Walmart - Settlement] Extracting file ${task.filename}`);

            try {
                const buffer = await s3
                    .getObject({
                        Bucket: task.bucket,
                        Key: task.key
                    })
                    .promise()
                    .then(response => response.Body.toString("utf-8").split("\n"));

                await etl(buffer, task.id);

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "C"
                	WHERE ReportId = ?
                `, [
                    task.id
                ]);

            } catch (error) {

                logger.error({
                    ...error,
                    label: "[Business Report - Walmart - Settlement]"
                });

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "E", Remark = ?
                	WHERE ReportId = ?
                `, [
                    error.message,
                    task.id
                ]);
            }

        },
        {
            concurrency: 1
        }
    );

    logger.debug("[Business Report - Walmart - Settlement] Program Completed");
    return true;
}

async function etl(buffer, id) {

    let headers = buffer.find(strings => /Walmart\.com Order #|WFSReferenceID|Partner ID/ig.test(strings));
    const headersIndex = buffer.indexOf(headers);

    headers = headers.split(",");
    let rows = buffer.slice(headersIndex);

    rows = await csv()
        .fromString(rows.join("\n"))
        .subscribe((json) => {
            json["Walmart.com Order #"] = json?.Walmart["com Order #"];
            json["Walmart.com Order Line #"] = json?.Walmart["com Order Line #"];
            json["Walmart.com PO #"] = json?.Walmart["com PO #"];
            json["Walmart.com P.O. Line #"] = json?.Walmart["com PO Line #"]
            delete json.Walmart;
        });

    rows = rows.map(row => {
        row = mapObject(row, (key, value) => [RECONCILIATION_HEADER_MAP[key], value]);
        row.TransactionDateTime = row.TransactionDateTime ? moment.tz(new Date(row.TransactionDateTime), "UTC").utc().format() : null;
        row.Qty = parseInt(row.Qty) || null;
        row.NetPayable = parseFloat(row.NetPayable) || null;
        return row;
    });

    logger.debug("[Business Report - Walmart - Settlement] Inserting data into LMSDb.RawSettlementWalmart");

    await map(
        _.chunk(rows, 5000),
        async (row, index) => {
            logger.debug(`[Business Report - Walmart - Settlement] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

            try {
                await MySql.query(
                    `
                        INSERT INTO RawSettlementWalmart (??)
                        VALUES ?
                    `,
                    [
                        TRANSFER,
                        row.map(transfer => [
                            id, Number(transfer["WalmartOrder"].replace(/=|\"/ig, "")) || null,
                            transfer["WalmartOrderLine"], Number(transfer["WalmartPO"].replace(/=|\"/ig, "")) || null,
                            transfer["WalmartPOLine"], transfer["TransactionType"],
                            transfer["ReasonCode"], transfer["Detail"],
                            transfer["WFSReferenceId"], transfer["TransactionDateTime"],
                            transfer["Qty"], Number(transfer["PartnerId"].replace(/=|\"/ig, "")) || null,
                            Number(transfer["PartnerGTIN"].replace(/=|\"/ig, "")) || null, transfer["PartnerItemName"],
                            transfer["Category"], transfer["SubCategory"],
                            transfer["NetPayable"]
                        ])
                    ]
                );
            } catch (error) {
                console.error(error?.message)
            }
        },
        {
            concurrency: 1
        }
    );

}

module.exports = handle;
