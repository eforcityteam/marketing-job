"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const moment = require("moment");
const csv = require("csvtojson");
const mapObject = require("map-obj");
const MySql = require("mysql-plugin");
const logger = require("../../../../utils/logger");
const initial = require("../../../../config");

const s3 = new AWS.S3({
    region: "us-west-2",
    useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const RECONCILIATION_HEADER_MAP = {
    "Period Start Date": "PeriodStartDate",
    "Period End Date": "PeriodEndDate",
    "Total Payable": "TotalPayable",
    "Currency": "Currency",
    "Transaction Key": "TransactionKey",
    "Transaction Posted Timestamp": "TransactionPostedTimestamp",
    "Transaction Type": "TransactionType",
    "Transaction Description": "TransactionDescription",
    "Customer Order #": "CustomerOrder",
    "Customer Order line #": "CustomerOrderLine",
    "Purchase Order #": "PurchaseOrder",
    "Purchase Order line #": "PurchaseOrderLine",
    "Amount": "Amount",
    "Amount Type": "AmountType",
    "Ship Qty": "ShipQty",
    "Commission Rate": "CommissionRate",
    "Transaction Reason Description": "TransactionReasonDescription",
    "Partner Item Id": "PartnerItemId",
    "Partner GTIN": "PartnerGTIN",
    "Partner Item Name": "PartnerItemName",
    "Product Tax Code": "ProductTaxCode",
    "Ship to State": "ShipToState",
    "Ship to City": "ShipToCity",
    "Ship to Zipcode": "ShipToZipCode",
    "Contract Category": "ContractCategory",
    "Product Type": "ProductType",
    "Commission Rule": "CommissionRule",
    "Shipping Method": "ShippingMethod",
    "Fulfillment Type": "FulfillmentType"
};

const TRANSFER = Object.values(RECONCILIATION_HEADER_MAP);
TRANSFER.unshift("ReportId");

async function handle() {

    await initial();

    const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId = "WALM-US" 
				AND 
			ReportTypeId = "PAYMENT" 
				AND 
            RecStatus = "Q";
    `);

    logger.debug(`[Business Report - Walmart - Reconciliation(US) - New] There are total ${tasks.length} tasks`);

    await map(
        tasks,
        async task => {

            logger.debug(`[Business Report - Walmart - Reconciliation(US) - New] Extracting file ${task.filename}`);

            try {
                const buffer = await s3
                    .getObject({
                        Bucket: task.bucket,
                        Key: task.key
                    })
                    .promise()
                    .then(response => response.Body.toString("utf-8"));

                await etl(task.id, buffer);

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "C"
                	WHERE ReportId = ?
                `, [
                    task.id
                ]);

            } catch (error) {
                console.log(error);
                logger.error({
                    ...error,
                    label: "[Business Report - Walmart - Reconciliation(US) - New]"
                });

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "E", Remark = ?
                	WHERE ReportId = ?
                `, [
                    error.message,
                    task.id
                ]);
            }

        },
        {
            concurrency: 1,
            stopOnError: process.env.NODE_ENV !== "production"
        }
    );

    logger.debug("[Business Report - Walmart - Reconciliation(US) - New] Program Completed");
    return true;

}

async function etl(id, buffer) {

    let rows = await csv().fromString(buffer);
    rows = rows.map(row => {
        row = mapObject(row, (key, value) => [RECONCILIATION_HEADER_MAP[key], value]);
        row.PeriodStartDate = row.PeriodStartDate ? moment(row.PeriodStartDate, "MM/DD/YYYY").format() : null;
        row.PeriodEndDate = row.PeriodEndDate ? moment(row.PeriodEndDate, "MM/DD/YYYY").format() : null;
        row.TransactionPostedTimestamp = moment(row.TransactionPostedTimestamp, "MM/DD/YYYY").format();
        return row;
    });

    logger.debug("[Business Report - Walmart - Reconciliation(US) - New] Inserting data into LMSDb.RawPaymentReportWalmart");

    await map(
        _.chunk(rows, 5000),
        async (row, index) => {
            logger.debug(`[Business Report - Walmart - Reconciliation(US) - New] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

            if (index === 0) {
                row.shift();
            }

            try {
                await MySql.query(
                    `
                        INSERT INTO RawPaymentReportWalmart (??)
                        VALUES ?
                    `,
                    [
                        TRANSFER,
                        row.map(transfer => [
                            id,
                            transfer["PeriodStartDate"], transfer["PeriodEndDate"],
                            transfer["TotalPayable"], transfer["Currency"],
                            transfer["TransactionKey"], transfer["TransactionPostedTimestamp"],
                            transfer["TransactionType"], transfer["TransactionDescription"],
                            transfer["CustomerOrder"], transfer["CustomerOrderLine"],
                            transfer["PurchaseOrder"], transfer["PurchaseOrderLine"],
                            transfer["Amount"], transfer["AmountType"],
                            transfer["ShipQty"], transfer["CommissionRate"],
                            transfer["TransactionReasonDescription"], transfer["PartnerItemId"],
                            transfer["PartnerGTIN"], transfer["PartnerItemName"],
                            transfer["ProductTaxCode"], transfer["ShipToState"],
                            transfer["ShipToCity"], transfer["ShipToZipCode"],
                            transfer["ContractCategory"], transfer["ProductType"],
                            transfer["CommissionRule"], transfer["ShippingMethod"],
                            transfer["FulfillmentType"],
                        ])
                    ]
                );
            } catch (error) {
                console.error(error?.message)
            }
        },
        {
            concurrency: 1
        }
    );

}

module.exports = handle;
