"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const moment = require("moment");
const csv = require("csvtojson");
const mapObject = require("map-obj");
const MySql = require("mysql-plugin");
const logger = require("../../../../utils/logger");
const initial = require("../../../../config");

const s3 = new AWS.S3({
	region: "us-west-2",
	useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const RECONCILIATION_HEADER_MAP = {
	"UPC": "UPC",
	"Item ID": "ItemId",
	"Vendor (Seller) SKU": "VendorSellerSKU",
	"Product Name": "ProductName",
	"Brand Name": "BrandName",
	"Published": "Published",
	"Available to Sell Units": "AvailableToSellUnits",
	"Damaged_Receipts": "DamagedReceipts",
	"Inbound Units": "InboundUnits",
	"Cube Used": "CubeUsed",
	"First In Stock Date": "FirstInStockDate",
	"Last_30_days_units_received": "Last30DaysUnitsReceived",
	"Last_30_days_PO_units": "Last30DaysPoUnits",
	"Days_of_supply_based_on_last_30_days_sales": "DaysOfSupplyBasedOnLast30DaysSales",
	"Predicted_out_of_stock_date": "PredictedOutOfStockDate",
	"Recommended_replenishment_quantity_based_on_last_30_days_sales": "RecommendedReplenishmentQuantityBasedOnLast30DaysSales",
	"Last_7_days_units_sales": "Last7DaysUnitsSales",
	"Last_7_days_sales": "Last7DaysSales",
	"Last_7_days_instock_days": "Last7DaysInStockDays",
	"Last_30_days_units_sales": "Last30DaysUnitsSales",
	"Last_30_days_sales": "Last30DaysSales",
	"Last_30_days_instock_days": "Last30DaysInStockDays"
};

const TRANSFER = Object.values(RECONCILIATION_HEADER_MAP);
TRANSFER.unshift("ReportId");

async function handle() {

	await initial();

	const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId = "WALM-US" 
				AND 
			ReportTypeId = "WFSINVEN" 
				AND 
			RecStatus = "Q";
    `);

	logger.debug(`[Business Report - Walmart - Inventory Health] There are total ${tasks.length} tasks`);

	await map(
		tasks,
		async task => {

			logger.debug(`[Business Report - Walmart - Inventory Health] Extracting file ${task.filename}`);

			try {
				const buffer = await s3
					.getObject({
						Bucket: task.bucket,
						Key: task.key
					})
					.promise()
					.then(response => response.Body.toString("utf-8"));

				await etl(task.id, buffer);

				await MySql.query(`
					UPDATE BusinessReport
					SET RecStatus = "C"
					WHERE ReportId = ?
				`, [
					task.id
				]);

			} catch (error) {
				console.log(error);
				logger.error({
					...error,
					label: "[Business Report - Walmart - Inventory Health]"
				});

				await MySql.query(`
					UPDATE BusinessReport
					SET RecStatus = "E", Remark = ?
					WHERE ReportId = ?
				`, [
					error.message,
					task.id
				]);
			}

		},
		{
			concurrency: 1,
			stopOnError: process.env.NODE_ENV !== "production"
		}
	);

	logger.debug("[Business Report - Walmart - Inventory Health] Program Completed");
	return true;

}

async function etl(id, buffer) {

	let rows = await csv({ flatKeys: true }).fromString(buffer);
	rows = rows.map(row => {
		row = mapObject(row, (key, value) => [RECONCILIATION_HEADER_MAP[key], value]);
		row.FirstInStockDate = row.FirstInStockDate ? moment(row.FirstInStockDate, "YYYY-MM-DD hh:mm:ss").format() : null;
		row.PredictedOutOfStockDate = row.PredictedOutOfStockDate ? moment(row.PredictedOutOfStockDate, "MM/DD/YYYY").format() : null;
		return row;
	});

	logger.debug("[Business Report - Walmart - Inventory Health] Inserting data into LMSDb.RawInventoryHealthWalmart");

	await map(
		_.chunk(rows, 5000),
		async (row, index) => {
			logger.debug(`[Business Report - Walmart - Inventory Health] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

			try {
				await MySql.query(
					`
                        INSERT INTO RawInventoryHealthWalmart (??)
                        VALUES ?
                    `,
					[
						TRANSFER,
						row.map(transfer => [
							id,
							transfer["UPC"], transfer["ItemId"],
							transfer["VendorSellerSKU"], transfer["ProductName"],
							transfer["BrandName"], transfer["Published"],
							transfer["AvailableToSellUnits"], transfer["DamagedReceipts"],
							transfer["InboundUnits"], transfer["CubeUsed"],
							transfer["FirstInStockDate"], transfer["Last30DaysUnitsReceived"],
							transfer["Last30DaysPoUnits"], transfer["DaysOfSupplyBasedOnLast30DaysSales"],
							transfer["PredictedOutOfStockDate"], transfer["RecommendedReplenishmentQuantityBasedOnLast30DaysSales"],
							transfer["Last7DaysUnitsSales"], transfer["Last7DaysSales"],
							transfer["Last7DaysInStockDays"], transfer["Last30DaysUnitsSales"],
							transfer["Last30DaysSales"], transfer["Last30DaysInStockDays"]

						])
					]
				);
			} catch (error) {
				console.error(error?.message)
			}
		},
		{
			concurrency: 1
		}
	);

}

module.exports = handle;
