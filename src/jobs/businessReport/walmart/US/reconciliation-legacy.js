"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const moment = require("moment");
const csv = require("csvtojson");
const mapObject = require("map-obj");
const MySql = require("mysql-plugin");
const logger = require("../../../../utils/logger");
const initial = require("../../../../config");

const s3 = new AWS.S3({
    region: "us-west-2",
    useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const RECONCILIATION_HEADER_MAP = {
    "Walmart.com Order #": "WalmartOrder",
    "Walmart.com Order Line #": "WalmartOrderLine",
    "Walmart.com PO #": "WalmartPO",
    "Walmart.com P.O. Line #": "WalmartPOLine",
    "Partner Order #": "PartnerOrder",
    "Transaction Type": "TransactionType",
    "Transaction Date Time": "TransactionDateTime",
    "Shipped Qty": "ShippedQty",
    "Partner Item ID": "PartnerItemId",
    "Partner GTIN": "PartnerGTIN",
    "Partner Item name": "PartnerItemName",
    "Product tax code": "ProductTaxCode",
    "Shipping tax code": "ShippingTaxCode",
    "Gift wrap tax code": "GiftWrapTaxCode",
    "Ship to state": "ShipToState",
    "Ship to county": "ShipToCounty",
    "County Code": "CountyCode",
    "Ship to city": "ShipToCity",
    "Zip code": "ZipCode",
    "shipping_method": "ShippingMethod",
    "Total tender to / from customer": "TotalTenderToFromCustomer",
    "Payable to Partner from Sale": "PayableToPartnerFromSale",
    "Commission from Sale": "CommissionFromSale",
    "Commission Rate": "CommissionRate",
    "Gross Sales Revenue": "GrossSalesRevenue",
    "Refunded Retail Sales": "RefundedRetailSales",
    "Sales refund for Escalation": "SalesRefundForEscalation",
    "Gross Shipping Revenue": "GrossShippingRevenue",
    "Gross Shipping Refunded": "GrossShippingRefunded",
    "Shipping refund for Escalation": "ShippingRefundForEscalation",
    "Net Shipping Revenue": "NetShippingRevenue",
    "Gross Fee Revenue": "GrossFeeRevenue",
    "Gross Fee Refunded": "GrossFeeRefunded",
    "Fee refund for Escalation": "FeeRefundForEscalation",
    "Net Fee Revenue": "NetFeeRevenue",
    "Gift Wrap Quantity": "GiftWrapQuantity",
    "Gross Gift-Wrap Revenue": "GrossGiftWrapRevenue",
    "Gross Gift-Wrap Refunded": "GrossGiftWrapRefunded",
    "Gift wrap refund for Escalation": "GiftWrapRefundForEscalation",
    "Net Gift Wrap Revenue": "NetGiftWrapRevenue",
    "Tax on Sales Revenue": "TaxOnSalesRevenue",
    "Tax on Shipping Revenue": "TaxOnShippingRevenue",
    "Tax on Gift-Wrap Revenue": "TaxOnGiftWrapRevenue",
    "Tax on Fee Revenue": "TaxOnFeeRevenue",
    "Effective tax rate": "EffectiveTaxRate",
    "Tax on Refunded Sales": "TaxOnRefundedSales",
    "Tax on Shipping Refund": "TaxOnShippingRefund",
    "Tax on Gift-Wrap Refund": "TaxOnGiftWrapRefund",
    "Tax on Fee Refund": "TaxOnFeeRefund",
    "Tax on Sales refund for Escalation": "TaxOnSalesRefundForEscalation",
    "Tax on Shipping Refund for Escalation": "TaxOnShippingRefundForEscalation",
    "Tax on Gift-Wrap Refund for escalation": "TaxOnGiftWrapRefundForEscalation",
    "Tax on Fee Refund for escalation": "TaxOnFeeRefundForEscalation",
    "Total NET Tax Collected": "TotalNetTaxCollected",
    "Tax Withheld": "TaxWithheld",
    "Adjustment Description": "AdjustmentDescription",
    "Adjustment Code": "AdjustmentCode",
    "Original Item price": "OriginalItemPrice",
    "Original Commission Amount": "OriginalCommissionAmount",
    "Spec Category": "SpecCategory",
    "Contract Category": "ContractCategory",
    "Product Type": "ProductType",
    "Flex Commission Rule": "FlexCommissionRule",
    "Return Reason Code": "ReturnReasonCode",
    "Return Reason Description": "ReturnReasonDescription",
    "Fee Withheld Flag": "FeeWithheldFlag",
    "Fulfillment Type": "FulfillmentType",
};

const TRANSFER = Object.values(RECONCILIATION_HEADER_MAP);
TRANSFER.unshift("ReportId", "Country");

async function handle() {

    await initial();

    const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId = "WALM-US" 
				AND 
			ReportTypeId = "ZPAYMENT" 
				AND 
            RecStatus = "Q";
    `);

    logger.debug(`[Business Report - Walmart - Reconciliation(US) - Legacy] There are total ${tasks.length} tasks`);

    await map(
        tasks,
        async task => {

            logger.debug(`[Business Report - Walmart - Reconciliation(US) - Legacy] Extracting file ${task.filename}`);

            try {
                const buffer = await s3
                    .getObject({
                        Bucket: task.bucket,
                        Key: task.key
                    })
                    .promise()
                    .then(response => response.Body.toString("utf-8"));

                await etl(task.id, buffer);

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "C"
                	WHERE ReportId = ?
                `, [
                    task.id
                ]);

            } catch (error) {
                console.log(error);
                logger.error({
                    ...error,
                    label: "[Business Report - Walmart - Reconciliation(US) - Legacy]"
                });

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "E", Remark = ?
                	WHERE ReportId = ?
                `, [
                    error.message,
                    task.id
                ]);
            }

        },
        {
            concurrency: 1,
            stopOnError: process.env.NODE_ENV !== "production"
        }
    );

    logger.debug("[Business Report - Walmart - Reconciliation(US) - Legacy] Program Completed");
    return true;

}

async function etl(id, buffer) {

    let rows = await csv()
        .fromString(buffer)
        .subscribe((json) => {
            json["Walmart.com Order #"] = json?.Walmart["com Order #"];
            json["Walmart.com Order Line #"] = json?.Walmart["com Order Line #"];
            json["Walmart.com PO #"] = json?.Walmart["com PO #"];
            json["Walmart.com P.O. Line #"] = json?.Walmart["com P"]?.O[" Line #"];
            delete json.Walmart;
        });


    rows = rows.map(row => {
        row = mapObject(row, (key, value) => [RECONCILIATION_HEADER_MAP[key], value]);
        row.TransactionDateTime = moment(row.TransactionDateTime, "MM/DD/YYYY").format()
        return row;
    })

    logger.debug("[Business Report - Walmart - Reconciliation(US) - Legacy] Inserting data into LMSDb.RawPaymentReportLegacyWalmart");

    await map(
        _.chunk(rows, 5000),
        async (row, index) => {
            logger.debug(`[Business Report - Walmart - Reconciliation(US) - Legacy] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

            try {
                await MySql.query(
                    `
                        INSERT INTO RawPaymentReportLegacyWalmart (??)
                        VALUES ?
                    `,
                    [
                        TRANSFER,
                        row.map(transfer => [
                            id, "US",
                            transfer["WalmartOrder"], transfer["WalmartOrderLine"],
                            transfer["WalmartPO"], transfer["WalmartPOLine"],
                            transfer["PartnerOrder"], transfer["TransactionType"],
                            transfer["TransactionDateTime"], transfer["ShippedQty"],
                            transfer["PartnerItemId"], transfer["PartnerGTIN"],
                            transfer["PartnerItemName"], transfer["ProductTaxCode"],
                            transfer["ShippingTaxCode"], transfer["GiftWrapTaxCode"],
                            transfer["ShipToState"], transfer["ShipToCounty"],
                            transfer["CountyCode"], transfer["ShipToCity"],
                            transfer["ZipCode"], transfer["ShippingMethod"],
                            transfer["TotalTenderToFromCustomer"], transfer["PayableToPartnerFromSale"],
                            transfer["CommissionFromSale"], transfer["CommissionRate"],
                            transfer["GrossSalesRevenue"], transfer["RefundedRetailSales"],
                            transfer["SalesRefundForEscalation"], transfer["GrossShippingRevenue"],
                            transfer["GrossShippingRefunded"], transfer["ShippingRefundForEscalation"],
                            transfer["NetShippingRevenue"], transfer["GrossFeeRevenue"],
                            transfer["GrossFeeRefunded"], transfer["FeeRefundForEscalation"],
                            transfer["NetFeeRevenue"], transfer["GiftWrapQuantity"],
                            transfer["GrossGiftWrapRevenue"], transfer["GrossGiftWrapRefunded"],
                            transfer["GiftWrapRefundForEscalation"], transfer["NetGiftWrapRevenue"],
                            transfer["TaxOnSalesRevenue"], transfer["TaxOnShippingRevenue"],
                            transfer["TaxOnGiftWrapRevenue"], transfer["TaxOnFeeRevenue"],
                            transfer["EffectiveTaxRate"], transfer["TaxOnRefundedSales"],
                            transfer["TaxOnShippingRefund"], transfer["TaxOnGiftWrapRefund"],
                            transfer["TaxOnFeeRefund"], transfer["TaxOnSalesRefundForEscalation"],
                            transfer["TaxOnShippingRefundForEscalation"], transfer["TaxOnGiftWrapRefundForEscalation"],
                            transfer["TaxOnFeeRefundForEscalation"], transfer["TotalNetTaxCollected"],
                            transfer["TaxWithheld"], transfer["AdjustmentDescription"],
                            transfer["AdjustmentCode"], transfer["OriginalItemPrice"],
                            transfer["OriginalCommissionAmount"], transfer["SpecCategory"],
                            transfer["ContractCategory"], transfer["ProductType"],
                            transfer["FlexCommissionRule"], transfer["ReturnReasonCode"],
                            transfer["ReturnReasonDescription"], transfer["FeeWithheldFlag"],
                            transfer["FulfillmentType"]
                        ])
                    ]
                );
            } catch (error) {
                console.error(error?.message)
            }
        },
        {
            concurrency: 1
        }
    );

}

module.exports = handle;
