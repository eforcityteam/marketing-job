"use strict";
const _ = require("lodash");
const map = require("p-map");
const AWS = require("aws-sdk");
const csv = require("csvtojson");
const mapObject = require("map-obj");
const MySql = require("mysql-plugin");
const logger = require("../../../../utils/logger");
const initial = require("../../../../config");

const s3 = new AWS.S3({
    region: "us-west-2",
    useAccelerateEndpoint: process.env.NODE_ENV !== "production"
});

const RECONCILIATION_HEADER_MAP = {
    "Partner GTIN": "PartnerGTIN",
    "Vendor SKU": "VendorSKU",
    "Item Name": "ItemName",
    "Fulfillment Center": "FulfillmentCenter",
    "Length": "Length",
    "Height": "Height",
    "Width": "Width",
    "Weight": "Weight",
    "Item Volume": "ItemVolume",
    "Total units on hand": "TotalUnitsOnHand",
    "Total units shipped": "TotalUnitsShipped",
    "Est Total Item Volume": "EstTotalItemVolume",
    "% of Storage Fee": "PercentageOfStorageFee",
    "Storage Fee": "StorageFee"
};

const TRANSFER = Object.values(RECONCILIATION_HEADER_MAP);
TRANSFER.unshift("ReportId");

async function handle() {

    await initial();

    const tasks = await MySql.query(`
		SELECT 
			ReportId as id,
			FullName as filename,
			BucketFolder as bucket,
			BucketKey as \`key\`
		FROM BusinessReport report
		WHERE 
			ReportClassId = "WALM-US" 
				AND 
			ReportTypeId = "WFSSTORA" 
				AND 
            RecStatus = "Q";
    `);

    logger.debug(`[Business Report - Walmart - Storage Report] There are total ${tasks.length} tasks`);

    await map(
        tasks,
        async task => {

            logger.debug(`[Business Report - Walmart - Storage Report] Extracting file ${task.filename}`);

            try {
                const buffer = await s3
                    .getObject({
                        Bucket: task.bucket,
                        Key: task.key
                    })
                    .promise()
                    .then(response => response.Body.toString("utf-8"));

                await etl(task.id, buffer);

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "C"
                	WHERE ReportId = ?
                `, [
                    task.id
                ]);

            } catch (error) {
                console.log(error);
                logger.error({
                    ...error,
                    label: "[Business Report - Walmart - Storage Report]"
                });

                await MySql.query(`
                	UPDATE BusinessReport
                	SET RecStatus = "E", Remark = ?
                	WHERE ReportId = ?
                `, [
                    error.message,
                    task.id
                ]);
            }

        },
        {
            concurrency: 1,
            stopOnError: process.env.NODE_ENV !== "production"
        }
    );

    await MySql.query(`
        UPDATE LMSDb.RawStorageReportWalmart 
        SET ProductListingId = SUBSTRING_INDEX(VendorSKU, '_', 1) 
        WHERE ProductListingId IS NULL;
    `)

    logger.debug("[Business Report - Walmart - Storage Report] Program Completed");
    return true;
}

async function etl(id, string) {

    let rows = string.split("\n");

    /**
     * 
     * Find the start index of the csv file
     * 
     */
    const startIndex = rows.findIndex(sentence => /Partner GTIN|Vendor SKU|Fulfillment Center/ig.test(sentence));

    /**
     * 
     * Remove the reminder from csv file
     * 
     */
    rows.splice(0, startIndex);

    rows = await csv({ flatKeys: true })
        .fromString(rows.join("\n"))
        .then(row => {
            row = mapObject(row, (key, value) => [RECONCILIATION_HEADER_MAP[key], value]);
            return row;
        });

    logger.debug("[Business Report - Walmart - Storage Report] Inserting data into LMSDb.RawStorageReportWalmart");

    await map(
        _.chunk(rows, 5000),
        async (row, index) => {
            logger.debug(`[Business Report - Walmart Walmart - Storage Report] chunk #${index + 1}/${Math.ceil(rows.length / 5000)}`);

            try {
                await MySql.query(
                    `
                        INSERT INTO RawStorageReportWalmart (??)
                        VALUES ?
                    `,
                    [
                        TRANSFER,
                        row.map(transfer => [
                            id,
                            Number(transfer["PartnerGTIN"].replace(/=|\"/ig, "")) || null,
                            transfer["VendorSKU"],
                            transfer["ItemName"], transfer["FulfillmentCenter"],
                            transfer["Length"], transfer["Height"],
                            transfer["Width"], transfer["Weight"],
                            transfer["ItemVolume"], transfer["TotalUnitsOnHand"],
                            transfer["TotalUnitsShipped"], transfer["EstTotalItemVolume"],
                            transfer["PercentageOfStorageFee"], transfer["StorageFee"]
                        ])
                    ]
                );
            } catch (error) {
                console.error(error?.message)
            }
        },
        {
            concurrency: 1
        }
    );

}

module.exports = handle;
