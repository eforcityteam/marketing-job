"use strict";
const updateAmazonTransaction = require("./amazon/transaction");
const updateTargetTransaction = require("./target/transaction");
const updateEBayTransaction = require("./ebay/transaction");
const updateWalmartPaymentReportLegacyCA = require("./walmart/CA/reconciliation-legacy");
const updateWalmartPaymentReportLegacyUS = require("./walmart/US/reconciliation-legacy");
const updateWalmartPaymentReportNewUS = require("./walmart/US/reconciliation-new");
const updateWalmartInventoryHealthUS = require("./walmart/US/inventory-heath");
const updateWalmartSettlementReportUS = require("./walmart/US/settlement-report");
const updateWalmartStorageReportUS = require("./walmart/US/storage-report");
const logger = require("../../utils/logger");

async function processing() {

	/**
	 * 
	 * Amazon
	 * 
	 */
	await updateAmazonTransaction();


	/**
	 * 
	 * Target
	 * 
	 */
	await updateTargetTransaction();

	/**
	 * 
	 * eBay
	 * 
	 */
	await updateEBayTransaction();

	/**
	 * 
	 * Walmart
	 * 
	 */
	await updateWalmartPaymentReportLegacyCA();
	await updateWalmartPaymentReportLegacyUS();
	await updateWalmartPaymentReportNewUS();
	await updateWalmartInventoryHealthUS();
	await updateWalmartSettlementReportUS();
	await updateWalmartStorageReportUS();

	/**
	 * 
	 * 
	 * 
	 */
	logger.debug("[businessReport] Program Finished");
}
processing()
module.exports = processing;
