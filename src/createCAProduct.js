"use strict";
const qs = require("qs");
const ms = require("ms");
const mem = require("mem");
const _ = require("lodash");
const fs = require("fs");
const axios = require("axios");
const UUID = require("uuid");
const MySql = require("mysql-plugin");
const moment = require("moment");
const initialize = require("./config");
const Account = require("./account/account.model.js");
const exceltojson = require("convert-excel-to-json");
const base64 = require("./utils/base64");
const md5 = require("md5");
const csvtojson = require("csvtojson");
const cheerio = require("cheerio");
const stringSimilarity = require("string-similarity");
const updateListingReport = require("./jobs/updateListingReport");
const encoder = require("./utils/encoder");
const excelToJson = require("convert-excel-to-json");
const excelColumnName = require("excel-column-name");


void async function () {

	const rows = excelToJson({ sourceFile: "./sample.xlsx" })["Product"];

	const SECTION_START = _.invert(rows.shift());
	const COLUMN = rows.shift();
	const LAST_COLUMN_INDEX = _.size(COLUMN);
	const BODY_END = excelColumnName.excelColToInt(SECTION_START["Attributes"]) - 1;

	const MAP = {
		BODY: [],
		ATTRIBUTES: []
	};

	for (let index = 1; index <= LAST_COLUMN_INDEX; index++)
		MAP[index <= BODY_END ? "BODY" : "ATTRIBUTES"].push(excelColumnName.intToExcelCol(index));

	const COLUMN_MAP = Object.fromEntries(_.map(MAP, (values, field) => values.map(value => [value, field])).flat());

	const array = rows.map(row => _.reduce(row, (body, value, column) => {
		if (COLUMN_MAP[column] == "BODY") {
			body[COLUMN[column]] = value;
		} else if (COLUMN_MAP[column] == "ATTRIBUTES") {
			(body.attributes = body.attributes || []).push({
				Name: COLUMN[column],
				Value: value
			});
		}
		return body;
	}, {}));

	console.log(JSON.stringify(array[0], null, 4));

}();
