"use strict";
if (process.env.NODE_ENV != "production")
	require("dotenv").config();

const mem = require("mem");
const MySql = require("mysql-plugin");
const logger = require("./utils/logger");
const getStore = require("./utils/node-utils/getStore");

const MYSQL_DB = "LMSDb";
const MYSQL_JUVO_DB = "ods";

async function initialize() {

	const keys = [
		"/common/production",
		"/clickhouse-server",
		"/analytics"
	];

	const stores = await getStore(keys);

	console.log({
		host: stores.mysql_host,
		user: stores.mysql_user,
		password: stores.mysql_password,
		database: MYSQL_DB,
	});

	/**
	 *
	 * Connect the default MySqlDb
	 *
	 */
	MySql.init({
		host: stores.mysql_host,
		user: stores.mysql_user,
		password: stores.mysql_password,
		database: MYSQL_DB,
	});

	/**
	 * 
	 * 
	 * 
	 */
	MySql.connect("oldDb", {
		host: stores.mysql_efc_host,
		user: stores.mysql_efc_user,
		password: stores.mysql_efc_password,
		database: "ProductManDb"
	}, {});

	MySql.connect(
		"EFCReadDb",
		{
			host: stores["mysql_host-read-replica-1"],
			user: stores.mysql_user,
			password: stores.mysql_password,
			database: MYSQL_DB
		}
	);

	/**
	 * 
	 * Connect the juvo MySqlDb
	 * 
	 */
	MySql.connect("juvoDb", {
		host: stores.mysql_juvo_write_host,
		user: stores.mysql_juvo_write_user,
		password: stores.mysql_juvo_write_password,
		database: MYSQL_JUVO_DB
	});

	logger.debug("[System] Completed on setup environment");

	return {
		database: {
			efcDb: {
				client: "mysql2",
				pool: {
					min: 2,
					max: 5
				},
				connection: {
					database: "LMSDb",
					host: stores.mysql_host,
					port: 3306,
					user: stores.mysql_user,
					password: stores.mysql_password
				},
				useNullAsDefault: true
			},
			juvoDb: {
				client: "mysql2",
				pool: {
					min: 2,
					max: 5
				},
				connection: {
					host: stores.mysql_juvo_write_host,
					user: stores.mysql_juvo_write_user,
					password: stores.mysql_juvo_write_password,
					database: MYSQL_JUVO_DB,
					port: 3306,
				},
				useNullAsDefault: true
			}
		},
		clickhouse: {
			juvo: {
				host: stores["analytics-clickhouse-host"],
				port: stores["analytics-clickhouse-port"],
				username: stores["analytics-clickhouse-username"],
				password: stores["analytics-clickhouse-password"]
			}
		},
		redis: {
			host: process.env.NODE_ENV === "production" ? "test-redis" : "10.0.1.33",
			port: process.env.NODE_ENV === "production" ? 6379 : 6369,
			password: stores["clickhouse-server-redis-password"],
			db: 3,
		},
	};
}

module.exports = mem(initialize);
