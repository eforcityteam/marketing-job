"use strict";
const _ = require("lodash");
const MySql = require("mysql-plugin");
const initial = require("./config");

// convertMySqlToStartCase
const options = {
	db: "juvoDb",
	table: "efc.uds_dim_product",
	ignore: ["sku", "upc", "asin"]
};

async function convertMySqlToStartCase() {

	await initial();

	const syntax = await MySql
		.pools[options.db]
		.exec("SHOW CREATE TABLE ?? ;", [options.table])
		.then(response => {
			const tableName = response[0]["Table"];
			const keys = response[0]["Create Table"]
				.split("\n")
				.slice(1, -1)
				.map((row) => {
					if (/KEY/.test(row)) {
						if (/PRIMARY KEY/.test(row)) {
							const regex = row.match(/\`(?<key>[a-zA-Z0-9_]+)\`/ig)[0].trim();
							if (options.ignore.includes(regex.replace(/\`/g, ""))) {
								return row;
							} else {
								const startCase = _.startCase(regex).replace(/\s+/g, "");
								return row.replace(regex, startCase);
							}
						} else {
							const regex = row.match(/\`(?<key>[a-zA-Z0-9_]+)\`/ig)[1].trim();
							if (options.ignore.includes(regex.replace(/\`/g, ""))) {
								const result = row.replace(/\`[a-zA-Z0-9_]+\`/, `IX_${regex.replace(/\`/g, "")}`);
								return result;
							} else {
								const startCase = _.startCase(regex).replace(/\s+/g, "");
								const result = row.replace(regex, startCase).replace(/\`[a-zA-Z0-9_]+\`/, `IX_${startCase}`);
								return result;
							}
						}
					} else {
						const regex = row.match(/\`(?<key>[a-zA-Z0-9_]+)\`/ig)[0].trim();
						if (options.ignore.includes(regex.replace(/\`/g, ""))) {
							return row;
						} else {
							const startCase = _.startCase(regex).replace(/\s+/g, "");
							return row.replace(regex, startCase);
						}
					}
				})
				.map((row, index) => {
					if (index === 0) {
						return row;
					} else {
						return "						".concat(row);
					}
				});

			return `
					CREATE TABLE \`${tableName}\` (
						${keys.join("\n")}
					);
				`;
		});

	return syntax;
}

(async function main() {
	const data = await convertMySqlToStartCase(options);
	console.log(data);
})()




// convertMySqlToClickHouse
const options = {
	sourceTable: "ReportDb.OmsCentralReportInvoiceLine",
	targetDb: "ReportDb"
};

async function convertMySqlToClickHouse(options) {

	// MYsql To ClickHouse
	const TYPE_MAP = {
		"date": "Date",
		"datetime": "DateTime",
		"timestamp": "DateTime",
		"int": "UInt32",
		"varchar": "String",
		"char": "String",
		"decimal": "Float32",
		"float": "Float32",
		"default": "String"
	};

	const syntax = await MySql
		.query("SHOW CREATE TABLE ?? ;", [options.sourceTable])
		.then(response => {
			const tableName = response[0]["Table"];
			const keys = response[0]["Create Table"]
				.split("\n")
				.slice(1, -1)
				.filter((row) => !/KEY/.test(row))
				.map((row) => {
					const regex = row.match(/\`(?<key>[a-zA-Z0-9_]+)\`\s(?<type>[a-zA-Z0-9_]+).*\s/);
					const isNotNull = /NOT NULL/.test(row);
					const key = regex.groups.key;
					const type = regex.groups.type;

					return isNotNull ?
						`					\`${key}\` ${TYPE_MAP[type] || "String"}` :
						`					\`${key}\` Nullable(${TYPE_MAP[type] || "String"})`;
				});
			keys[0] = keys[0].trim();
			return `
				CREATE TABLE ${options.targetDb}.${tableName} (
					${keys.join(", \n")}
				)  ENGINE = MySQL(
					'eerpdb.cluster-ro-clctnzsn6euc.us-west-2.rds.amazonaws.com:3306',
					'ReportDb',
					'ReportCache',
					'itadmin',
					'linsanity7'
				);
			`;
		});
	return syntax;
}

(async function main() {
	const data = await convertMySqlToClickHouse(options);
	console.log(data);
})()
