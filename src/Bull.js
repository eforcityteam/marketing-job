"use strict";
const ms = require("ms");
const map = require("p-map");
const Queue = require("bull");
const MySql = require("mysql-plugin");
const logger = require("./utils/logger");

function Bull(setting) {

	const _setting = deepCopy(setting);

	if (typeof _setting.defaultJobOptions?.timeout === "string")
		_setting.defaultJobOptions.timeout = ms(setting.config.defaultJobOptions.timeout);

	if (typeof _setting.config.db == "number")
		_setting.config.redis.db = _setting.config.db;

	logger.info({
		message: `[${_setting.config.prefix}] There are total ${_setting.jobs.length} tasks`,
		payload: _setting
	});

	this._setting = _setting;
}

Bull.prototype.updateConfigs = async function () {

	logger.debug("[Bull] Updating monitoring configs");

	const setting = this._setting;

	await MySql
		.query(`
			INSERT INTO MonitorDb.DefJobQueue (??) VALUES ? 
			ON DUPLICATE KEY UPDATE 
				QueueId = VALUES(QueueId), 
				Description=VALUES(Description),
				RecStatus = "A";
			UPDATE MonitorDb.DefJobQueue 
			SET RecStatus = "D" 
			WHERE CategoryId = ? AND QueueId NOT IN (?);
		`,
			[
				[
					"QueueId",
					"CategoryId",
					"FullName",
					"Description",
					"Path",
					"Schedule",
					"InvokeAtStart"
				],
				setting.jobs.map(item => [
					item.name,
					setting.config.prefix,
					item.name,
					item.description,
					item.path,
					item.repeat,
					item.invoke ? 1 : 0
				]),
				setting.config.prefix,
				setting.jobs.map(item => item.name)
			]
		);

	return {};
};

Bull.prototype.createJob = async function (job) {

	const queue = new Queue(job.name, this._setting.config);

	// Remove active jobs
	await queue.clean(0, "active");

	queue.process(1, require(job.path));

	queue.on("error", error => {
		error.message = `[Job] ${error.message}`;
		logger.error(error);
		process.exit(1);
	});

	const payload = job.payload ? JSON.parse(job.payload) : {};

	if (process.env.NODE_ENV === "production") {
		if (job.repeat) {
			queue.add(payload, { repeat: { cron: job.repeat } });
		}
	}

	if (job.invoke)
		queue.add(payload);

	return queue;
};

Bull.prototype.process = function () {
	const jobs = this._setting.jobs;
	return map(jobs, job => this.createJob(job));
};

function deepCopy(config) {
	return JSON.parse(JSON.stringify(config));
}

module.exports = Bull;
