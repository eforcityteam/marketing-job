build:
	git config credential.helper store
	git pull
	docker-compose build
	hostname=$(shell hostname 2>/dev/null || echo 'localhost') docker-compose up -d --remove-orphans
	docker-compose logs -f --tail=100 

dev:
	pm2 start pm2.json
	pm2 monit 
